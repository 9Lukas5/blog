---
tags:
    - Glasfaser
    - Telekom
    - Hotline
    - Service

title: Glasfaser endlich in Betrieb
description:
    "Follow-Up zur finalen Inbetriebnahme des Glasfaseranschlusses mit den vielen Service-Anrufen"
date: 2023-04-19 00:00
update: 2023-04-19 00:00
list_entry: true
---

# {{ title }}

<p class="introduction" markdown>
<span class="date">{{ date }}</span>  
{{ description }}
</p>

---

[Im letzten Eintrag zu diesem Glasfaserausbau][lastEntry] stand die Antwort des Subunternehmers zum weiteren Vorgehen aus.
Mittlerweile ist der Anschluss endlich in Betrieb \o/ und dieser Eintrag die Vervollständigung, was bis dahin jetzt noch passiert ist.

## Antwort Subunternehmer

Der Subunternehmer konnte mit der Glasfaser-Auftragsnummer der Telekom nichts wirklich anfangen.
Stattdessen wurde einfach mit der Anschrift des Objekts gesucht und mir deren _Bau-ID_ genannt.

Problem aus Sicht des Subunternehmers: Laut deren System war der Auftrag blockiert, da seitens der Telekom wohl ein Storno vorlag.
Solange die Telekom das nicht ändert, kann der Subunternehmer den Auftrag nicht bearbeiten, so deren Aussage. :face_with_raised_eyebrow:
Ich bekam eine Hotline-Nummer der Telekom genannt, angeblich ganz neu eingerichtet, dort soll ich das Problem anbringen.

## Nochmal Telekom Hotline-PingPong

Neu war diese Hotline-Nummer nicht, und auch nicht von der Abteilung von der sie laut Subunternehmer sein sollte.
Und wie üblich für die Telekom Hotlines: ruft man bei der einen an und bräuchte jemanden einer der anderen vielen, können/wollen sie einen nicht weiterverbinden. :neutral_face:

Letztlich bei der richtigen Hotline-Nummer angerufen, konnten diese aber, anders als vom Subunternehmer versprochen, nichts mit deren Bau-ID anfangen.
Also wieder die Glasfaser-Auftragsnummer benutzt, um sie zum richtigen Auftrag zu führen :face_with_rolling_eyes:  
Aber weiter gebracht hat mich das dennoch nicht, denn laut Telekom Hotline liegt kein Storno auf dem Auftrag.

## Patt-Situation

Der Subunternehmer sagt die Telekom hat ein Storno auf dem Auftrag, die Telekom behauptet der Auftrag sei nicht blockiert, storniert, oder sonst was.
Als Kunde sitzt man da dann ganz schön auf dem Trockenen, wenn beide Seiten sich stur stellen.

Glücklicherweise war das beim Subunternehmer nicht der Fall:
Nach Bericht über die Aussage der Telekom Hotline hat man einfach kurzerhand beschlossen mir trotz Blockade einfach einen Termin zu geben und ich soll dann mit dem Techniker gemeinsam vor Ort schauen, ob der Auftrag bearbeitet werden kann oder nicht. :partying_face:

## 1. Termin Subunternehmer: Bauplaner

Im Gegensatz zu den Telekom-Terminen, bei denen man einen kompletten Tag das Sofa bewacht, gab es vom Subunternehmer eine spezifische Uhrzeit und der Techniker kam auch pünktlichst. :ok_hand_tone2:

Der Mitarbeiter der zu diesem Termin kam, war erstmal ein Bauplaner.
Dieser hatte also nicht direkt vor etwas an der Installation zu arbeiten, sondern sich ein Bild der Lage machen und das weitere Vorgehen zu besprechen und festzulegen.

Nachdem er sich mal alles angesehen hat und die bisherige Geschichte in groben Zügen gehört hat, hat er den Hausanschluss kurz dokumentiert und mir den Anschluss im System für einen Inbetriebnahme-Termin auf den Weg gebracht.

Es sollte sich dann innerhalb einer Woche etwas rühren.
Nachdem es nach dem Wochenende noch keine eingegangenen Nachrichten gab (weder E-Mail, noch SMS), habe ich nochmal eine Mail los geschickt und einen Tag später kam die Benachrichtigung, der Anschluss sei jetzt zur Inbetriebnahme fertig.
Jetzt konnte man auf der Telekom Seite des Glasfaserauftrags einen Termin zur Inbetriebnahme wählen.

## 2. Termin Subunternehmer: Inbetriebnahme

Wie beim ersten Termin des Subunternehmers war auch dieser mit einer spezifischen Uhrzeit.
Dieses Mal waren die Techniker bereits zwei Stunden früher als vereinbart bereit sich an unseren Termin zu machen und haben angerufen, ob wir den vorziehen könnten.
Konnten wir :thumbsup_tone2:

Der Termin lief eigentlich völlig unspektakulär und so ziemlich so ab, wie es überall mehr oder weniger sein sollte:
Techniker klemmt sein Testmodem an, prüft Verbindung, Messung in Ordnung, Anschluss wird im System freigegeben und die Glasfaser-Home-ID zugeteilt.
Diese wurde außen auf die Glasfaser-Steckdose in der Wohnung und im Keller am Hausanschluss an die zugehörige Glasfaser geklebt.

Nachdem der Techniker weg war, war auch die Benachrichtigung, dass wir unseren Anschluss jetzt nutzen können, bereits da.
Also haben wir direkt unser Modem angeschlossen, am Router das Kabel umgesteckt und alles lief so ziemlich Plug'n'Play, ohne dass ich was in der Router-Konfiguration anpassen hätte müssen.

!!! tip "Tipp"

    Anleitung lesen soll helfen :grimacing:

    Wenn der Router bisher an der TAE Dose hing und darüber einen Link aufgebaut hat: Auch wenn am Router hinten ein normal gewachsener RJ45 dafür ist, das Patch-Kabel vom Glasfaser-Modem gehört nicht in den `DSL` gelabelten Anschluss, sondern den wo `Link` dran steht^^

Mittlerweile ist der Wechsel auf die Glasfaser knappe zwei Wochen her und läuft soweit problemlos.
(Wäre ja auch noch schöner, wenn nicht :zany_face:)

[lastEntry]: ../8_service-hell-telekom/index.md
