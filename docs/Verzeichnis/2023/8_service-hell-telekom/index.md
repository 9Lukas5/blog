---
tags:
    - Glasfaser
    - Telekom
    - Hotline
    - Service

title: Servicehölle Telekom
description:
    "Eine Odysee von Hotlineanrufen im Rahmen des Glasfaserausbaus bei der Deutschen Telekom"
date: 2023-03-10 00:00
update: 2023-03-10 00:00
list_entry: true
---

# {{ title }}

<p class="introduction" markdown>
<span class="date">{{ date }}</span>  
{{ description }}
</p>

---

Nach einer Umfrage wer Interesse hat, bei der sich ausreichend viele gemeldet haben, wurde der Glasfaserausbau in unserem Gebiet tatsächlich angefangen.
Involviert sind dabei verschiedene Firmen, für den Endkunden ist das aber mal mehr mal weniger offensichtlich.
Das Ganze wäre nicht weiter dramatisch, wenn die Hotline bei Problemen wenigstens entsprechend den Durchblick hätte.
Stattdessen irren sie meist völlig planlos durch die Gegend und erst nach einem dutzend Versuchen findet sich jemand in den tieferen Ebenen, der tatsächlich sieht, was Stand der Dinge ist.  
Aber: Was kann denn bei so einem Glasfaserausbau schon groß schiefgehen?
Ist doch nur Kabel ziehen, Dose im Haus anbringen, Geräte anschließen und fertig :shrug:

## Die Ausbauschritte

Irgendwann war es dann so weit und es kamen tatsächlich die Bauigel und haben angefangen die Gehwege aufzumachen und ihre Leerrohre reinzuwerfen.
Es wurden an verschiedenen Stellen so graue Kästen in den Straßen aufgestellt, in denen die Leerrohre zusammen kommen.
Die Bauigel hatten Listen, wer alles am Ausbau teilnimmt, denn es wird nicht einfach jedes Gebäude im Ausbaugebiet mit FTTH ausgebaut, sondern nur die, die explizit daran Teilnehmen.
So haben beispielsweise unsere Nachbarn keine Notwendigkeit für Glasfaser gesehen und nicht teilgenommen, bei denen bleibt es also beim Klingeldraht und Coax-Kabel von heute Vodafone.

Mit den Teilnehmern wurden dann Gebäudebegehungen von den Bauigeln gemacht, wo die Glasfaser ins Gebäude kommen soll.
Danach hatte man erstmal wieder sehr lange nichts mehr von denen gehört.
Die haben ihre Leerrohre verlegt, die Gehwege wieder zu gemacht und, an jedem Gebäude das angeschlossen werden soll, an der vereinbarten Stelle einen Fleck im Gehweg nur mit Split abgedeckt, statt direkt wieder ordentlich zu verschließen.

Nach erfolgtem verlegen der Leerrohre in den Gehwegen kam dann die Einführung in die einzelnen Gebäude an die Reihe.
Zuerst wurde einfach durch die Straßen gegangen und geklingelt, diejenigen die so nicht erfasst wurden, bekamen dann dedizierte Termine dafür.
Dann hing also das zuvor noch im Gehweg aufgerollte Leerrohr innen aus der Wand :partying_face:

Es verging wieder einige Zeit.
Dauert eben bis alle Leerrohre in den Häusern sind und man selbst dann mit dem Einziehen der Glasfaser dran ist.
Irgendwann war es aber soweit: Termine kamen, Faser wurde eingeblasen und der __Hausübergabepunkt__ an die Wand geschraubt.

Auch danach dauerte es wieder etwas bis dann der nächste Termin anstand: das Verbauen der __Glasfasersteckdose__, sozusagen die Anschlussdose/TAE-Buchse des Glasfasers.
Vielleicht gibts da auch noch einen anderen Fachbegriff für, aber der Begriff __Glasfasersteckdose__ ist mir halt dabei über den Weg gelaufen.  
Diese Glassteckdose wird normal direkt in der Wohnung angebracht, heißt vom Hausübergabepunkt wird ein Glasfaserkabel im Gebäude bis zum gewünschten Installationsort der Glassteckdose gezogen.
Der installierende Techniker hat ein eigenes Modem dabei, steckt das dort an und telefoniert mit jmd. in der Technik um die Verbindung final zu testen.
Klappt das alles einwandfrei, wird wieder abgestellt und es kommt eine siebenstellige alpha-numerische __Glasfaser-HomeID__ auf die Glassteckdose.

Ab diesem Moment ist der Glasfaserausbau technisch abgeschlossen.
Jetzt gibts in der Post ein Modem zugesandt, sowie einen Einrichtungslink per E-Mail.

!!! attention

    Zur Einrichtung des Internetzugangs wird Internet benötigt :clown_face:

Auf der Seite des Links wird man Schritt für Schritt durch die Einrichtung des Glasfasermodems gelotst.
Das ganze beginnt mit der Eingabe der Glasfaser-HomeID von der Glassteckdose, gefolgt von der ID des Modems das angeschlossen wird.
Danach geht am Modem das Blinklicht in ein Dauerlicht über und der Router kann angeschlossen werden.
Dabei sei gesagt, der Router braucht möglicherweise noch nicht einmal Zugangsdaten, wenn der Easy-Login der Telekom aktiv ist, was standardmäßig der Fall zu sein scheint.
Internet gibts dann jedenfalls durch einen PPPoE Tunnel vom Router übers Modem.

Die für den technisch interessierten Kunde offensichtliche Abfolge der technischen Punkte ist also:  
__Verteilerkasten - Hausübergabepunkt - Glasfasersteckdose - Modem - Router__

## Die verschiedenen Szenarien

Es gab verschiedene Optionen den Glasfaserausbau zu bekommen:
Fürs Eigenheim mit der Verpflichtung auch einen Vertrag nach erfolgtem Abschluss zu buchen, oder als Vermieter.

Der Unterschied dabei ist, dass bei der Option fürs Eigenheim im selben Ausbauauftrag das Einführen der Glasfaser ins Gebäude als auch der einzelne Teilnehmeranschluss steckt, während es bei der Vermieter Option bereits mit dem Hausübergabepunkt endet.
Bei der Vermieter Option muss dann jeder Mieter, der von der Glasfaser Gebrauch machen will, einen eigenen Vertrag abschließen in dessen Rahmen der letzte Teilabschnitt von Hausübergabepunkt in die Wohnung abgedeckt wird.

So viel vorweg: dieser Unterschied trägt auch irgendwie zum hier entstandenen Chaos bei.

## Das Problemgebäude

Die aktuelle Misere die mich zu dieser textuellen Aufarbeitung des ganzen getriggert hat fand/findet im Haus meiner Tante statt.
Es ist ein Eigenheim, also die erste der beiden oben genannten Optionen: Vom Verteiler bis in die Wohnung und Vertrag in einem einzigen durchgängigen Bauauftrag.

Das Verlegen in der Straße, das Einführen ins Gebäude und Setzen des Hausübergabepunktes verliefen recht ereignislos.
Aber ab da sind komische Dinge passiert und es ist lange liegen geblieben, sich manuell um Korrektur/Weiterarbeit zu kümmern.
Weiter als der Hausübergabepunkt wurde nicht ausgebaut, also wie bei der Option Vermieter, ironischerweise kam aber ein Modem in der Post und eine E-Mail mit Einrichtungslink.
Dabei war noch nicht einmal ein Anschlusspunkt, aka Glasfasersteckdose, verbaut, an der das Modem angeschlossen werden muss. :clown_face:

### Fehlkommunikationen

Wir haben eine recht klare Vermutung WO/WODURCH das ganze ursprünglich vom Weg abgekommen ist:
Das Gebäude wird von einer einzelnen Partei bewohnt. Mit entsprechender räumlicher Trennung und der Einliegerwohnung gäbe es aber potenziell die Möglichkeit drei unabhängige Teilnehmer im Gebäude zu haben.
Daher haben wir den Bauigeln die den Hausübergabepunkt gesetzt haben bei der Frage auch gesagt, dieser soll für mindestens 3 potenzielle Anschlüsse ausgelegt sein.

Es wurde dann einer mit vier Fasern gesetzt.
Danach haben die Bauigel den Glasfaserauftrag wohl als erledigt gesehen und auch im System als abgeschlossen markiert.
Vermutung hier: Da der Ausbauauftrag im System einer für Eigenheime war an dessen Ende auch direkt ein vereinbarter Tarif stand, war für die Telekom der Anschluss betriebsbereit hergestellt, nachdem die Bauigel den Auftrag als erledigt markiert haben.
Daher ging wohl auch das Modem in die Post und nach einiger Zeit auch Anrufe raus, man solle das Modem doch bitte endlich in Betrieb nehmen.  

Aber warum haben die Bauigel jetzt den Auftrag als erledigt angesehen?
Wir vermuten aufgrund der Anzahl der im Hausübergabepunkt eingezogenen Fasern.
Es scheint so, als würden bis zwei Fasern als Eigenheime gesehen, aber ab drei aufwärts ging man von einem Vermieter-Ausbau aus. Damit wurde in diesem Gebäude wohl fälschlicherweise davon ausgegangen, dass nach dem Hausübergabepunkt nichts weiter auszubauen wäre, obwohl der Auftrag das eigentlich anders vorgesehen hatte.

### Und jetzt?

Der alte Glasfaserauftrag war mittlerweile so alt UND als erledigt markiert, dass dieser im System schon nicht mehr vorhanden war.
Schließlich gibt es keinen Grund mehr Daten dazu aufzubewahren, wenn alles erfolgreich abgeschlossen ist :information_desk_person:  
Als Kunde der Telekom ruft man natürlich dort an, um wieder Bewegung in die Sache zu bringen.
Die Bauigel sind ja bereits lange nicht mehr in den Straßen zu sehen und wen sonst sollte man mit seinen Problemen behelligen, als den Vertragspartner?

Ich habe also über die Hotline einen Technikertermin vereinbart, um das fehlende Stück Hardware endlich mal zu verbauen:
Vom Hausübergabepunkt das Glasfaser in die Wohnung und eine Anschlussdose dran an der das Glasfasermodem in Betrieb genommen werden kann.  
Dieser Termin hat an sich auch einwandfrei geklappt: Terminbestätigung, Technikerin kam und die Glasfasersteckdose wurde von dieser auch physisch verbaut.

Das Problem: Es gab ja keinen aktiven Glasfaserauftrag mehr.
Mangels dessen konnte an der verbauten Glasfasersteckdose keine Abschlussmessung durchgeführt werden und auch keine Home-ID zugewiesen werden.
Ich habe also parallel zur physischen Herstellung der Leitung nochmals mit der Hotline konferiert und es wurde ein neuer Glasfaserauftrag angelegt.
Da das allerdings nicht innerhalb von 5 Minuten in allen Ecken des Systems ankommt, sondern 1-2 Werktage dauert, war das innerhalb des Besuchs der Technikerin leider nicht mehr hilfreich :grimacing:

Nach dem physischen Setzen der Glasfasersteckdose habe ich dann fast zwei Wochen versucht der Hotline eine Glasfaser-HomeID zu entlocken und einfach auf die abschließende Messung zu verzichten.
Vergeblich.
Der Ausbaustatus des neu angelegten Glasfaserauftrags steht auf dem Punkt, dass die Glasfasersteckdose noch fehlt.
Und keiner an der Hotline konnte oder wollte dieses auf Zuruf ändern.
Ich habe auch nochmals versucht einen Technikertermin anzuberaumen.
Dieses Mal allerdings ohne Erfolg: Es gab keine Terminbestätigung und es tauchte letztlich auch niemand im genannten Zeitraum auf.

### Keiner aufgetaucht, keiner Zuständig?

Nach weiteren Telefonaten mit der Hotline bin ich tatsächlich an jemanden geraten, die die Gesamtsituation offenbar recht zügig erfasst hat, einige Schlüsse daraus gezogen, sich derer nochmals vergewissert hat und mir anschließend zurückgerufen hat.

Die Telekom nutzt verschiedene Subunternehmer bei ihren Ausbauten.
Die Technikerin, die uns die Glasfasersteckdose einfach mal an die Wand geschraubt hat, hat damit dem Subunternehmer quasi vorgegriffen, denn der Termin war im System eigentlich als einer zur Entstörung deklariert^^

Das Ding ist: die Subunternehmer arbeiten die Aufträge eben auch schrittweise ab.
Da der jetzige Auftrag ja erst neu angelegt wurde, kann das eben unter Umständen auch etwas dauern, bis die da mal auf einen zukommen.
Die kompetente Person in den Untiefen der Hotline hat mir allerdings zumindest mal gesagt welche Firma in unserem Gebiet zuständig ist und wie ich die erreichen kann.
Aufgrund des jetzt physisch bereits recht weit geratenen Zustandes hat die Dame bereits per Mail dort angefragt, ob sie uns vorziehen könnten und wollte sich dann nochmal bei mir melden.
Mal sehen was draus wird.

## Hat überhaupt jemand einen Plan?!

Was mich an der ganzen Sache eigentlich am meisten nervt, ist gar nicht, dass es eventuell alles lange dauert, oder mehrere Firmen in den Prozess involviert sind.
Das Problem ist die scheinbare Ahnungslosigkeit der Supportmitarbeiter in den Hotlines, widersprüchlicher Informationen die ausgegeben werden, völlig unterschiedliches Abfrageverhalten irgendwelcher Daten, etc.

Daher hier jetzt einige Beispiele dessen.

### Modem anschließen, ohne Anschlussdose

Dass Fehler passieren ist menschlich, dass also die Bauigel einfach aufgrund der Größe des montierten Hausübergabepunktes den Auftrag als erledigt markiert haben, obwohl aus den Auftragsdaten eigentlich weitere Ausbauschritte hätten ersichtlich sein müssen (so hoffe ich doch), seis drum.
Aber in den Systemen der Telekom ist dadurch ja dennoch nicht magisch eine Glasfasersteckdose aufgetaucht.
Also wie kann es sein, dass ohne eine im System existierende Anschlussdose und ohne generierte Glasfaser-HomeID ein Modem in die Post geht?
Und obendrein: Wie kann nach einigen Monaten stillstand ein Servicemensch aktiv bei meiner Tante anrufen, diese auffordern endlich das Modem in Betrieb zu nehmen und auf den Hinweis, dass da ja noch ein Ausbauschritt fehlt, nicht erkennen, dass im System noch gar keine Glasfastersteckdose hinterlegt ist und keine Glasfaser-HomeID existiert?

Eigentlich würde ich erwarten, dass spätestens bei der aktiven Aufforderung zur Inbetriebnahme, was ja offensichtlich eine manuelle Intervention war, jemand mal genauer hinsieht und dann auch erkennt, dass hier noch etwas fehlt und selbstständig die Korrektur dafür in Gang setzt.
Aber stattdessen ist nichtmal nach der direkten Information meiner Tante, dass da noch ein Teil fehlt, kein Ticket dafür erstellt worden, oder ein neuer Glasfaserauftrag angelegt worden.
Es ist einfach _nichts_ passiert.

### Zuständigkeits-Schwarzer-Peter

Es gibt etliche verschiedene Hotlines bei der Telekom.
Da verliert man manchmal irgendwie den Überblick, wen man in welcher Situation/Ausbauphase jetzt anrufen sollte.
Beispiele: Hotline für Glasfaser, Gigabit, Bauherren, allgemeiner Kundenservice :shrug:

_Manchmal_ wenn man beim Bauherrenservice anruft und eine Glasfaser-Auftragsnummer nennt, bekommt man zu hören "Da sind die Kollegen von der Glasfaserhotline zuständig".
_Manchmal_ wird auch auf die Gigabit-Kollegen verwiesen.  
Von denen bekommt man nach kurzem Blick in den Auftrag und der Realisierung, dass der physische Ausbau noch gar nicht beendet ist, dann aber zu hören "Solange keine Abschlussmessung vorliegt und die Leitung als einwandfrei freigegeben ist, können wir da nichts machen und die vom Bauherrenservice sind zuständig".
Einen Technikertermin vereinbaren, um die Abschlussmessung durchzuführen, könne man auch nicht.
Das müssten/dürften ebenfalls nur die vom Bauherrenservice, bitte dort nochmal anrufen. :clown_face:

### Kundenlegitimations-Bingo

Meine Tante ist nicht wirklich technisch bewandert, deshalb kümmere ich mich um den Kram.
Um das zu können hab ich verschiedene eingegangene E-Mails exportiert, wie Auftragsbestätigungen, Auftragsnummern, etc.
Dazu habe ich eine Keepass-Datei angelegt mit den Zugangsdaten fürs Online-Konto, den Auftragsnummern und allen Daten, die ich brauche, um mich als befugt legitimieren zu können (Kundennummer, Geburtsdatum, Kontonummer, ...).

Bei jedem Anruf und vor allem auch bei jeder Weiterleitung innerhalb des Anrufes :clown_face: fängt man wieder von vorne an erstmal seine Geschichte zu erzählen, irgendeine Nummer wie Kundennummer oder Auftragsnummer zu nennen und dann darf man gespannt sein, welche Daten das Gegenüber dieses mal zur Legitimation abfragen will.
Mal interessiert es keinen, häufig sind es Kundennummer und die letzten vier (manchmal auch sechs) Ziffern der IBAN.

Wenn man dann mal den Fehler macht sich am Telefon statt einfach mit "Hallo", mit dem eigenen Nachnahmen zu melden, passiert es immer mal, dass jemand verwirrt ist, weil der Anschlussinhaber ja anders heißt.
Meistens ist das zwar schnell erledigt, indem man wahrheitsgetreu erklärt, dass es sich um den Anschluss von Verwandten handelt und die üblichen Legitimierungsfragen durchgeht.  
Einmal bin ich aber an einen gekommen, der dann der Meinung war mit mir ein elends langes Quizz spielen zu müssen und alles was er so finden konnte abzufragen: Anschrift, E-mail, Geburtsdatum, Kundennummer, IBAN; :face_with_raised_eyebrow: nur um am Ende dennoch nicht helfen zu können.

### Nur der Anschlussinhaber!!

Ein weiteres Highlight war eine Begegnung dieser Art, bei der man sich am Ende darauf geeinigt hatte, dass man ein Ticket einstellen könnte den momentanen Zustand mal zu erörtern, wodurch je nach Ergebnis das Herstellen der fehlenden Teile angestoßen werden würde.
Der Haken dabei: Um dieses Ticket zu erstellen, MUSS aber mit dem Anschlussinhaber gesprochen werden.
Ohne dessen persönliches GO ginge das nicht.
Dass ich alle Daten vorweisen konnte, die zur Legitimation abgefragt werden, war völlig belanglos, weil es war ja nicht mein Vertrag.

Ich habe versucht zu argumentieren, dass es doch selbst ganz ohne ein Gespräch mit irgendjemanden möglich sein sollte, ein Ticket anzulegen, wenn man einen Missstand erkennt, um diesen zu beseitigen.
Schließlich ging es nicht um eine Kündigung oder dergleichen, sondern eigentlich nur darum, dass man endlich mal seinen vertraglich vereinbarten Pflichten nachkommt.
Hat aber nichts geholfen, das Gespräch mit diesem Mitarbeiter blieb ergebnislos.

An diesem Punkt ist es dann doch von Vorteil, dass man bei jedem Anruf wieder woanders rauskommt und man kann es einfach beim nächsten Bauernopfer versuchen :shrug:

Es stellt sich letztlich aber doch die Frage, wie sich derjenige das vorgestellt hat:
Ich könnte einfach _irgendjemand_ anderen ans Telefon holen, der sich als Anschlussinhaber ausgibt.
Wie soll das geprüft werden?
Die einzige wirklich tragfähige Option wäre ein Videomeeting anzuberaumen, in dem der Anschlussinhaber mit seinem Ausweis neben seinem Gesicht in die Kamera winkt.
Alles, was aber von den Hotlinern an Aufwand vertretbar wäre, ist letztlich doch auch nur Augenwischerei.

Die Konsequenz daraus: Man meldet sich bei zukünftigen Anrufen einfach direkt mit dem Nachnahmen des Anschlussinhabers. :shrug:
Catch me if you can :eyes:

### Inkonsistente Informationen

Nachdem nun die Glasfasersteckdose physisch verbaut ist, aber ich mehrfach daran gescheitert bin rein durch Anrufe die Abschlussmessung zu überspringen und eine Glasfaser-HomeID zu erhalten, habe ich beschlossen einfach nochmals einen Techniker herzubeordern um zu messen und das endlich zu erledigen.
Am Telefon wurde mir dann ein Datum genannt, etwas von "dazwischen schieben" gemurmelt und als Zeitraum 8-16 Uhr an dem Tag genannt.
Typisches Klischee irgendwie, oder?
Gut, dass ich mir das mit meiner Teilzeit aktuell leisten kann, einfach komplette Tage auf fremden Sofas zu vertrödeln :clown_face:

Das komische schon direkt zum Tagesbeginn: Es gab keine SMS, die den Termin nochmals bestätigt hätte und einem Link, um den Abstand des Technikers einzusehen wie beim Termin zwei Wochen zuvor.  
Als um 14 Uhr noch niemand da war, habe ich wieder den Hörer geschwungen, um nachzuhaken, ob der Termin im System denn ersichtlich sei.
Dieser Hotliner hat den Termin gefunden, die richtige hinterlegte Rückrufnummer vorgelesen und mitgeteilt der Techniker käme noch.  
Zwei Stunden später, um 16 Uhr, war der genannte Zeitraum abgelaufen und niemand aufgetaucht.
Bei einem weiteren Anruf um nun zu klären, was aus dem Termin jetzt geworden ist, konnte dieser Hotliner allerdings keinen Termin mehr finden :shrug:
Ist der einfach schlag 16 Uhr aus dem System gefallen?
Oder schaut da etwa einfach jeder an völlig unterschiedlichen Stellen ins System und es ist eine Lotterie, ob man jemand kompetenten oder ahnungslosen erwischt??

### Weiterleitung bis in die Sackgasse

Während der Jagd nach dem Grund des verschollenen Technikertermins bin ich mehrere Male weitergeleitet worden.
Ziel war irgendwann "direkt die Dispo".
Beim ersten Versuch bin ich aber wohl vom Weg abgekommen.
Der Mensch am anderen Ende wirkte etwas verwirrt und fragte mich dann, ob ich hierher weitergeleitet worden sei.
Letztlich erklärte er mir dann hier aber falsch zu sein und ich müsste nochmal neu anrufen.
Ich hab erst gelacht und entgegnet nicht wieder von vorne anzufangen, er möchte mich doch bitte wieder zurück verbinden oder alternativ direkt zur Techniker-Dispo, wo es ja eigentlich hingehen sollte.
Problem dabei: ich glaube derjenige gehörte eigentlich nicht mal annähernd zum Telefon-Support Personal und war technisch gar nicht in der Lage mich wieder in die Hotline-Gefilde zu verbinden :grimacing:

### Gesamtsituation erfassen, anyone?

Zweiter Versuch mehr über den geplatzten Termin herauszufinden hat die Verbinderei wohl geklappt.
Nach dem Gespräch mit dieser engagierten Supporterin die offensichtlich einen sehr guten groben Überblick darüber hatte, wie Dinge generell ablaufen, wie die Zusammenhänge sind, etc., stellt sich mir die Frage:  
Wie kann es sein, dass es ein dutzend Anrufe braucht, um am Ende durch reines Glück im Umfeld der Techniker-Disposition eine Person zu finden, die wirklich kompetent scheint und recht zügig klare Aussagen über den Stand der Dinge treffen kann?!

Die Mitarbeiterin dort hat sich meine Geschichte angehört und hatte allein davon bereits direkt eine Vermutung, was wohl passiert sein könnte.
Sie hat mich dann kurz auf die Seite gelegt, um ein paar andere Leute anzurufen.
Aufgrund des fortgeschrittenen Abends, war allerdings nicht mehr wirklich jemand erreichbar um da jetzt definitive Aussagen zu bekommen.
Also hat sie mich gefragt, ob sie mich am nächsten Tag zurückrufen dürfe, sie will ein paar Vermutungen checken und meldet sich dann.

Normal bin ich ja recht reserviert mit Rückrufen, weil ich bei den 0815 Supportern irgendwie nicht wirklich Vertrauen darin habe, dass der Rückruf auch tatsächlich stattfindet.
Wurde mit solchen Versprechen schon viel zu oft einfach geghostet, egal ob Rückruf oder "ich schick ihnen das gleich per Mail".
Bei ihr hatte ich aber aus dem Gespräch den Eindruck, dass ich jemand hab, der tatsächlich eine gute Vorstellung im Kopf des Szenarios hat und da auch tatsächlich am Ball bleiben will.
Und dann ist es letztlich tatsächlich besser man beendet das aktive Gespräch, sie kann in Ruhe ihre Recherchen machen und wenn sie alles hat, was wie wollte, in Ruhe zurückrufen, statt mich die ganze Zeit aktiv in der Leitung zu halten und unter diesem Druck nachforschen zu müssen.

Der Rückruf am nächsten Tag kam wie versprochen und brachte mir einen Blick auf die Gesamtlage, der sooooo viel hilfreicher war, als alles was mir die vielen anderen Hotliner zuvor zu der Auftragsnummer so gesagt haben.
Wie kann es sein, dass keiner der anderen denen ich mein Leid geklagt habe, mit der Auftragsnummer im System sieht/erkennt, dass der Auftrag einem Subunternehmen zugeschlagen ist und einen darauf verweist?
Stattdessen wird man mit sinnlosen Phrasen wie "Aufklärungsticket", "einfach noch Geduld haben", etc. vertröstet.
Aber wirklich mehr weiß man am Ende erst nicht und hat nur wieder eine Menge Lebenszeit verschwendet.

## Status Quo

In unserem Ausbaugebiet wird ein Subunternehmer für die Glasfaseraufträge eingesetzt, diese bearbeitet die Telekom _nicht_ selbst.  
Die Technikerin die kam und uns dann physisch die Glasfasersteckdose gesetzt hat, obwohl zu diesem Zeitpunkt ja gar keine Auftragsnummer mehr bestand, auf die man das hätte im System hinterlegen können, kam zu uns für eine "Entstörung".
Interessante Info nebenbei, denn mir war das letztlich ja egal _wie_ der Termin im System gelabelt wurde, Hauptsache jemand kam und hat die Installation näher an den Endzustand gebracht.

Der parallel neu angelegte Glasfaserauftrag wird dem in unserem Ausbaugebiet eingesetzten Subunternehmen zugeschlagen.
Die Anbringung der Glasfasersteckdose durch die vor Ort befindliche Technikerin hat an dieser Stelle also quasi dem Subunternehmer vorgegriffen, der das eigentlich machen würde.
Da der ursprüngliche Auftrag ja durch einen kleinen Denkfehler zu früh erledigt erklärt und der neue erst angelegt wurde, könnte das erstmal etwas dauern, bis der Subunternehmer diesen Auftrag zum Abarbeiten aufgreift.

Ich habe jetzt Kontaktdaten um mich direkt mit dem Subunternehmen abzusprechen.
Vielleicht ist es ja möglich, dass sie den Auftrag nach vorne ziehen, nachdem ja eigentlich schon quasi *fast* alles gemacht ist.
Aber selbst wenn nicht, gibts hoffentlich wenigstens einen Zeithorizont, mal sehen.

Update folgt dann :wink:
