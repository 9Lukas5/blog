/boot
└── efi
    ├── EFI
    │   ├── BOOT
    │   │   └── BOOTX64.EFI
    │   ├── Linux
    │   └── systemd
    │       └── systemd-bootx64.efi
    └── loader
        ├── entries
        ├── entries.srel
        ├── loader.conf
        └── random-seed
