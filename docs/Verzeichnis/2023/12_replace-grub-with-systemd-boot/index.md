---
tags:
    - HowTo
    - Tutorial
    - Linux
    - Bootloader
    - Grub
    - Systemd
    - Systemd-Boot
    - Ubuntu
    - Fedora
    - Debian

title: Replace Grub with Systemd-Boot
description:
    How to replace Grub in your existing Ubuntu, Fedora or Debian installation.
date: 2023-07-29 22:30
update: 2023-07-29 22:30

comments:
    host: mastodontech.de
    username: 9Lukas5
    id: 110799305056080822
image: systemd-boot-menu.avif
list_entry: true
---

# {{ title }}

![Systemd-Boot bootmenu on my multi-distro vm](./systemd-boot-menu.avif){ loading=lazy }

<p class="introduction" markdown>
<span class="date">{{ date }}</span>  
{{ description }}
</p>

---

Right now it seems the broadest adopted bootloader on Linux distros is Grub.
As for audio it was Pulseaudio and for graphics stuff X11.  
Since I'm already using Wayland for a while, switched my audio stack to Pipewire and stumbled across the topic Systemd-Boot again, I went down the rabbit whole of getting it to work on my devices :face_with_peeking_eye:  
As a result you're praised with this blog entry :partying_face:.
In fact, it's more in the style of a tutorial than a story.
Hopefully you can convert your systems with less struggle/research, than I had :zany_face:

!!! info "Disclaimer/Shoutout"
    First of all, I'd like to thank [@sebastiaanfranken@mastodon.nl][sebastiaanfranken-mastodon] and the GitHub repo [systemd-boot-conversion][gh-systemd-boot-conversion], from which the majority of my gained knowledge, and thereby the content of this post, originates from.

    In the repo you'll find scripts, which do pretty much automatically for Fedora, what I'm about to describe here to do manually step by step.

## What is this even about?!

![GUID partition table scheme](./GUID_Partition_Table_Scheme.svg){ align=right style="background: white; max-width: 50%" data-description=".guid-table-scheme-desc" loading=lazy }
<div class="glightbox-desc guid-table-scheme-desc">
    CC-BY-SA 2.5 <a href='https://en.wikipedia.org/wiki/User:Kbolino'><u>Kbolino</u></a>, 
    (<a href='https://en.wikipedia.org/wiki/GUID_Partition_Table#/media/File:GUID_Partition_Table_Scheme.svg'><u>downloaded from Wikipedia</u></a>)
</div>

In order to use a working operating system it has to be loaded somehow.
This is done using a bootloader, which can be loaded from the motherboard, which then will do the initial load of your actual operating system and make it boot up.

Formerly™, hard disks where formatted with a so-called Master Boot Record partitioning scheme.
This relied on the bootloader being placed at a very specific place on the disks start.
The motherboard would then be able to load this, and only this, bootloader from that disk.[^wiki-disk-partition]

Today everyone has an UEFI capable system and uses a GUID(GPT) partitioning scheme.
With this, a dedicated partition is used.
This has to be FAT-formatted and set a specific GUID.
On this partition one (or multiple) bootloader(s) can then be installed using `EFI`-loader files.[^wiki-efi]

Grub is the bootloader seemingly every distro is shipping with and supports both things.
The new kid on the playground is Systemd-Boot, only supporting UEFI systems.  
This post is basically about actively replacing the old bootloader with a newer one, not waiting for the distros to do the change by default.


## Do I need this :face_with_monocle:

If your system is booting fine and you just want it to work, not unnecessarily fiddling around with anything just for the sake of loading your OS with the newest tool available: __No__.  
_But_: If you'd like to ~~invest~~ waste your time changing your systems way of booting, just for the fun in breaking and then desperately needing to fix it again: Go on :face_with_cowboy_hat:

## Prerequisites

I tried this now on various distros.
I'd say the following are the bare minimum requirements:

!!! warning "no liability assumed"
    It's totally possible I'm wrong about this list.
    Let's just hope not :zany_face:

- UEFI
- disk formatted in GPT (Systemd-Boot doesn't support MBR partitioned disks)
- `bootctl` installed on your system
- `/usr/lib/systemd/boot/efi/systemd-bootx64.efi` installed/installable on your system

## Pre-/Post situation explained

Assuming an installation booting with GRUB, not yet setup with/for Systemd-Boot, certain files should be there and others not be.
Also, the partitioning layout could maybe differ for both approaches, depending on how you set up your systems up to now.
So, let's have a look onto my setup and what will change throughout the switch.

- __Partitioning layout__

    === "Pre-Switch with GRUB"

        My traditional partitioning layout I apply to basically every installation looks something like this:

        ~~~text
        {% include "./lsblk-before.txt" %}
        ~~~

        I normally use a encrpyted partition for my OS and data, so in order to be able to boot the system I have to keep my `/boot` partition seperate and unencrypted.

        In this case the kernel and initram files reside under `/boot`, while the ESP mounted under `/boot/efi` only contains the Grub-EFI loader and a Grub-config, redirecting to the actual Grub-config on `/boot`

    === "Post-Switch with Systemd-Boot"

        After switching, a dedicated `/boot` partition is no longer needed, but it's space is required for the ESP-partition:

        ~~~text
        {% include "./lsblk-after.txt" %}
        ~~~

- __Bootloader Files__

    === "Pre-Switch with GRUB"

        Here are the files I got in a pretty normal Grub installation with the given partitioning layout:

        ??? example "tree of `/boot`"
            ~~~text
            {% include "./tree-boot-before.txt" %}
            ~~~

        Now let's untangle what we got there:

        === "`/boot/efi/EFI/ubuntu/grub.cfg`"

            This is basically just a trampoline to the `/boot` partition and the config there.

            ~~~sh
            {% include "./grub-efi-grub.cfg" %}
            ~~~

        === "`/boot/grub/grub.cfg`"

            ![Grub bootloader menu top page](./grub-screenshot-bootmenu-top.avif){ align=right style="max-width:33%" loading=lazy }
            ![Grub bootloader menu submenu page for 'Advanced options for Ubuntu'](./grub-screenshot-bootmenu-sub.avif){ align=right style="max-width:33%" loading=lazy }

            This is the actual grub configuration, resulting in the bootmenus on the right:<br><br><br><br><br>

            ??? example "file content"
                ~~~sh
                {% include "./grub-boot-grub.cfg" %}
                ~~~

        === "`/boot/{vmlinuz*,initrd.img*}`"

            These are the actual kernel and ramdisk files, Grub loads as defined in the menuentries.

    === "Post-Switch with Systemd-Boot"

        After the switch the `/boot` and ESP-partition contain comparably few files:

        ??? example "tree of `/boot`"
            ~~~text
            {% include "./tree-boot-after.txt" %}
            ~~~

        What did we get now:

        === "`/boot/efi/loader/loader.conf`"
            This is the configuration file for Systemd-Boot.
            Things like the menu timeout can be set here.
            By default it's basically empty, just providing a few commented options:

            ~~~properties
            {% include "./loader-conf-after.txt" %}
            ~~~

            For a list of available options, see the [loader.conf reference][loader-conf-reference]{ target="_blank" }.

        === "`/boot/efi/loader/entries/*`"
            This directory contains a configuration file _per bootable option_.
            Each machine has a dedicated Machine-ID (found in `/etc/machine-id`), which will be the start of the filename.

            ![Systemd-Boot bootloader menu for the shown example loader configuration](./systemd-boot-menu-ubuntu.avif){ align=right style="max-width:33%" loading=lazy }

            ~~~text
            {% include "./loader-entry-ubuntu.txt" %}
            ~~~

            These loader-entries contain just a few configurations, giving Systemd-Boot a bit metadata and what to load with which options.
            The filepaths are always seen from the root of the ESP-partition.

        === "`/boot/efi/<machine-id>`/*"
            Each os will create a folder named by its machine-id on the ESP-partition.
            In there the actual kernel files will then be placed, the loader-entries will point to.

        !!! note "Ubuntu stores kernel files twice"
            To successfully boot, it would be enough to have the kernel files in the ESP-partition.
            Ubuntu keeps putting them in the `/boot` directory anyways, so the data sits twice on disk.

- __Kernel installation hooks__

    On each install/update/remove of a kernel, some actions are performed.
    I came across a few folders while doing this, in which these kernel-hooks reside.

    === "Pre-Switch with GRUB"

        Following is a tree-output of how these looked on a fresh Ubuntu install before doing anything:

        ??? example "tree of `/etc/kernel`, `/etc/initramfs` and `/lib/kernel`"
            ~~~text
            {% include "./tree-kernel-hooks-before.txt" %}
            ~~~

    === "Post-Switch with Systemd-Boot"

        After the switch, we got a new file under `/etc/kernel` called `cmdline`.
        This file contains the options passed to the kernel on boot.

        The post-installation and remove hooks lost there scripts for shim and the grub configuration update, while gaining scripts for Systemd-Boot.
        Additionally, the previously not existing folder `/etc/initramfs` got an update hook for Systemd-Boot.

        ??? example "tree of `/etc/kernel`, `/etc/initramfs` and `/lib/kernel`"
            ~~~text
            {% include "./tree-kernel-hooks-after.txt" %}
            ~~~

- __Bootctl status__

    `bootctl` manages the Systemd-Boot installation.
    Run the following command to see what the current state on your system is:

    ```sh
    sudo bootctl status
    ```

    === "Pre-Switch with GRUB"
        ~~~text
        {% include "./ubuntu-bootctl-status-before.txt" %}
        ~~~

    === "Post-Switch with Systemd-Boot"
        ~~~text
        {% include "./ubuntu-bootctl-status-after.txt" %}
        ~~~

## Switching to Systemd-Boot

The steps are basically:

- place the right configurations and hooks
- remove Grub
- _(optional)_: change partitioning layout
- install Systemd-Boot bootloader to ESP partition
- reinstall kernels
- check/create/update EFI-bootentries

### Configurations and Hooks

One very important configuration is your __kernel command line__.
These are the commands/parameters passed to your kernel about your system on boot.
E.g., where the root partition is and some other stuff, like things you maybe have configured through your `/etc/default/grub`.

What the _current_ one is, can be found out in the file `/proc/cmdline`.
Another place could be the menuentry of your current Grub-config in the boot directory.

So, check what yours should be and add the content to a new file under `/etc/kernel/cmdline` in case it's missing.
Taken from the Grub menuentry example above, mine would look like this:

!!! example "/etc/kernel/cmdline"

    ```sh
    root=UUID=585f9e8c-0116-434d-96d7-799052f8f0c4 ro rootflags=subvol=@  quiet splash $vt_handoff
    ```

=== "Ubuntu <=22.04/Generic Debian based"

    !!! info "Debian based"
        This *could* work, on any Debian based system, as long as the prerequisites are met.

    Without the `systemd-boot` package, available from Ubuntu 22.10 onwards, the configurations and kernel-hooks have to be placed manually.
    I have just taken the files from the Ubuntu 22.10 package and used them in 22.04 :shrug:

    Create the following files:

    ??? "`/etc/initramfs/post-update.d/systemd-boot`"

        ~~~sh
        {% include "./ubuntu-systemd-boot-hook-postupd.sh" %}
        ~~~

    ??? "`/etc/kernel/postinst.d/systemd-boot`"

        ~~~sh
        {% include "./ubuntu-systemd-boot-hook-postinst.sh" %}
        ~~~

    ??? "`/etc/kernel/postrm.d/systemd-boot`"

        ~~~sh
        {% include "./ubuntu-systemd-boot-hook-postrm.sh" %}
        ~~~

    !!! warning "make executeable"
        Don't forget to make them executeable, in order for them beeing picked up.

=== "Ubuntu >=22.10/Debian >=11"

    !!! info "Debian"
        I tested this with Debian 12, but the package archive has it already for Debian 11.

    Providing a `systemd-boot` package in its package-repos bringing in all the hooks, with newer Debian based systems it is pretty simple to get them in place:

    ```sh
    sudo apt install systemd-boot
    ```

    ??? example "diff of tree output after installation"
        ~~~diff
        {% include "./ubuntu-after-apt-install-systemd-boot.diff" %}
        ~~~

    After the installation, you should have these hook-scripts in your system, as the diff shows.
    No manual intervention should be necessary.

=== "Fedora >=36"

    !!! info "Fedora 36"
        Fedora 36 doesn't have the package systemd-boot-unsigned, but it's not needed either.
        The EFI-loaders are already in the system after a fresh install. :shrug:

    Fedora also provides a package:

    ```sh
    sudo dnf install systemd-boot-unsigned
    ```

    But: unlike the Debian based derivates, this package does not make _any_ changes to the installed kernel hooks :face_with_monocle:

    Looking into what this package actually provides, it can be seen that it indeed does not bring any config files.
    This raises the question, if the package is acutally needed :eyes:.
    And the answer is: Yes.
    Because what we can see, and I checked it's not also provided by something else anyways, it provides the Systemd-Boot EFI-loader `systemd-bootx64.efi`.
    Without this, bootctl can't install the bootloader to the ESP-partition.

    ??? info "`systemd-boot-unsigned` provided files"
        ~~~text
        {% include "./fedora38-systemd-boot-provided-files.txt" %}
        ~~~

    The required configurations have to be created manually.
    That's where, once again, the shoutout goes to [@sebastiaanfranken@mastodon.nl][sebastiaanfranken-mastodon], from where I put together my setup for Systemd-Boot.

    Create the following files:

    ??? "`/etc/kernel/install.conf`"
        If it already exists, just append it/ensure the settings:

        ```properties
        layout=bls
        ```

    ??? "`/etc/dnf/plugins/post-transaction-actions.d/systemd-udev.action`"

        ```properties
        systemd-udev:in:bootctl update
        ```

    Create the following links to `/dev/null` in order to disable system provided scripts from `/lib/kernel/install.d`:

    !!! info "20-grubby.install"
        This is not needed everywhere, but at least on Fedora 36 it is still there after removing the grubby package, as it's provided by some udev-package.

    ```sh
    sudo ln -s /dev/null /etc/kernel/install.d/20-grubby.install
    ```

    Now, make sure the program `objcopy` is installed.
    If not:

    ```sh
    sudo dnf install binutils
    ```

### Remove Grub

!!! danger "Non-bootable system"
    After removing Grub, your system CANNOT boot anymore, until you are through with the Systemd-Boot setup _or_ reinstalling and configuring Grub again.

    If, for some reason, the system is shut down, you should have a live system prepared to continue from there.

Before placing any new files, do a clean-up of the old bootloader packages and its files.

=== "Ubuntu/Generic Debian based"

    - purge grub and shim packages

        ```sh
        sudo apt purge "grub*" "shim*" --allow-remove-essential
        ```

        After doing this, and choosing to remove Grub's files from `/boot`, the diff shows, it acutally didn't _completely_ nuke the `/boot/grub` directory :clown_face:

        ??? example "diff of tree on `/boot`"
            ~~~diff
            {% include "./ubuntu-tree-boot-after-grub-purge.diff" %}
            ~~~

        On the kernel hooks we can see that it removed them :+1:

        ??? example "tree of `/etc/kernel`, `/etc/initramfs` and `/lib/kernel`"
            ~~~diff
            {% include "./ubuntu-tree-kernel-hooks-after-grub-purge.diff" %}
            ~~~

    - cleanout other leftover Grub files

        There are still the Grub-EFI loader on the ESP-partition and the leftover folder on `/boot`.

        !!! danger "removes other bootloaders"
            The command below removes ALL bootloaders from the ESP partition.
            So be careful in case you have some special setup holding multiple EFI-loaders there.

        ```sh
        sudo rm -rf /boot/grub
        sudo rm -rf /boot/efi/EFI
        ```

=== "Debian >=11"

    In my test, unlike Ubuntu, Debian not only installed the Systemd-Boot package, but also triggered the bootloader installation to the ESP-partition.
    So, removing Grub you shouldn't go full nuke mode on the ESP-partition^^

    Therefore, you should just remove the Grub packages and the dedicated leftover directories:

    ```sh
    sudo apt purge "grub*" "shim*" --allow-remove-essential
    sudo rm -rf /boot/grub
    sudo rm -rf /boot/efi/EFI/debian
    ```

=== "Fedora >=36"

    - remove Grub packages

        ```sh
        # allow removal of these protected packages
        sudo rm -rf /etc/dnf/protected.d/{grub*,shim}.conf
        sudo dnf remove grubby "grub2*" "shim*"
        # don't allow installation in future
        echo "ignore=grubby grub2* shim*" | sudo tee -a /etc/dnf/dnf.conf
        ```

        ??? example "diff of tree of kernel hooks"

            ~~~diff
            {% include "./fedora38-tree-kernel-hooks-grub-removed.diff" %}
            ~~~

    - cleanout other leftover files

        Fedora already had a `loader` folder, but under `/boot/loader`.
        It's important to remove this as well, otherwise this interferes with the planned installation to the ESP-partition.

        Basically, just remove everything under `/boot` and `/boot/efi`.

        !!! danger "nuklear option"
            Be aware that this once again is a very nuklear option.

        ```sh
        sudo rm -rf /boot/*
        sudo rm -rf /boot/efi/*
        ```


### _(optional)_ Change partition layout

In my default partitioning layout I used, the ESP partition was fairly small and `/boot` had the space for the kernels.
After the change the kernels reside in the ESP-partition.
Therefore, I removed the `/boot` partition and created a new ESP-partition over the space of both, the old ESP and `/boot`.

- check the parted output for your disk

    ```sh
    sudo parted /dev/<disk> print
    ```

    Example output:

    ~~~text
    {% include "./ubuntu-parted-pre-change.txt" %}
    ~~~

- unmount `/boot/efi` and `/boot`

    ```sh
    sudo umount /boot/efi
    sudo umount /boot
    ```

- remove ESP and `/boot` partitions

    ```sh
    sudo parted /dev/<disk> rm 1
    sudo parted /dev/<disk> rm 2
    ```

- create new ESP partition from the disk start to the root partition (adjust sizes to your situation)

    ```sh
    sudo parted /dev/<disk> mkpart primary 0% 5400MB
    ```

- set the ESP flag on the freshly created partition

    !!! tip "verify partiton number"
        The newly created partition _should_ have the number `1`.
        But it's never a bad idea to check such things.

    ```sh
    sudo parted /dev/<disk> set 1 esp on
    ```

- format the new partition

    ```sh
    sudo mkfs.fat -n EFI /dev/<disk><part-number>
    ```

- create new empty `efi` folder under `/boot` and mount it there

    ```sh
    sudo mkdir /boot/efi
    sudo mount /dev/<disk><part-number> /boot/efi
    ```

!!! warning "fstab update"
    Don't forget to update your `/etc/fstab` for the new partitioning layout.

### Install Systemd-Boot bootloader to ESP partition

!!! info "Debian"
    As said before: Debian did this automatically on the package installation.
    So, probably check if the files are already present, in which case all the other steps are unnecessary.

Now it's time to install the Systemd-Boot bootloader.
This is done using `bootctl`.

```sh
sudo bootctl install
```

??? example "command output"
    ~~~text
    {% include "./ubuntu-bootctl-install-output.txt" %}
    ~~~

Now, let's check what our file system looks like:

!!! example "`/boot`"

    ~~~text
    {% include "./ubuntu-tree-boot-after-bootctl-install.txt" %}
    ~~~

### Reinstall kernel

With the new bootloader, configurations and kernel hooks in place, the kernel has to be reinstalled in order to create the needed boot entry and place the kernel to the new place.

- Find out your currently installed kernels:

    === "Ubuntu/Debian"

        ```sh
        sudo apt list --installed "linux-image*"
        ```

        !!! example "command output"
            ~~~text
            {% include "./ubuntu-apt-grep-kernels.txt" %}
            ~~~

    === "Fedora"

        It's just `kernel-core`^^

- Reinstall the currently installed one

    === "Ubuntu/Debian"

        !!! attention "Meta-package"
            Do not use the meta-package, but the actual package of the current kernel version

        Example based on the output above:

        ```sh
        sudo apt reinstall linux-image-5.19.0-46-generic
        ```

    === "Fedora"

        ```sh
        sudo dnf reinstall kernel-core
        ```

    After this, the kernel hooks should have been run and our kernel and a loader configuration be placed in the ESP partition.
    Let's see how the file system changed since the `bootctl install`.

    === "Ubuntu/Debian"

        ~~~diff
        {% include "./ubuntu-tree-boot-after-kernel-reinstall.diff" %}
        ~~~

    === "Fedora"

        ~~~diff
        {% include "./fedora38-tree-boot-after-kernel-reinstall.diff" %}
        ~~~

### Check/Create/Update EFI-bootentries

Before rebooting, make sure a new efi-boot-entry was created:

```sh
efibootmgr -v
```

??? example "command output"

    ~~~text
    {% include "./ubuntu-efibootmgr-list.txt" %}
    ~~~

In my case the bootctl installation did in fact add the boot entry `000B` and put it to the front of the boot order.

If your boot entries don't work, `efibootmgr` can also add/remove entries and change the boot-order.
But as the blog post got fairly lengthy already (sorry :face_with_peeking_eye:), I won't include detailed descriptions on how to fiddle around with your EFI boot-entries now.
Maybe I could do that in a follow-up post :thinking:, but not directly here.

!!! warning "magical things"
    Editing your EFI boot-entries can sometimes result in some unexpected stuff.
    I myself setup Systemd-Boot just fine and it booted.
    But when I decided to clean up my orphaned boot-entries, suddenly the one needed went missing, even though it was shown before rebooting.

[gh-systemd-boot-conversion]: https://github.com/sebastiaanfranken/systemd-boot-conversion
[sebastiaanfranken-mastodon]: https://mastodon.nl/@sebastiaanfranken

[^wiki-disk-partition]: [Wikipedia: Disk partitioning](https://en.wikipedia.org/wiki/Disk_partitioning)
[^wiki-efi]: [Wikipedia: EFI system partition](https://en.wikipedia.org/wiki/EFI_system_partition)

[loader-conf-reference]: https://systemd.network/loader.conf.html
