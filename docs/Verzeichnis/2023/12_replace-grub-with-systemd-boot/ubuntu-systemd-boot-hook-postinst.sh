#!/bin/sh

set -e

test -x /usr/bin/bootctl || exit 0

bootctl is-installed || exit 0

echo "Installing kernel version $1 in systemd-boot..."

kernel-install add "$1" "$2"
