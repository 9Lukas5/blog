System:
     Firmware: n/a (n/a)
  Secure Boot: disabled (unsupported)
 TPM2 Support: no
 Boot into FW: supported

Current Boot Loader:
      Product: n/a
     Features: ✗ Boot counting
               ✗ Menu timeout control
               ✗ One-shot menu timeout control
               ✗ Default entry control
               ✗ One-shot entry control
               ✗ Support for XBOOTLDR partition
               ✗ Support for passing random seed to OS
               ✗ Load drop-in drivers
               ✗ Boot loader sets ESP information
          ESP: n/a
         File: └─n/a
