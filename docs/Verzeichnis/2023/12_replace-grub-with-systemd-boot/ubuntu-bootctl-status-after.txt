System:
     Firmware: UEFI 2.70 (EDK II 1.00)
  Secure Boot: disabled (unsupported)
 TPM2 Support: no
 Boot into FW: supported

Current Boot Loader:
      Product: systemd-boot 251.4-1ubuntu7.3
     Features: ✓ Boot counting
               ✓ Menu timeout control
               ✓ One-shot menu timeout control
               ✓ Default entry control
               ✓ One-shot entry control
               ✓ Support for XBOOTLDR partition
               ✓ Support for passing random seed to OS
               ✓ Load drop-in drivers
               ✓ Boot loader sets ESP information
          ESP: /dev/disk/by-partuuid/303474c6-72af-4597-89c5-4707c575b482
         File: └─/EFI/BOOT/BOOTX64.EFI
