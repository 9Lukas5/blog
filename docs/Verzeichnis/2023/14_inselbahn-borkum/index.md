---
tags:
    - Eisenbahn
    - Kleinbahn
    - Nebenbahn
    - FV-NE
    - Insel
    - Borkum

title: Inselbahn Borkum
description:
    "Die Borkumer (nicht) Kleinbahn: Reisebericht über die Inselbahn auf Borkum"

date: 2023-10-31 15:35
update: 2023-10-31 15:35
comments:
    host: mastodontech.de
    username: 9Lukas5
    id: 111330147548211867
image: zug-abgestellt.avif
list_entry: true
---

# {{ title }}

![Zug abgestellt](./zug-abgestellt.avif){ data-description=".zug-abgestellt" loading=lazy }
<div class="glightbox-desc zug-abgestellt">
    Garnitur der Borkumer-Kleinbahn die vormittags zwischenabgestellt war.
    Fotografiert vom Bahnübergang aus, hinter dem der Zug unmittelbar geparkt wurde in einem linksbogen.
</div>

<p class="introduction" markdown>
<span class="date">{{ date }}</span>  
{{ description }}
</p>

---

Borkum ist die westlichste und größte der ostfriesichen Inseln[^wiki-borkum].
Erreicht wird sie per Fähre mit der AG-EMS von Emden :de: oder Eemshaven :nl:[^ag-ems-fahrplan].
Um vom Anleger(Stadtteil Reede) in den Stadtteil Borkum zu kommen, fährt abgestimmt auf die Fähren die Inselbahn vom Anleger am östlichen Ende der Insel auf die Westseite.
Fähre und Bahn gehören zur selben Firmengruppe.

## Kleinbahn? - Nene, eine Nebanbahn

Die Bahn nennt sich zwar "Borkumer Kleinbahn", rechtlich gesehen gibt es diese Form der Eisenbahnen in Deutschland aber nicht mehr.
Das Preußische-Kleinbahngesetz von 1892[^wiki-preusische-kleinbahngesetz] wurde bis 2005 fast überall durch neuere Landeseisenbahngesetze ersetzt.
Als Folge sind die früheren _Kleinbahnen_ heute als _Nebenbahnen_ klassifiziert und unterliegen der _Eisenbahn-Bau- und Betriebsordnung_[^wiki-kleinbahn].

## Zugleitbetrieb(ZLB)

![Signal Ne1 - Trapeztafel](./Ne1.avif){ align=left style="max-width: 33%;" loading=lazy }

Ich muss hier ja ganz platt zugeben: Ich bin zwar Betriebseinsenbahner, aber bisher selbst einfach nicht damit in berührung gekommen.
Selbst kenne ich eben _die große Eisenbahn_, wo Stellwerke und Fahrdienstleiter sind, und mit Hauptsignal ausgerüstete Infrastruktur :information_desk_person:<br>
Und dann gibts halt noch ne Menge komischer Spezial-Dinge irgendwo, wo ich nicht lang komme, wo der Teufel weiß, wie die funktionieren, ne? :face_with_peeking_eye:

Aber stellt sich raus: Klar, es gibt natürlich örtliche Unterschiede, aber im Kern greifen viele auf ein standardisiertes Betriebsverfahren zu: den __Zugleitbetrieb(ZLB)__.
Ich hab das jetzt mal nur quergelesen und verlinke hier auf den [Eintrag bei TF-Ausbildung.de zum ZLB][tf-ausbildung-zlb] als öffentlich verfügbare Quelle, wenn es jemand selbst querlesen will.

![Signal Ne5 - H-Tafel](./Ne5.avif){ align=right style="max-width: 33%;" loading=lazy }

Grob zusammengefasst: Es gibt eine zentrale Person, den __Zugleiter__, der quasi alle Fäden in der Hand hält.
Bei diesem müssen alle die sich bewegen wollen eine __Anfrage__ stellen von A nach B zu fahren, oder in A zu rangieren und auf die entsprechende __Erlaubnis__ von diesem warten.
Wenn die Erlaubnis erteilt wurde, kann das angefragte durchgeführt werden.
Im Nachgang muss rückgemeldet werden, dass man bspw. vollständig am Ziel ist.<br>
Dieses Betriebsverfahren basiert offensichtlich sehr stark auf eindeutiger Kommunikation, weshalb hierbei natürlich rigorose Dokumenation von allem Gesprochenem sehr wichtig ist.

![Signal Ra10 - Rangierhalttafel](./Ra10.avif){ align=left style="max-height: 12em;" loading=lazy }
![Spezielles Signal für Sperrfahrten bei Abstellung](./sperrfahrten-halt.avif){ align=left style="max-height: 12em;" loading=lazy }

Die mit wichtigsten Signale im ZLB sind die __Trapeztafel(Ne 1)__ und die __H-Tafel(Ne 5)__.
Wie oben erwähnt, wird für jede Bewegung eine Erlaubnis angefordert, mit konkreten Zielen.
Wenn in eine Betriebsstelle eingefahren werden soll, dann stellt die H-Tafel das Zielsignal dar.
Ein Überfahren ohne Erlaubnis wäre also wie ein Überfahren eines Halt-zeigenden Signals zu werten.

![Rückfallweiche mit Überwachung](./Signal-Rueckfallweiche.avif){ align=right style="max-width: 33%;" data-description=".rueckfallweiche" data-caption-position=right loading=lazy }
<div class="glightbox-desc rueckfallweiche">
    Die Weiche im Bild steht in Grundstellung abzweigend, um nach Ankunft für die Rückfahrt in das für die neue Fahrtrichtung rechte Gleis zu kommen.<br>
    <br>
    Das gelb/schwarze Hebelgewicht markiert die Weiche als Rückfallweiche; daneben steht das Signal Ne 13 und zeigt das Signalbild Ne 13a, dass die Weiche spitz befahren werden kann.
</div>

Um auch ohne Fahrdienstleiter einen zweigleisigen Betrieb bewerkstelligen zu können, kommen Rückfallweichen zum Einsatz.
Zu erkennen sind diese an den gelb/schwarzen Hebelgewichten.
Beim spitzen Befahren der Weichen steht ein Überwachungssignal vor der Weiche, das einem signalisiert, ob das spitze Befahren möglich ist.


Abgesehen von Trapez- und H-Tafeln habe ich noch Rangierhalttafeln (Ra 10) und ein spezielles Signal gesichtet.
Alle Signale haben diese blaue Leuchte unten dran, aus der ich allerdings nicht wirklich schlau geworden bin, wann die nun mal leuchtet oder nicht.
Hörensagen zufolge soll die lediglich unterstützend sein, und signalisieren ob der Magent der Zugsicherung aktiv ist oder nicht? :shrug:<br>
Ach ja: ist euch aufgefallen, dass da offenbar keine 0815 PZB-Magenete verbaut sind?
Besonders bei der H-Tafel weiter oben auf den ersten Blick verwirrend, aber das leuchtend gelbe ist kein (falsch montierter) PZB-Magnet, sondern das fast schon schwarz gewordene Ding bisschen rechts drunter :zany_face:

## Betriebsgelände

![Borkum Bf - Bahnsteig](./Borkum-Bf-Zug.avif){ align=left style="max-width: 50%;" data-description=".borkum-bf-bstg" loading=lazy }

<div class="glightbox-desc borkum-bf-bstg">
    Auf dem Bild ist der Bahnsteig des Bahnhof Borkum mit Blick vom Gleisende zu sehen, der in einem leichten Bogen liegt.
    Es steht aktuell eine Garnitur der Kleinbahn am Bahnsteig.
</div>

Am Bahnhof in Borkum liegt der zweigleisige Bahnsteig direkt am Gleisende und schmiegt sich dort perfekt in die Umgebung ein.
Das ganze wirkt wie eine Straßenbahnhaltestelle, wie sich die Bahnanlage dort in die Umgebung einpasst.

Vom Bahnsteig aus verläuft die Strecke in einem langen Linksbogen aus dem Ort hinaus.
Vom Bahnsteigbereich geradeaus, statt dem Streckengleis folgend, ist die Werkstatt und ein Teil der Abstellflächen zu finden.
Um die zweite Halle mit Abstellflächen zu erreichen, muss dagegen zuerst auf dem Streckengleis dem Linksbogen gefolgt und nach Passieren der Weiche die Fahrtrichtung gewechselt werden.

![Kartenausschnitt Borkum Bf](./karte.avif){ align=right style="max-width:50%;" data-description=".openrailwaymap-borkum-bf" data-caption-position="right"loading=lazy }
<div class="glightbox-desc openrailwaymap-borkum-bf">
    Auf diesem Kartenausschnitt sind der Bahnhof Borkum, das Werkstattgelände, sowie die Abstellhalle zu sehen.<br>
    <br>
    Von oben chronologisch folgend:
    <ul>
        <li> Sh2-Tafel am Gleisende
        <li> Bahnsteiggleise bis zu den H-Tafeln
        <li> Erster blau markierte Bereich: Werkstatthalle und Abstellflächen befahrbar aus Richtung Bahnsteigen
        <li> Grüner Strich: Bereich in dem der Zug aus dem Titelbild vormittags zwischenabgestellt war
        <li> Zweiter blau markierte Bereich: Abstellhalle befahrbar aus Richtung Anleger
    </ul>
    Die roten und blauen Pfeile kennzeichnen die Fahrtrichtung für die die auf der Karte eingezeichneten Signale aufgestellt sind.<br>
    <br>
    Die Trapeztafel die rechts unten mit dem roten Pfeil für die Fahrtrichtung zum Anleger auf der Karte eingezeichnet ist, ist übrigens keine normale Trapeztafel, sondern dieses rechteckige Text-Schild von weiter oben.
</div>

In der Karte sind die bekannten Signale zu sehen, welche ich mit Pfeilen versehen habe, um klarzumachen, für welche Fahrtichtung diese jeweils aufgestellt sind.
Aufgrund der Auftsellung nehme ich an, dass der Bahnhofsbereich hier asynchron beginnt/endet:
Auf dem Regelgleis vom Anleger Richtung Bahnhof kommt die Trapeztafel deutlich später als auf dem Gegengleis.
Dadurch ergibt sich die betriebliche Situation, dass vom Bahnsteig bis hinter die Weiche in die zweite Remise als Rangierfahrt verkehrt werden kann und erklärt auch gleichzeitig ganz einfach, wie der Zug an der grün markierten Stelle abgestellt werden konnte.

Aktuell werden die Bahnsteige nach und nach umgebaut.
Bislang sind diese auf Schienenoberkante(SOK) oder ~15 cm über SOK.
Bei den jetzigen Umbaumaßnahmen wird auf etwas über 70 cm erhöht, um einen höhengleichen Einstieg in die Wagen zu ermöglichen.
[^bkb-bstg-baublog]

Wenn die Bahnsteige alle umgebaut sind, ob die Trittstufen an den Wagen dann abmontiert und auf Plattformhöhe wieder angebracht werden? :thinking:
Sonst bleibt ja trotz Höhengleichheit noch ein Spalt übrig :information_desk_person:

## Fahrzeuge

![Borkum Bf - Bahnsteig](./Borkum-Bf-Zug-2.avif){ align=right style="max-width: 50%;" data-description=".borkum-bf-bstg-2" loading=lazy }

<div class="glightbox-desc borkum-bf-bstg-2">
    Sicht von Werkstattseite auf den Bahnsteig/Zug.
</div>

Es gibt noch einige alte Wagen im Bestand, die für spezielle Fahrten auch noch genutzt werden.
Primär im Einsatz sind aber die mitte der 90er Jahre neu gebauten Wagen und drei Lokomotiven.
2007 wurde noch eine vierte Lokomotive in Dienst gestellt, womit an jedem Zug immer an jedem Ende eine Lok hängt und sich das Umspannen einsparen lässt.
[^bkb-historie]

Im Gegensatz zum alten Fahrzeugpark wurde der neue mit einer SchaKu und durchgehenden Druckluftbremse ausgestattet, während beim alten Fahrzeugpark lediglich die Lokomotiven gebremst waren.
Die Wagen die eine Bremse haben sind gelb lackiert, abgesehen davon haben die Wagenfarben keine technischen Hintergründe.
[^bkb-historie]

![Abstellung mit Zufahrt aus Anleger Richtung](./Remise-1.avif){ style="max-height: 6em;" data-description=".remise" loading=lazy }
![Abstellung mit Zufahrt aus Anleger Richtung](./Remise-2.avif){ style="max-height: 6em;" data-description=".remise" loading=lazy }
![Drehgestell mit Bremse](./Wagen-mit-Bremse.avif){ style="max-height: 6em;" data-description=".drehgestell-m-bremse" loading=lazy }
![Drehgestell ohne Bremse](./Wagen-ohne-Bremse.avif){ style="max-height: 6em;" data-description=".drehgestell-o-bremse" loading=lazy }
<div class="glightbox-desc remise">
    Blick vom hinteren Ende der Abstellhalle die aus Anleger-Richtung zugänglich ist.
    Zu sehen sind mehrere der im Fuhrpark befindlichen Lokomotiven und im Hintergrund ein paar Wagen.
</div>
<div class="glightbox-desc drehgestell-m-bremse">
    Wagen mit gelber Außenlackierung, hier ist am Radsatz ein Bremsklotz zu sehen.
</div>
<div class="glightbox-desc drehgestell-o-bremse">
    Wagen mit anderer Außenlackierung, hier ist am Radsatz kein Bremsklotz zu sehen.
</div>

## Zukunft

Die Borkumer Kleinbahn hat 2020 in Eigenregie einen neuen Wagen gefertigt.
[^bkb-historie]
Aus diesem Muster ist mittlerweile ein ganzer Zug geworden und befindet sich wohl in der Erprobung.
Interessant an diesem Neubau fand ich: Es musste wohl eine Brandmeldeanlage eingebaut werden, auf eine Türüberwachung konnte aber aufgrund der niedrigen Höchstgeschwindigkeit weiterhin verzichtet werden.
[^bkb-buschfunk]

Die aktuelle Konzession läuft noch bis 2028.
Wie der Betrieb danach rechtlich weiter vergeben wird bleibt abzuwarten.
Aber selbst wenn es theoretisch eine europäische Ausschreibung geben sollte, gäbe es in der Praxis wohl kaum andere Betreiber für die Strecke.
[^wiki-bkb]
:shrug:

## Eigene Gedanken

Was ich mich persönlich spontan gefragt habe ist, wie die technische Zukunft der BKB so aussehen könnte.
Ich hatte kurz phantatsiert, ob sich eine Geschwindigkeitserhöhung und Akku-Triebwagen für einen festen 15/30-Minuten Takt lohnen könnten? Hab nur keine Ahnung wie so der Bedarf ist, ob das Fahrgastpotential dafür vorhanden wäre. :information_desk_person:  
Der erst vor einigen Jahren gebaute Wagen im Stil der bisherigen zeigt aber eigentlich ganz klar auf: Es geht vor allem auch um den Erhalt des Charakters der Bahn, womit moderne Triebwagen eigentlich ausscheiden.

Eine technische Evolution für die Lokomotiven könnte in den nächsten Jahren aber unter Umständen eine Umrüstung auf E-Antrieb mit Akku sein :thinking:
Der Sprit muss schließlich aufwändig auf die Insel gebracht werden, was man auch an der Auto-Tankstelle sieht: Preise jenseits der 2€ je Liter :smirk:.
Und bei lediglich 7-8 Kilometer Strecke[^wiki-bkb] dürfte die benötigte Reichweite mit einer Akku-Umrüstung bestimmt drin sein :nerd:

Lasst mal in den Kommentaren hören, was ihr so machen würdet :eyes:

## Weitere Aufnahmen

Zum Schluss noch ein paar andere Bilder, die ich im Text nicht mehr mit untergebracht habe, aber nicht einfach ungezeigt lassen wollte :camera:, sowie ein Video bei der Ausfahrt aus dem Borkumer Bahnhof :video_camera:.

![Historischer Schienenbus in Halle](./Schienenbus.avif){ style="height: 6em;" loading=lazy }
![Schnappschuss Führerstand Diesellok](./Fst.avif){ style="height: 6em;" loading=lazy }
![Steuerventil Diesellok](./Steuerventil.avif){ style="height: 6em;" loading=lazy }
![Anschrift Hauptuntersuchung](./HU-Anschrift.avif){ style="height: 6em;" loading=lazy }
![Überwachungssignal](./Ueberwachungssignal.avif){ style="height: 6em;" loading=lazy }
![Privater Hauszugang über Gleise](./privater-Zugang.avif){ style="height: 6em;" data-description=".privat-zugang" loading=lazy }

<div class="glightbox-desc privat-zugang">
    Es gibt sogar manche Häuser die direkt neben der Strecke liegen und den Zugang zur Haustür mit einem privaten Fußweg über die Gleise haben.
</div>

<video controls >
    <source src="./Ausfahrt-Borkum-Bf.mp4" type="video/mp4">
</video>

## Quellen

[^ag-ems-fahrplan]: [AG-EMS Fahrplan][ag-ems-fahrplan]
[^bkb-bstg-baublog]: [Borkumer Kleinbahn - Bau-Blog Bahnsteige][bkb-bstg-baublog]
[^bkb-buschfunk]: Gespräche vor Ort
[^bkb-historie]: [Borkumer Kleinbahn - Unternehmensgeschichte][bkb-historie]
[^wiki-bkb]: [Wikipedia: Borkumer Kleinbahn][wiki-bkb]
[^wiki-borkum]: [Wikipedia: Borkum][wiki-borkum]
[^wiki-kleinbahn]: [Wikipedia: Kleinbahn][wiki-kleinbahn]
[^wiki-preusische-kleinbahngesetz]: [Wikipedia: Preußisches Kleinbahngesetz][wiki-preusische-kleinbahngesetz]

[ag-ems-fahrplan]: https://www.ag-ems.de/fahrplan
[bkb-bstg-baublog]: https://www.borkumer-kleinbahn.de/aktuelles/bau-blog-bahnsteige
[bkb-historie]: https://www.borkumer-kleinbahn.de/footer/historie
[tf-ausbildung-zlb]: https://tf-ausbildung.de/BahnInfo/zlb.htm
[wiki-bkb]: https://de.wikipedia.org/wiki/Borkumer_Kleinbahn
[wiki-borkum]: https://de.wikipedia.org/wiki/Borkum
[wiki-kleinbahn]: https://de.wikipedia.org/wiki/Kleinbahn
[wiki-preusische-kleinbahngesetz]: https://de.wikipedia.org/wiki/Preu%C3%9Fisches_Kleinbahngesetz
