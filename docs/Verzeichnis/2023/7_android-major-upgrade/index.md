---
tags:
    - Android
    - LineageOS
    - Compile
    - Buildumgebung
    - Linux

title: Android 13 Major-Update
description:
    "Odysee alle Dinge für die lokale Build-Umgebung zu finden, um auf eine neue Android-Hauptversion zu wechseln."
image: header.avif
date: 2023-01-21 00:00
update: 2023-01-21 00:00
list_entry: true
---

# {{ title }}

<p class="introduction" markdown>
<span class="date">{{ date }}</span>  
{{ description }}
</p>

---

Ich habe ein [Xiaomi Mi 8 Lite :link:][devicespecifications_mi8lite]{target="_blank"} und [Samsung Galaxy Tab S6 Lite (Wifi) :link:][devicespecifications_tabs6lite]{target="_blank"}, die beide auf LineageOS 18.1 (Android 11) gelaufen sind.
Das Tablet habe ich einmal geflasht und seitdem (~ Mitte 2020) tatsächlich nicht mehr angefasst :shushing_face:, für das Handy habe ich jeden Monat selbst ein Update gebaut, wenn die monatlichen Sicherheitspatches gemergt waren.

Nachdem ich immer wieder im [XDA-Forum :link:][xdaforum]{target="_blank"} geschaut habe, gab es im Subforum des Handys jetzt endlich eine vielversprechende Android-13 Version.
Also habe ich für LineageOS 20 die Build-Umgebung initialisiert und begonnen mithilfe der Links zu den verwendeten Gerätespezifischen Quellcode-Repositories alles für mein Handy zusammenzutragen, dass ich für ein vollständiges Build brauche.


## Vorgeschichte: Warum baue ich überhaupt selbst?

### Grundsatzentscheidung

Ich habe vor einiger Zeit beschlossen auf meinen Geräten keine Google-Apps mehr zu nutzen.
Stattdessen nutze ich [F-Droid :link:][fdroid-website]{target="_blank"} als App-Store, was recht ähnlich wie Linux-Paketverwaltungen funktioniert.
Als Erstes habe ich versucht für alles was ich brauche, Apps zu nutzen die direkt im F-Droid eigens gehosteten Repo verfügbar sind.
Zweite Option sind Apps bei denen die Entwickler selbst ein F-Droid Repo hosten, dass ich als Quelle konfigurieren kann.
Alles andere was mir dann noch fehlt hoste ich in einem privaten F-Droid Repo, für das ich die Apk-Dateien zuerst versuche aus [APKMirror :link:][apkmirror]{target="_blank"} zu beziehen, oder sonst [über eine Java-Applikation :link:][raccoon]{target="_blank"} direkt aus dem PlayStore.

### Ersatz für Google-Dienste

Ein großes Problem sind die nicht funktionierenden Push-Mitteilungen ohne Google-Apps.
An dieser Stelle kommt [MicroG :link:][microg-website]{target="_blank"} zum Zug, was übrigens sogar vom BMBF[^bmbf-microg-sponsor] gefördert wird :ok_hand_tone3:  
MicroG ist ein Drop-In Ersatz für die Google-Dienste und stellt teilweise deren Schnittstellen im System zur Verfügung.
Manche davon werden dann datenschutzfreundlicher repliziert, wie beispielsweise die Push-Benachrichtigung über die Google-Server, andere werden zum Teil auch einfach nur als Dummy zur Verfügung gestellt, damit Apps, die auf das Vorhandensein von Schnittstellen angewiesen sind, funktionieren, aber schlicht keine Verarbeitung von irgendetwas stattfindet.

### Xposed-Framework

Damit MicroG dieses Drop-In allerdings vollziehen kann, muss es sich im System für die Google-Dienste ausgeben (genannt Signature-Spoofing), was Android normalerweise natürlich nicht erlaubt.
Um diese Funktionalität zu bekommen, habe ich in der Vergangenheit Tools wie das [Xposed-Framework :link:][xposed-tools]{target="_blank"} genutzt.
Allein für das Erreichen der Signature-Spoofing Funktion war das allerdings ein eigentlich viel zu überladenes Tool und dessen Entwicklung gestaltete sich mit fortschreitenden Android-Versionen immer schwieriger.
Es musste also eine andere Lösung her.

### Offiziell MicroG-gepatchte Roms

Es gibt auch von [MicroG für viele Geräte direkt im Quellcode gepatchte Builds :link:][microg-lineage]{target="_blank"}, die das Signature-Spoofing als zusätzliche Berechtigung in das Android-Rechtesystem bringen.
Das Problem damit für mich: Es wird sehr unregelmäßig gebaut und es dauert mir zu lange, bis ich tätsächlich auf der Höhe der Entwicklung an sich bin.

Bis/Ob ein Gerät dort offizielle Unterstützung erlangt ist keine sichere Sache.
Dazu kommt, dass der Support jeder Zeit enden kann.
Beispiel aus heutiger Sicht: Fürs Tablet gibt es aktuell drei Builds online: vom 19. und 05. Dezember 2022 und 03. April 2022.
Was ist zwischen April und Dezember gewesen? :eyes:  
Beim Handy gibts welche vom 15. und 04. Oktober 2022, neuere Builds sind nicht verfügbar.
Vielleicht weil danach keine Lineage-18.1 Builds mehr gebaut wurden?
Keine Ahnung.

Diese Unsicherheit, ob und wie lange es Support für eine spezifische Android-Version gibt, war der Grund einfach selbst zu kompilieren, um jeden Monat die Upstream-Änderungen und monatlichen Sicherheitsupdates zu bekommen.

## Build-Umgebung einrichten

Für alle die selbst LineageOS bauen wollen, findet sich beim Suchen relativ schnell eine Anleitung zur Einrichtung online.
Einfach im [LineageOS-Wiki :link:][lineage-wiki]{target="_blank"} nach dem eigenen Gerät suchen und dort auf den Punk "Build for yourself" gehen.
[Beispiel für mein Gerät :link:][lineage-build-platina]{target="_blank"} :wink:

Da ich Android-11 bereits selbst gebaut habe, viel das meiste der Einrichtung ins Wasser und es blieb eigentlich nur der Quelldownload für Android-13.
Also, Platz dafür auf der Platte suchen und den Repo-Init Befehl laufen lassen:

```sh
repo init -u https://github.com/LineageOS/android.git -b lineage-20.0
repo sync --force-sync
```

### Signaturespoof-Patch für Android 13

Jetzt zu dem Punkt, warum es überhaupt notwendig ist, statt einem offiziellen LineageOS oder inoffiziellen Build von jemand aus dem Forum, selbst zu kompilieren: Die Android-Rechteerweiterung um Signature-Spoofing.

Für die verschiedensten Android-Versionen gibt es die Source-Code-Änderungen die in den offiziellen MicroG-LineageOS-Builds verwendet werden, auch als fertig anwendbare Patches in einem [LineageOS4MicroG Repo :link:][microg-lineage-patches]{target="_blank"}. :partying_face:  
Für LineageOS-20 braucht es von dort also die beiden Dateien [android_frameworks_base-Android13.patch][frameworks_base-patch] und [packages_modules_Permission-Android13.patch][modules_perms-patch].
Das fügt zum einen die Funktion des Signature-Spoofing an sich in den Android-Kern ein und erweitert das UI der System-Berechtigungsverwaltung um diese.

Damit ist das Signature-Spoofing auch nicht einfach global für jede installierte App möglich, sondern unterliegt nochmals der Nutzerregulierung auf App-spezifischer Ebene, welche App es letztlich tatsächlich darf, wenn sie es versucht.

<figure markdown>
<div style="display: flex; justify-content: space-around; align-items: center">
<img alt="Android Berechtigungsmanger mit allen vorhandenen Rechten, einschließlich der neuen Berechtigung 'Spoof package signature' die zwei Apps erteilt wurde" src="Berechtigung-uebersicht-mit-spoofing-Eintrag.avif" loading="lazy">
<img alt="Spoof package signature Berechtigung für 'microG Services Core die auf 'Zulassen' steht" src="Berechtigung-microg-zugelassen.avif" loading="lazy">
<img alt="MicroG Selbsttest-Seite die anzeigt, dass Signature-Spoofing unterstützt wird und die Berechtigung erteilt wurde" src="MicroG-selbsttest.avif" loading="lazy">
</div>
<figcaption>Android Rechtemanagement und MicroG Selbsttest mit den Signature-Spoofing Erweiterungen im UI</figcaption>
</figure>

### Neuer Ort für Emoji-Schriftart

Wenn ich schon selbst baue, dann kann ich ja auch gleich noch eine kleine Anpassung vornehmen, die ich sonst mit jedem Update wieder händisch machen müsste :information_desk_person_tone3:  
Ich mag irgendwie die Android-Emoji's leider nicht :shrug_tone3:.
Sonst hab ich ja nichts von Apple, aber bei den Emoji's find ich die aus iOS einfach am besten :grimacing:

Ich hab mir also die Emoji-TTF aus iOS gesucht (kommt wohl aus ner iOS 16 Beta :thinking: :shrug_tone3:) und füge diese bei meinen Builds immer statt der eigentlich in Android enthaltenen ein.  
In Lineage-18.1 war diese unter `external/noto-fonts/emoji/NotoColorEmoji.ttf` platziert.
Das hat sich aber mittlerweile wohl geändert :hushed:
Kurze Suche hat als wahrscheinlichste neue Platzierung `external/noto-fonts/emoji-compat/font/NotoColorEmojiCompat.ttf` ergeben und nach dem Build später auch bestätigt.

### Alle Geräte-Repositories finden

Nachdem alles vom Grundsystem wieder zusammengetragen war, kommt das was immer wieder irgendwie ein Problem ist: Die Geräte-spezifischen Teile die es braucht :see_no_evil:

Ausgangspunkt war der [Forums-Thread :link:][xda-platina-los20]{target="_blank"} auf XDA vom Entwickler der sich LOS-20 für mein Gerät angenommen hat.
In diesem Thread ist aber, wie schon so oft festgestellt, nur das verwendete [Kernel-Repo :link:][gh-sabarop-sdm660-kernel]{target="_blank"} und der [Device-Tree :link:][gh-sabarop-platina-device-tree]{target="_blank"} verlinkt.  
Problem: das reicht meistens noch nicht, um tatsächlich ein Build zu erzeugen.
Es braucht normal mindestens noch ein Vendor-Repo dazu und uU noch ein paar weitere die für das spezifische Gerät benötigte Dateien enthält.
Diese werden aber kaum mal direkt im Thread mit verlinkt und einen Automatismus der diese aus dem Geräte-Repo hinzufügt gibt es oft auch nicht.

Nach Frage beim Entwickler habe ich hier die noch fehlenden Links bekommen.
Die zu verwendenden Branches habe ich dann erst mal etwas geraten, wobei ich auch erstmal daneben gegriffen hab, da der mit den neusten Änderungen noch nicht stabil ist.
Dadurch hatte ich dann zum Beispiel auch mal vier Tage, in denen eingehende Anrufe komplett nicht funktioniert haben.
(Das muss einem erstmal auffallen, wenn ausgehend noch funktioniert, nur eingehend nicht :grimacing:)

Letztlich sind die folgenden Repositories und Branches aktuell für das Xiaomi Mi 8 Lite notwendig, um LineageOS-20 zu bauen:

| Repo                                                                                          | Branch         |
| --------------------------------------------------------------------------------------------- | -------------- |
| [kernel_xiaomi_sdm660_clover :link:][gh-sabarop-platina-kernel-build-branch]{target="_blank"} | lineage-20-4.4 |
| [device_xiaomi_platina :link:][gh-sabarop-platina-device-tree-build-branch]{target="_blank"}  | lineage-20     |
| [vendor_xiaomi_platina :link:][gh-platina-vendor-build-branch]{target="_blank"}               | 13             |
| [vendor_xiaomi-firmware :link:][platina-firmware-build-branch]{target="_blank"}               | thirteen       |

Im Geräte-Baum gibt es noch einen Branch der als Work-in-Progress benannt ist, mit dem zusätzlich noch das [android_hardware_xiaomi][gh-platina-hardware-build-branch]{target="_blank"} Repo benötigt wird.
Dieser enthält aber unter anderem das Problem mit den eingehenden Anrufen^^  

Der Entwickler arbeitet aktuell daran das Gerät vom momentan genutzten Linux 4.4 Kernel auf 4.19 zu heben.
Mal sehen wie das so klappt.
Daumen drücken auf jeden Fall :pray_tone3:

## Ergebnis

Nach der Grundinitialisierung für die Quellen von Lineage-20 und dem zusammensuchen der Geräte-Repos habe ich folgendes in der `roomservice.xml` unter `.repo/local_manifests/`:

??? "roomservice.xml"

    ```xml
    <?xml version="1.0" encoding="UTF-8"?>
    <manifest>
        <remote name="los" fetch="https://github.com/LineageOS/" revision="lineage-20" />
        <remote name="linux4" fetch="https://github.com/Linux4/" revision="lineage-20.0" />
        <remote name="sabarop" fetch="https://github.com/sabarop/" />
        <remote name="pixelexperience-vendor" fetch="https://gitlab.pixelexperience.org/android/vendor-blobs/" revision="thirteen" />

        <!-- platina -->
        <project name="device_xiaomi_platina" path="device/xiaomi/platina" remote="sabarop" revision="lineage-20" />
        <project name="vendor_xiaomi_platina" path="vendor/xiaomi/platina" remote="sabarop" revision="13" />
        <project name="kernel_xiaomi_sdm660_clover" path="kernel/xiaomi/platina" remote="sabarop" revision="lineage-20-4.4" />
        <project name="vendor_xiaomi-firmware" path="vendor/xiaomi-firmware" remote="pixelexperience-vendor" />
    </manifest>
    ```

Im Root-Ordner der Build-Umgebung liegen dann noch die beiden Patches für das Signature-Spoofing, sowie die iOS-Emoji-Font.
Zum Bauen eines neuen Builds, dass alle Quellen neu synchronisiert, die Patche neu anwendet, die Emoji's austauscht, etc habe ich dann noch folgendes Skript:

=== "Ablauf"

    - erzwingt ein Sync aller Quellen
    - wendet die beiden Signature-Spoof Patche an und committed sie
    - kopiert die Emoji-Font
    - räumt alte Build-Artefakte auf
    - kompiliert für das angegebene Gerät

=== "Quellcode"

    ```sh
    #!/bin/bash
    #

    set -e

    . ./env/bin/activate

    if [ -z "$1" ]
    then
        echo "device argument missing"
        exit 1
    fi

    BASEDIR=$PWD
    PATH="$PWD/.repo/repo:$PATH"

    if [[ -z "$SKIPSYNC" || $SKIPSYNC == 0 ]]
    then
        repo sync --force-sync

        # apply microG patches
        cd $BASEDIR/frameworks/base
        patch -p1 -i $BASEDIR/android_frameworks_base-Android13.patch
        git add -u
        git commit -m "apply signature spoof permission patch" --no-gpg-sign
        git clean -df .

        cd $BASEDIR/packages/modules/Permission
        patch -p1 -i $BASEDIR/packages_modules_Permission-Android13.patch
        git add -u
        git commit -m "apply signature spoof permission patch" --no-gpg-sign
        git clean -df .

        cp $BASEDIR/unofficial_iOS16.ttf $BASEDIR/external/noto-fonts/emoji-compat/font/NotoColorEmojiCompat.ttf
    fi

    # pre-build phase setup
    cd $BASEDIR
    . ./envconfig

    # clear old build artifacts
    make clean

    # build 20.0 for target
    cd $BASEDIR
    breakfast $1
    brunch $1

    deactivate
    ```

Jetzt habe ich eine laufende Build-Umgebung für LineageOS-20 mit MicroG Patch, bin mit Handy und Tablet umgestiegen und baue mir jeden Monat ein neues Build mit den Sicherheitsupdates.
Wann diese gemergt sind, kontrolliere ich [hier :link:][lineage-gerrit-patch-state]{target="_blank"}.  
Für mein Tablet (gta4xlwifi) war es zur Abwechslung übrigens enorm einfach die roomservice.xml zusammen zu bekommen: Den Geräte-Baum eingefügt, einaml `breakfast` laufen lassen und alle anderen benötigten Einträge wurden automatisch ergänzt :flushed: :ok_hand_tone3:

<figure markdown>
  ![Screenshot Android Einstellungen Xiaomi Mi 8 Lite mit Android 13 LineageOS 20](./platina-settings-screenshot.avif){ loading=lazy }
  <figcaption>Android-Version Bildschirm meines momentanen genutzten Builds</figcaption>
</figure>

[^bmbf-microg-sponsor]: [Bundesministerium für Bildung und Forschung :link:](https://prototypefund.de/project/microg/){target="_blank"}
[apkmirror]: https://www.apkmirror.com/
[devicespecifications_mi8lite]: https://www.devicespecifications.com/de/model/a6444c8a
[devicespecifications_tabs6lite]: https://www.devicespecifications.com/de/model/b8cd534b
[fdroid-website]: https://f-droid.org/
[frameworks_base-patch]: https://github.com/lineageos4microg/docker-lineage-cicd/raw/master/src/signature_spoofing_patches/android_frameworks_base-Android13.patch
[gh-platina-hardware-build-branch]: https://github.com/LineageOS/android_hardware_xiaomi/tree/lineage-20
[gh-platina-vendor-build-branch]: https://github.com/sabarop/vendor_xiaomi_platina/tree/13
[gh-sabarop-platina-device-tree-build-branch]: https://github.com/sabarop/device_xiaomi_platina/tree/lineage-20
[gh-sabarop-platina-device-tree]: https://github.com/sabarop/device_xiaomi_platina
[gh-sabarop-platina-kernel-build-branch]: https://github.com/sabarop/kernel_xiaomi_sdm660_clover/tree/lineage-20-4.4
[gh-sabarop-sdm660-kernel]: https://github.com/sabarop/kernel_xiaomi_sdm660_clover
[lineage-build-platina]: https://wiki.lineageos.org/devices/platina/build
[lineage-gerrit-patch-state]: https://review.lineageos.org/q/message:+%2522Bump+Security+String%2522+OR+message:+%2522Update+Security+String+to%2522
[lineage-wiki]: https://wiki.lineageos.org/
[microg-lineage-patches]: https://github.com/lineageos4microg/docker-lineage-cicd/tree/master/src/signature_spoofing_patches
[microg-lineage]: https://lineage.microg.org/
[microg-website]: https://microg.org/
[modules_perms-patch]: https://github.com/lineageos4microg/docker-lineage-cicd/raw/master/src/signature_spoofing_patches/packages_modules_Permission-Android13.patch
[platina-firmware-build-branch]: https://gitlab.pixelexperience.org/android/vendor-blobs/vendor_xiaomi-firmware/-/tree/thirteen
[raccoon]: https://github.com/onyxbits/raccoon4
[xda-platina-los20]: https://forum.xda-developers.com/t/rom-unofficial-lineageos-20-0-android-13-for-mi-8-lite-platina.4530579/
[xdaforum]: https://forum.xda-developers.com/
[xposed-tools]: https://github.com/rovo89/XposedTools/
