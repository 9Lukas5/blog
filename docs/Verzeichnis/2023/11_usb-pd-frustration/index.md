---
tags:
    - Linux
    - USBC
    - USB-C
    - USBPD
    - PowerDelivery
    - Charging
    - Lenovo
    - IdeaPad

title: The USB-PD lockdown frustration
description:
    Imagine a device with a single USB-C Port, capable of Data and PowerDelivery, locking down charging until unlocked in the BIOS again on plug/unplug.
date: 2023-07-16 00:00
update: 2023-07-16 00:00
comments:
    host: mastodontech.de
    username: 9Lukas5
    id: 110725424927439806
image: laptop-io.avif
list_entry: true
---

# {{ title }}

![](./laptop-io.avif){ loading=lazy }

<p class="introduction" markdown>
<span class="date">{{ date }}</span>  
{{ description }}
</p>

---

## The machine

I have a Lenovo IdeaPad Flex5 14ARE05 at the moment.
The target specs choosing it were basically:

- Team red Ryzen Mobile
- Touchscreen & 360° hinge (which admittedly are not at all used :face_with_peeking_eye:)
- USB power delivery capable USB-C Port

So, I didn't want some crazy shit high performance machine, but rather a light one not cracking a 100 Watts.
With today's chips this is absolutely sufficient for my needs, isn't too heavy on traveling around with it and I can carry one power brick with me, able to charge all my devices from it.

## Desk setup

When being at home I have a dedicated mouse and keyboard, the big screen, cabled network (which the laptop doesn't have a port for) and the stereo.
So, what's the obvious thing to buy for the desk? :eyes:

__A Hub__ :partying_face:

The idea here was, I wanted to have as few as possible connectors to plug/unplug if I come to/from the desk.
Therefore I searched for a Hub featuring at least:

- USB-PD pass-through
- 1Gbit Ethernet
- 3,5mm Audio out
- a bunch of USB-A/C Ports

Then I had only two cables to connect, both directly next to each other: The USB-C Hub + Monitor.

## First Hub

The first one I got was just a cheap non-name one for 40€.
It had USB-PD, 2x USB-A 2.0, 2x USB-A 3.0, 3,5mm audio out, a card reader, Gbit Ethernet, HDMI and VGA out.
The display outputs and card reader would most likely be never utilized, but the rest fit my list.
And what could you do wrong on a USB-Hub, right? :shrug:

The one I ended up with was this one:

<figure markdown>
{% set caption = "first no-name hub with VGA" %}
![{{caption}}](./hub-1.avif){ loading=lazy }
<figcaption>{{caption}}</figcaption>
</figure>

In the beginning this actually worked out fine.
(It was a while ago by now, so memories are a bit vague, but I can't remember any major issues at the start :thinking:)  
If I'm not mistaken, the first problems came up after a power outage.
The Laptop, Hub and power brick were connected before, throughout and on return of the power.

From there on the Hub worked mostly fine, as long as everything was already plugged in before booting the laptop.
But it only got worse.
Soon, I'd be on the encryption unlock and my USB stuff wouldn't be recognized anymore, the network controller missing from the device list, etc.
And on top: once unplugged, there was no chance to get it to charge again.
That's when I decided to replace the Hub.

## Second Hub

I thought maybe I should invest a bit more money into a decent product, that doesn't get immediately fried just because the power grid fails once.
So, for the next one I spent 90€ and went for a well known brand.  
This had 2x USB-A 2.0, 1x USB-A 10Gbit, 1x USB-C 10Gbit, HDMI, DisplayPort, card reader, 3,5mm audio out and Gbit Ethernet.

<figure markdown>
{% set caption = "second hub from Anker with a bit more modern I/O" %}
![{{caption}}](./hub-2.avif){ loading=lazy }
<figcaption>{{caption}}</figcaption>
</figure>

With this, again, it seemed in the beginning everything is perfect.
But my laptop's USB-C Port still disables the charging function often times, if unplugged while online.
It's not happening always, but that actually makes it worse, as it's not predictable when it's gonna happen.

## Fried power brick

Some time down the road we had another power outage.
On this occasion there seemed to be a spike before the grid turned off.
I recognized it in the lights and some other places as well.
For my power brick it was too much: I heard a little spark and the power brick didn't do anything anymore, after the power came back, had an electric smell to it and was dark on the power-in side of the brick on disassembly.

So, a new power brick had to be bought.
Bad thing the new one does, which the old one didn't do: After some time the power brick makes a new power delivery negotiation out of the blue.
Practically that has the same effect as physically dis-/reconnecting hub and power brick from each other.

The effect for me: As soon as the negotiation happened, the laptop locked down the charging functionality in the port.
Then I ran on battery until I had the time and nerve, or ran out of battery life, to close everything, reboot to the BIOS and disable the battery to, assumingly, reset the controller.

## Workaround

As the laptop has no problem when the power brick is directly connected to it, I just switched to a setup where I have the USB extensions plugged to one of my 10Gbit USB-A ports on one side, as well as the HDMI and USB-C power brick on the other side.
Meaning I have now three instead of two things to un-/plug on leaving/coming to the desk.

## Who is at fault?

As much as it bothered me, it seemed it's not entirely (or at all?) the fault of the hubs.
Testing with a friends' laptop I discovered on his machine it's working fine through the latest hub I bought.
I can happily plug and unplug in any combinations it seems.
Therefore, the idea came up it maybe could be my laptop being the one at fault.

I set up a Windows 10 install on a USB-stick and had already another multi-distro-bootable one set up.
So, I tried to actively trigger the port charging lock down to figure out if it happens fully independent from the operating system, which would likely point to a hardware defect on my laptop, or if it's maybe a Linux bug.

On booting, immediately starting with un-/plug combinations, I couldn't get Windows to lock down the port.
On Linux I tried it with different kernel version on an Ubuntu.
But sadly I couldn't reliably reproduce it.
Sometimes I boot the current kernel and it just works fine a bunch of plugs in a row no problem, other times I can trigger it right away. :face_with_raised_eyebrow:

Maybe there is something else already in the boot process, that's not the same every time, having an effect on my testing.
But how should I discover this :see_no_evil:?

There are a few lines I saw in the dmesg output may or may not be connected to this:

```
ucsi_acpi USBC000:00: UCSI_GET_PDOS failed (-5)
...
ucsi_acpi USBC000:00: con1: failed to register partner alt modes (-5)
...
ucsi_acpi USBC000:00: ucsi_handle_connector_change: GET_CONNECTOR_STATUS failed (-110)
```

## Status Quo

I'm basically sitting here with a laptop with just one USB-C port, which I'm using solely for charging but nothing else.
I have no clue if it's a software thing, or a hardware defect.
I just opened a ticket on the Fedora Bug Tracker now, as I'm running that as productive system on this laptop right now.
But I don't have actually much hope this gets fixed that way :thinking:

Should I assume the port has maybe some (part)-defect?
I could send the device for repair, hoping they are able to reproduce the issue on their end.
But would that be worth the trouble and cost?
What would you do?
