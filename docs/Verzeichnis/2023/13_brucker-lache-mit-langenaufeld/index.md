---
tags:
    - Natur
    - Wald
    - Spaziergang

title: Brucker Lache mit Langenaufeld
description:
    "Kleiner Feierabend-Spaziergang durch die Brucker Lache, die vom Büro aus praktisch gelegen ist."

date: 2023-10-11 22:00
update: 2023-10-11 22:00
comments:
    host: mastodontech.de
    username: 9Lukas5
    id: 111218141745790908
image: title.avif
list_entry: true
---

# {{ title }}

![Wasserquelle](./title.avif){ data-description="Sprudelnde Wasserquelle neben dem Weg." loading=lazy }

<p class="introduction" markdown>
<span class="date">{{ date }}</span>  
{{ description }}
</p>

---

![OsmAnd Aufzeichung - Karte](./track-map.avif){ style="max-width: 24%;" data-description=".osm-and-map" loading=lazy }
![OsmAnd Aufzeichung - Kopfdaten](./track-details-01.avif){ style="max-width: 24%;" data-description=".osm-and-meta1" loading=lazy }
![OsmAnd Aufzeichung - Kopfdaten](./track-details-02.avif){ style="max-width: 24%;" data-description=".osm-and-meta2" loading=lazy }
![OsmAnd Aufzeichung - Höhen-/Geschwindkeits-Graph](./track-details-03.avif){ style="max-width: 24%;" data-description=".osm-and-graph" loading=lazy }
<div class="glightbox-desc osm-and-map">
    Aufzeichnung der gelaufenen Strecke auf Kartenausschnitt eingezeichnet
</div>
<div class="glightbox-desc osm-and-meta1">
    Kopfdaten des Tracks mit
    <ul>
        <li>4,2 km Entfernung
        <li>127m Bergauf
        <li>117m Bergab
        <li>279-315m Höhenbereich
    </ul>
</div>
<div class="glightbox-desc osm-and-meta2">
    Kopfdaten des Tracks mit
    <ul>
        <li>5,1 km/h Durchschnittsgeschwindigkeit
        <li>8,6 km/h Höchstgeschwindigkeit
        <li>51 Minuten Dauer
        <li>48 Minuten in Bewegung
    </ul>
</div>
<div class="glightbox-desc osm-and-graph">
    Graph über den Track mit Geschwindigkeit in orange und Höhe in blau
</div>

Ich hab das Waldstück süd-östlich vom Siemens-Campus schon einige Male auf der Karte beäugt.
Da es heute nochmal angenehm warm war und das Freibad schon längst zu hat, hab ich um etwas Bewegung zu haben heute eine Runde durch die _Brucker Lache_ gemacht.

Eigentlich genial, wie nah an der Stadt man einfach so ne Stunde durch den Wald laufen kann:

![Wald](./wald-01.avif){ style="max-width: 19%;" data-description=".wald-1" loading=lazy }
![Wald](./wald-02.avif){ style="max-width: 19%;" data-description=".wald-2" loading=lazy }
![Wald](./wald-03.avif){ style="max-width: 19%;" data-description=".wald-3" loading=lazy }
![Wald](./wald-04.avif){ style="max-width: 19%;" data-description=".wald-4" loading=lazy }
![Wald](./wald-05.avif){ style="max-width: 19%;" data-description=".wald-5" loading=lazy }
<div class="glightbox-desc wald-1">
    Blick über Waldweg
</div>
<div class="glightbox-desc wald-2">
    Blick über Waldweg
</div>
<div class="glightbox-desc wald-3">
    Blick in ein Waldstück in dem jede Menege loses Holz liegt.
    Würde sich richtig lohnen das auszuräumen, denke das wird bestimmt vom Förster irgendwie ausgeschrieben, wer es mitnehmen darf, oder so^^
</div>
<div class="glightbox-desc wald-4">
    Blick in Waldstück und die Baumkronen
</div>
<div class="glightbox-desc wald-5">
    Blick über engen Waldweg
</div>

Unterwegs hab ich auf den engeren Trampelpfaden auch zwei-/dreimal Vollernter-Schneißen gekreuzt.
Hinterlassen ganz schöne Spuren im Boden die Dinger :face_with_open_eyes_and_hand_over_mouth:

![Vollernter Spuren](./vollernter-01.avif){ style="max-width: 24%;" data-description=".vollernter" loading=lazy }
![Vollernter Spuren](./vollernter-02.avif){ style="max-width: 24%;" data-description=".vollernter" loading=lazy }
<div class="glightbox-desc vollernter">
    Im Waldboden sind breite Spuren zu erkennen und eine Schneiße, die vermutlich von einem Vollernter herrühren.
</div>

An einem Abschnitt bin ich an einer gemähten Wiese vorbeigekommen, hab eine sprudelnde Quelle gefunden und ein Vogelhäuschen gesehen :hugging:

![Gemähte Wiese](./wiese.avif){ style="max-width: 33%;" data-description=".wiese" loading=lazy }
![Quelle](./quelle.avif){ style="max-width: 33%;" data-description=".quelle" loading=lazy }
![Vogelhaus](./vogelhaus.avif){ style="max-width: 32%;" data-description=".vogelhaus" loading=lazy }
<div class="glightbox-desc wiese">
    Blick über gemähte Wiese, Baumreihe im Hintergrund und blauer Himmel.
</div>
<div class="glightbox-desc quelle">
    In einer von weitem erst wie eine gewöhnliche Pfütze nebem dem Weg vermuteten Wasserlache hat es aktiv gesprudelt.
</div>
<div class="glightbox-desc vogelhaus">
    Neben dem Weg stand ein Vogelhaus auf einem Baumstumpf, scheinbar einfach aus herumliegendem Holz und einem Stein zum Beschweren oben drauf gemacht.
</div>


![Blick in Wald](./sudtirol-ahnlich.avif){ align=right style="max-width: 75%;" loading=lazy }
<div class="glightbox-desc sudtirol-like">
    Viele dünne hohe Nadelbäume.
    Auf dem Boden liegen einige Zapfen herum.
    Zwischen den großzügig voneinander stehenden dünnen Bäumen schlängelt sich ein Trampelpfad.
</div>

An der einen Stelle ging es von unten auf eine kleine Anhöhe, deren Anblick würde man so genauso auch in Südtirol finden.
Da hat es mich in den Erinnerungen direkt für einen kurzen Moment in die Berge katapultiert :relieved:
<br>
<br>
<br>
<br>
<br>
<br>

So, damit hab ich meine Bilder von heute los und konnte mal wieder einen Eintrag im Blog hinterlassen, statt es _einfach_ nur auf Social Media raus zu hauen :upside_down_face:
