---
tags:
    - Ritten
    - Renon
    - Südtirol
    - Alto Adige
    - Rittnerbahn
    - Tourismus
    - Wandern

title: Tagestourismus am Ritten - Fluch oder Segen?
description:
    "Die Rittnerbahn stand einmal kurz vor der Stilllegung.
    Mittlerweile ist sie so gut besucht, dass das nicht mehr vorstellbar ist.
    Aber wie viel Tagestourismus ist noch gesund?"

date: 2023-12-05 21:00
update: 2023-12-05 21:00
comments:
   host: mastodontech.de
   username: 9Lukas5
   id: 111529579359428972
image: header.avif
list_entry: true
---

# {{ title }}

![Station Oberbozen](./header.avif){ data-description=".header-desc" loading=lazy }
<div class="glightbox-desc header-desc">
    Im Rücken der Kamera ist die Werkstatt der Rittnerbahn, von links kommt die Strecke aus Klobenstein an.
    Der Blick geht von der Werkstatt über ein paar Weichen in Richtung Bahnsteig in Oberbozen.
    Im Hintergrund hinter den Bäumen ist die Bergstation der Seilbahn zu erkennen.
</div>

<p class="introduction" markdown>
<span class="date">{{ date }}</span>  
{{ description }}
</p>

---

Ich komme jedes Jahr auf den Ritten.
Bin am ersten Tag gleich wieder die 0815 Runde gelaufen, und habe dabei mal wieder realisiert:
Der Berg und sein Bahnl haben sich verändert über die Jahrzehnte.  
Den Blog-Eintrag werde ich deshalb in zwei Teile spalten:

- [Die Wanderung und Bilder davon](#wanderung)
- [Ein paar Worte zur Bahn](#die-rittnerbahn)

## Wanderung

![OSMAnd Karte mit Track-Linie](./route-gesamt.avif){ style="max-width: 79%; max-height: 15em;" loading=lazy }
![GPS-Track Details](./route-meta.avif){ style="max-width: 20%; max-height: 15em;" data-description=".gps-details-desc" data-caption-position=right loading=lazy }
<div class="glightbox-desc gps-details-desc">
    <ul>
        <li>Entfernung: 13 km
        <li>Zeit in Bewegung: 2h 13m
        <li>Durchschnittsgeschwindigkeit: 5,8 km/h
        <li>Höchstgeschwindigkeit: 11,2 km/h
        <li>Höhenbereich: 1022 - 1298m
        <li>Bergauf: 450m
        <li>Bergab: 440m
    </ul>
</div>

Ich verlasse Klobenstein über den alten "1er", der mittlerweile die Nummer 35 trägt und "[Freud Promenade][ritten-com_wandern]" heißt.
Unterwegs gibt es entsprechend eine Menge Info-Tafeln zu Freud, wen es interessiert :information_desk_person:
Aber Vorsicht: Die Lesezeit ist in die Wegzeit-Angaben auf den Schildern nicht eingerechnet :point_up_tone2: :zany_face:

Direkt am Anfang des 1er und dem Ortsrand Klobensteins liegt das "Haus Edelweis" mit einem ungehinderten Blick auf den Schlern im Hintergrund.  
Von dort verläuft der Weg erstmal ein gutes Stück oberhalb der Bahn durch den Wald.

![Haus Edelweis / Schlern](./Haus-Edelweiss.avif){ style="max-width: 19%;" loading=lazy }
![Breiter Waldweg](./Wald-01.avif){ style="max-width: 19%;" loading=lazy }
![Brücke über Bach](./Wald-02.avif){ style="max-width: 19%;" loading=lazy }
![Brücke über Bach](./Wald-03.avif){ style="max-width: 19%;" loading=lazy }
![Bachschneiße im Wald](./Wald-04.avif){ style="max-width: 19%;" loading=lazy }

Nach einem guten Stück kommt man oberhalb der "Adler Lodge" vorbei.
Das Resort liegt direkt oberhalb der Bahntrasse und ein gutes Stück unterhalb des darüber verlaufenden Wanderwegs.

Statt dem 1er hier weiter zu folgen, nehme ich den Abzweig direkt neben dem Resort Gelände nach unten.
Dieser kommt direkt neben der ursprünglich ersten normalen Station Rappersbichl vorbei.

![Adler-Lodge / Schlern](./Adler-1.avif){ style="max-width: 24%;" loading=lazy }
![Adler-Lodge](./Adler-2.avif){ style="max-width: 24%;" loading=lazy }
![Station Rappersbichl](./Colle.avif){ style="max-width: 24%;" data-description=".colle" loading=lazy }
<div class="glightbox-desc colle">
    Blick ist Richtung Klobenstein.
    Der Weg an der Adler-Lodge runter hat am im Bild zu sehenden Gleisbogen vorbeigeführt.
    Im Rücken der Kamera ist die Wiese in Richtung Lichtenstern.
</div>


Von dort aus geht der Wanderweg direkt wieder ein kleines Stück nach oben zum Waldrand.
Die Bahn führt in einem Bogen unterhalb, der Fußweg oberhalb einer Wiese entlang.
Anschließend treffen sich beide an der Ausweichstation an der Mitte der Strecke: Lichtenstern.

Dort ist auch ein Trinkwasser-Reservoir zu finden.

![Wiese, Rinnsal und Bahntrasse](./colle-stella-1.avif){ style="max-width: 24%;" loading=lazy }
![Lichtensterner See (Trinkwasserspeicher)](./colle-stella-2.avif){ style="max-width: 24%;" loading=lazy }
![Auseichstelle Lichtenstern](./Stella-1.avif){ style="max-width: 24%;" loading=lazy }
![Auseichstelle Lichtenstern](./Stella-2.avif){ style="max-width: 24%;" loading=lazy }

An dieser Stelle habe ich die Bahntrasse dann gequert und einen Abstecher über den Hügel südlich der Trasse gemacht.
Wem das zu steil ist, kann auch einfach nördlich der Trasse weiter laufen und kommt so nahezu ohne Höhenunterschied zu derselben Stelle beim Bienenmuseum Wolfsgruben.

Der südliche Weg führt zuerst steil nach oben und danach auf der anderen Seite wie die Bahntrasse wieder hinab.
Mit dem vielen Laub und jetzt frisch darauf gekommenem Schnee eine nicht ganz triviale Angelegenheit da herunterzukommen :grimacing:  
Am Ende geht es dann wieder übers Gleis.
In Deutschland hätte man hier vermutlich überall Andreas-Kreuze stehen, oder so ein schmaler Übergang wäre nicht einmal zulässig.
Hier steht dagegen einfach nur ein simples Schild :shrug:

![Zugefrorener Mini-See](./stella-costalovara-1.avif){ style="max-width: 24%; max-height: 8em;" loading=lazy }
![Gefälle mit Laub und frischem Schnee](./stella-costalovara-2.avif){ style="max-width: 24%; max-height: 8em;" loading=lazy }
![Gleisüberweg](./stella-costalovara-3.avif){ style="max-width: 24%; max-height: 8em;" loading=lazy }
![Bahntrasse vom Bienenmuseum aus](./stella-costalovara-4.avif){ style="max-width: 24%; max-height: 8em;" loading=lazy }

Vom Bienenmuseum noch ein Stück weiter kommt dann die Bahnstation von Wolfgruben.
Etwas oberhalb dieser ist das "Haus der Familie", wo man im Sommer sehr schön draußen für Kaffe & Kuchen Rast machen kann.

![Station Wolfsgruben](./Costalovara.avif){ style="max-width: 24%;" data-description=".costalovara" loading=lazy }
<div class="glightbox-desc costalovara">
    Blick ist Richtung Klobenstein.
    Direkt rechts ist der Überweg zum Bahnsteig, im Rücken der Kamera der Weg zum Haus der Familie.
</div>

Ich bin jetzt nur vorbeigelaufen und von dort in den Wald, um wieder zurück nach oben auf den 1er (bzw. den 35er aka Freud Promenade) zu kommen.
Wer nochmal einen Blick auf die Karte oben wirft, wird feststellen ich hätte auch einfach an der Straße hoch laufen können.
Aber ich will ja dieses Flair von schmalen Fußwegen durch die Natur haben, daher dieser Haken.

![Wald mit quasi nicht erkennbarem Trampelpfad](./costalovara-1er-01.avif){ style="max-width: 24%; max-height: 8em;" loading=lazy }
![Wald mit quasi nicht erkennbarem Trampelpfad](./costalovara-1er-02.avif){ style="max-width: 24%; max-height: 8em;" loading=lazy }
![Kleiner Wasserlauf führt über den Weg](./costalovara-1er-03.avif){ style="max-width: 24%; max-height: 8em;" loading=lazy }
![Kleiner Wasserlauf führt über den Weg](./costalovara-1er-04.avif){ style="max-width: 24%; max-height: 8em;" data-description="Am zweiten Tag war es wieder so wenig und so kalt, dass nur noch Eis übrig war." loading=lazy }

Angekommen an der primären Endstation Oberbozen, der Streckenteil weiter bis Maria Himmelfahrt wird nur eine Hand voll mal am Tag befahren, offenbart sich einem auch direkt der Tagestourismus in seiner vollen Blüte:

Der Zug steht gute 10 Minuten bevor er wieder fährt.
Die Leute strömen rein und stehen dicht an dicht und es bleiben trotzdem noch oft welche am Bahnsteig zurück.  
Vor der Gondel sieht es nicht besser aus, die Schlange arbeitet sich eben langsam ins Gebäude und nach Bozen.

Wartet man einen Zug ab, wiederholt sich das Schauspiel aufs Neue :shrug:
Ich habe dieses Mal tatsächlich nicht darauf geachtet, ob die Schlange an der Gondel zwischenzeitlich mal ganz verschwunden war, aber habe es schon häufig genug gesehen, dass nicht^^

![Station Oberbozen](./soprabolzano-1.avif){ style="max-width: 19%; max-height: 6em;" data-description=".soprabolzano" loading=lazy }
![Zug nach Fahrgastwechsel](./soprabolzano-2.avif){ style="max-width: 19%; max-height: 6em;" loading=lazy }
![Schlange vor der Gondel](./soprabolzano-3.avif){ style="max-width: 19%; max-height: 6em;" loading=lazy }
![Zug nach Fahrgastwechsel (Runde 2)](./soprabolzano-4.avif){ style="max-width: 19%; max-height: 6em;" loading=lazy }
![Schlange vor der Gondel (Runde 2)](./soprabolzano-5.avif){ style="max-width: 19%; max-height: 6em;" loading=lazy }
<div class="glightbox-desc soprabolzano">
    Blick Richtung Klobenstein, rechts das Gondelgebäude.
    Auf dem Bahnhofsvorplatz ist der Christ-Bahnl Weihnachtsmarkt zugange und der Zug steht da.
</div>

Da ich kein Bedürfnis hatte mich mit denen allen in den Zug zu quetschen, hab ich mich lieber wieder zu Fuß aufgemacht. :walking:  
Für den Rückweg hab ich dann allerdings keine Umwege genommen, sondern bin einfach den kürzesten Weg über den 1er zurückgelaufen.
Der Zug braucht ~15min, zu Fuß sind es ~45-60min (wenn man halt nicht an allen Schildern ewig rumsteht :information_desk_person:)

[ritten-com_wandern]: https://www.ritten.com/de/3105-wandern

## Die Rittnerbahn

![Zweiachser Tw11 und Alioth](./werkstatt-1.avif){ style="max-width: 49%;" loading=lazy }
![Schweizer Triebzug](./werkstatt-2.avif){ style="max-width: 49%;" loading=lazy }

Heute stehen die alten Wagen fast ausschließlich in den Remisen.
Den 30-Minuten Takt zwischen Oberbozen und Klobenstein stemmen immer zwei der drei Schweizer Fahrzeuge mit je zwei Wagen.

### Touristenmassen

Trotz des Halbstundentaktes mit diesen Fahrzeugen: Die Umlaufgondel fördert im 3-4 Minuten Takt (je nach eingesetzter Gondelzahl) ~25 Leute aus dem Tal.
Die allermeisten davon sind mittlerweile Tagestouristen die von Bozen oder womöglich sogar dem Umland kommen, hochfahren, mit dem Bahnl rüber und zu den Erdpyramiden laufen.
Ab dem Nachmittag läuft das ganze dann, für gewöhnlich und überwiegend, wieder retour.

Dank dem Ganz-Südtirol-Nahverkehrsticket (3 oder 7 Tage), in dem auch die Gondel und die Ritterbahn enthalten sind, ist das natürlich recht gedankenlos möglich. :shrug:  
Durch diese Massen von Leuten komme ich mittlerweile häufig am Bahnhof vorbei und laufe dann aber lieber kopfschüttelnd rüber, statt mitzufahren.
Dauert ja auch nur eine knappe Stunde und ist eh gesund.

In den ersten Jahren der Umlaufgondel bin ich zum Teil aus Faulheit lieber mit der Bahn rüber und der Gondel runter in den Supermarkt dort, statt einfach in Klobenstein im Ort in den Supermakrt zu gehen. :upside_down_face:  
Irgendwann kam der Moment, wo ich in der Talstation die Treppe herunterkam und sich die Schlange fürs Hochfahren bis vor die Tür zog. :flushed:
Nach dem Einkaufen habe ich mich in die Bäckerei gesetzt, Kaffe und Kuchen bestellt und von da die Schlange beobachtet.
Zu meiner Enttäuschung kam das Ende aber nie zur Tür rein. :neutral_face:  
Ich hatte dann allerdings die Frechheit besessen die Schlange orthogonal zu queren, mit dem Aufzug in die Abfahrtsetage zu fahren und mich da mit einzureihen.
So habe ich es dann immerhin in die zweite oder dritte Gondel ab dem Moment geschafft, statt mich ganz hinten anzustellen. :face_with_peeking_eye:

Meine Motivation ins Tal zu fahren ist seitdem deutlich geschrumpft.
Sowohl für Zug als auch Gondel warte ich mittlerweile lieber bis spät Abends ab, oder stehe morgens gleich frühs für den ersten Zug auf. :pensive:

Das Ganze war aber auch mal anders :eyes:

### Von der Vergangenheit ins Hier und Jetzt

![Esslinger Tw12](./esslinger.avif){ align=left style="max-width: 50%;" loading=lazy }

Kann sich jemand zufällig noch an diesen Zeitgenossen hier erinnern?

Die überwiegende Zeit, die ich auf den Ritten komme, lag die Hauptlast des Verkehrs hier oben auf dem Tw12 von der END (Straßenbahn Esslingen-Nellingen-Denkendorf)[^tw12-end]. :relieved:  
Das erreicht allerdings bald den Break-Even-Point: Ich komme jetzt seit 28 Jahren her, 15 davon war der Esslinger das modernste Fahrzeug hier oben.
Die schweizer Fahrzeuge sind erstmalig 2010 im Fahrgasteinsatz gewesen und damit mittlerweile auch bereits seit 13 Jahren hier oben unterwegs.

Bevor die Gondel erneuert wurde, waren es zwei große Kabinen die hin und her gefahren sind und jeweils mit Personal besetzt waren.
Gefahren sind die Gondeln natürlich auch nicht im Vier-Minuten-Takt, sondern irgendwas zwischen alle 30 und 60 Minuten.
Je nach Tageszeit fuhr sie auch etwas enger oder weiter getaktet glaube ich. :thinking:  
Passend dazu, fuhr die Bahn lediglich im Stundentakt mit einem Fahrzeug, dem Esslinger Tw12, hin und her.
Und da war in der Regel immer genügend Platz drin :speak_no_evil:

Damals gab es dieses Südtirolweite Ticket noch nicht und die Gondel war eben lang nicht so leistungsfähig wie heute.
Damit hatte die Bahn primär die Bergbewohner und die Touristen die _auf dem Berg_ Urlaub gemacht haben zu bewegen.
Die Touristen, die man damals dort oben hatte, waren gezielt auf dem Berg und hatten dort ihre Unterkunft.
Tagestourismus gab es eher wenig.

Auch die Haltestellen haben ganz anders ausgesehen. Heute glänzen diese ja förmlich und haben alle einen Bahnsteig der für die Schweizer lang genug ist.
Davor waren eigentlich nur die Stationen Klobenstein, Rappersbichl, Lichtenstern, Wolfsgruben und Oberbozen richtig als Stationen erkennbar.
Die anderen Bedarfshalte waren oft nur eine Holzschwelle quer gelegt und ein bisschen Schotter aufgeschüttet für die erste Tür.
Außerdem gab es an allen möglichen Ecken, wie kleinen Überwegen oder Bahnübergängen, inoffizielle Zwischenstopps, wenn die richtigen Bewohner im Zug saßen oder reinwollten :nerd_face:  

2009 wurde die Umlaufgondel in Betrieb genommen und es musste seit Ewigkeiten wieder ein 30-Minuten-Takt mit Begegnungsverkehr eingeführt werden.
Gestemmt wurde das Fahrgastaufkommen zuerst noch vom Esslinger Tw12 und einem der großen alten Wagen, dem "Vierachser" (Tw2) oder "Alioth".
2010 kam dann der erste der Schweizer Züge ins Spiel.
[^eisenbahn-amateur_RittnerBahn]

Und damit war quasi das Flair in gewisser Weise besiegelt.
Weder die alten Wagen, noch der Esslinger kommen noch wirklich zum Einsatz, da die Züge selbst morgens um 10 bereits zu voll sind dafür.
Damit einhergehend gibt es nur noch abgetrennte Führerstände, was zuvor ja auch keine existierten, und ein so bürgernaher Betrieb wie früher ist heute nicht mehr denkbar.  
Aus dem beschaulichen kleinen Bahnl auf dem Berg ist ein in Fahrgästen ertrinkender Zugverkehr geworden.

[^eisenbahn-amateur_RittnerBahn]: [Eisenbahn Amateur: Die Rittner Bahn - eine Erfolgsgeschichte][eisenbahn-amateur_RittnerBahn]
[^tw12-end]: [Strassenbahn Magazin: Einmal Ritten und zurück][tw12-end]

[eisenbahn-amateur_RittnerBahn]: https://eisenbahn-amateur.ch/2019/12/13/die-rittner-bahn-eine-erfolgsgeschichte/
[tw12-end]: https://strassenbahn-magazin.de/leseprobe/einmal-ritten-und-zurueck

### Und die Zukunft?

Die Schweizer Züge im 30-Minuten-Takt reichen eigentlich nicht mehr aus, die Fahrzeuge sind technisch durch und im Personal schwimmen tut man auch hier nicht.
Wie jeder Nahverkehr ist auch hier europäisches Vergaberecht unterwegs und die Leute sind nicht so wirklich zufrieden hat man den Eindruck.

Kriegt man bei der Organisationsstruktur wieder die Kurve?
Ich kann es nur hoffen.  
Ob man sich wohl zum Kauf neuer Fahrzeuge durchringen kann, bevor die Schweizer tatsächlich aufgeben?
Wer weiß.

Eine andere grundsätzliche Frage ist, wie man sich vorstellt in Zukunft das Fahrgastaufkommen zu bewältigen.
Ob man wohl über so drastische Maßnahmen nachdenkt wie weitere Ausweichstellen, Signalsystem, etc?  
Wirklich vostellen kann ich es mir nicht.
Wird wohl viel eher ein "immer weiter so, mehr Infrastruktur geht nicht" sein.
Aber irgendwas muss (oder zumindest sollte) man sich halt überlegen, wie man das in Zukunft bewältigen will.

Aber damit solls das gewesen sein :raising_hand:
