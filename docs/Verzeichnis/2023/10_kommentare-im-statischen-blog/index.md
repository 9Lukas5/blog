---
tags:
    - Fediverse
    - MkDocs
    - Static Site Generator
    - Comment System

title: Kommentare in statischem Blog via Fediverse
description:
    "Einbindung der Antworten eines Fediverse Posts als Kommentare in den statischen Blog"
date: 2023-07-06 00:00
update: 2023-07-06 00:00

comments:
    host: mastodontech.de
    username: 9Lukas5
    id: 110668787228123229
list_entry: true
---

# {{ title }}

<p class="introduction" markdown>
<span class="date">{{ date }}</span>  
{{ description }}
</p>

---

Auf einer rein statischen Seite eine Kommentarfunktion einbauen?
Klar, einfach eine Fediverse Anbindung reinbasteln :face_with_cowboy_hat:

Ich werd jetzt nicht unnötigerweise einen langen Blog-Eintrag daraus basteln.
Hab das ganze einfach mal gesucht und bin letztlich auf eine [Beispiel-Implementation][bsp-impl] von [@carlschwan@floss.social][@carlschwan@floss.social] gestoßen.

Die hab ich quasi einfach kopiert, purify.min.js geladen und mit in meine assets gepackt und das CSS etwas angepasst, dass es primär die Farb-Variablen vom MkDocs nutzt.

Kleines Problem hatte ich mit dem CSS-Nesting, dass es irgendwie bei mir nicht gegeriffen hat (Firefox) :thinking:
Aber mir wars zu blöd da jetzt lange zu debuggen, da hab ichs einfach den langen Weg geschrieben :shrug:

Beispiel:

=== "Wie es eigentlich aussehen sollte"

    ```css
    .outer {
        .inner {
            ...
        }
    }
    ```

=== "Wie ich es jetzt im Stylesheet hab"

    ```css
    .outer {
        ...
    }

    .outer .inner {
        ...
    }
    ```

[bsp-impl]: https://floss.social/@carlschwan/109774012599031406
[@carlschwan@floss.social]: https://mastodontech.de/@carlschwan@floss.social
