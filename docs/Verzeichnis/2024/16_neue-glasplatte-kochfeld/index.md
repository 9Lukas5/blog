---
tags:
    - Heimwerker
    - Küche
    - Herd

title: Neue Glasplatte fürs Kochfeld
description:
    "Die Glasplatte des Kochfeldes hatte nach punktuell konzentrierter Einwirkung kinetischer Energie einen Sprung und musste ersetzt werden."

date: 2024-05-05 14:00
update: 2024-05-05 14:00
comments:
   host: mastodontech.de
   username: 9Lukas5
   id: 112388356555668260
image: kochfeld-mit-neuer-glasplatte-laeuft.avif
list_entry: true
---

# {{ title }}

![Ceran-Kochfeld](./kochfeld-mit-neuer-glasplatte-laeuft.avif){ data-description=".header-desc" loading=lazy }
<div class="glightbox-desc header-desc">
    Kochfeld mit neuer Glasplatte und allen vier Platten in Betrieb.
</div>

<p class="introduction" markdown>
<span class="date">{{ date }}</span>  
{{ description }}
</p>

---

## Schadensbilanz

![Glasplatte mit Sprung](./sprung.avif){ align=left style="max-width: 33%;" data-description=".sprung-desc" loading=lazy }
<div class="glightbox-desc sprung-desc">
    Nachdem ein schwerer Topf gepaart mit Schwung Energie mit dem Eck auf der Glasplatte aufgekommen ist, hatte sie einen Sprung von diesem Eck ausgehend.<br>
    <br>
    Der hier zu sehende Zustand ist vom Ausbau, bei dem ein größeres Stück ausgebrochen ist.
    Bis dahin war die Platte noch in einem nutzbaren Zustand.
</div>

Fragt micht nicht wie genau es passiert ist.
Die mittlerweile Ex-Mitbewohnerin hat das nur 2-3 Wochen vor ihrem Auszug geschafft. :see_no_evil:

Was ich weiß: Sie hatte ein komplettes gefrorenes Hähnchen in einem entsprechend großen Topf. 
Beim Bewegen des ganzen auf den Herd ist sie damit mit Schwung auf der Glasplatte aufgekommen. :information_desk_person:

Meine Vermutung:
Auf dem Weg zum Herd irgendwie aus dem Gleichgewicht gekommen.
In der Folge mit dem schweren Topf, erweitert um die Energie des eigenen Körpers, in Richung Herd gekippt.
Den Topf kann/will man ja auch nicht einfach fallen lassen, also fängt man sich einfach mit/durch den Topf.  
Wäre der Topf mit einer breiteren Fläche aufgekommen, hätte die Glasplatte das vermutlich wegstecken können. :thinking:
Offenbar muss dieser aber mit der Kante aufgekommen sein, was die gesamte kinetische Enerige in diesem einen Punkt abgeleitet hat. :grimacing:

Als Folge davon ist am Aufprallpunkt ein kleines Stück Glas ausgebrochen.
Es war aber kein Loch im Glas, das den Herd unbenutzbar gemacht hätte.


## Ersatzteilbeschaffung

Nach schneller Recherche hat sich ergeben, dass es die Glasplatte einzeln für 100€ zu kaufen gibt. :eyes:
Mit dieser Option muss das Kochfeld, das ja noch einwandfrei funktioniert, nicht getauscht und damit auch der Vermieter nicht involviert werden. :ok_hand_tone2:  
Die Kosten sind dann halt einfach bei der Kaution der jetzt Ex-Mitbewohnerin abgezogen worden. :shrug:

Auf der Website stand "lieferbar in voraussichtlich 14 Tagen", was der 17. April gewesen wäre.  
Später kam dann eine Korrektur des Liefertermins auf voraussichtlich 24. April :face_with_monocle:

> 2 Wochen später...

Korrektur des voraussichtlichen Liefertermins um *weitere* 8 Wochen :flushed:

> 1 Woche später...

Glasplatte ist im Versand, hier Sendungsverfolgung :wave_tone2:

Zwischenzeitlich hatten wir schon überlegt einfach für 150€ ein neues Induktionskochfeld zu kaufen, falls sie die Glasplatte am Ende gar nicht mehr finden sollten :thinking: :zany_face:  
Aber dann haben sie sie ja doch gefunden und sie kam am 4. Mai tatsächlich an :upside_down_face:


## Demontage

!!! danger "Arbeitssicherheit: Strom"
    Vor irgendeiner Tätigkeit rund um den Herd/Ofen sicherstellen, dass dieser spannungsfrei ist

![Abgesprunges Eck an Glasplatte](./hebel-unfall.avif){ align=right style="max-width: 33%;" data-description=".hebel-unfall-desc" loading=lazy }
<div class="glightbox-desc hebel-unfall-desc">
    Nach Schraubenzieher drunter und nur *ganz leicht* gehebelt, hat sich hier ein Eck vom Glas verabschiedet.
</div>

Ich weiß nicht wie es euch geht, aber ich hatte bisher noch genau keinen Herd oder Backofen (de-)montiert. :information_desk_person:
Und Anleitungen lesen ... macht ja keiner sowas :thinking: :shrug:

Unter der naiven Annahme, dass das Kochfeld "einfach nur" von oben aufliegt, habe ich mal damit angefangen mit einem Schlitz-Schraubenzieher unter den Glasrand zu drücken und etwas zu hebeln.  
Ich dachte zwar ich wäre vorsichtig gewesen, aber so eingespannt war auch das schon zu viel :arrow_right: An besagtem Eck ist die Platte gesplittert :face_with_peeking_eye:

Naja gut, das Kochfeld scheint also irgendwie da drin fest gemacht zu sein.
Ergo, Zeit herauszufinden wie und damit war verbunden den Ofen vorzuziehen :face_with_rolling_eyes:  
Versuch 1:
Einfach mal dran ziehen.
Klar, bewegt sich nix :smiling_face_with_tear:

![Regler und Ofen](./ofen-befestigung.avif){ align=left style="max-width: 50%;" data-description=".ofen-befestigung-desc" loading=lazy }
<div class="glightbox-desc ofen-befestigung-desc">
    Regler für Ofen und das Kochfeld und oberer Ausschnitt der Ofen-Öffnung.
    Markiert sind links und rechts je ein Schraubenkopf in der Länge der Ofen-Öffnung.
</div>

Versuch 2:
Annahme, dass unter der Regler-Blende etwas zu finden ist.
Ich hatte die Blende schon ab und habe dort nach einer Befestingung gesucht. :face_with_monocle:  
Aber glaubt ihr, ich hätte die beiden Schrauben links und rechts im Blech als die beiden Verantwortlichen ausgemacht?
Natürlich nicht, die hab ich erst als letztes probiert :joy:

![Ofen vorgezogen](./ofen-ausgebaut.avif){ align=right style="max-width: 33%;" data-description=".ofen-ausgebaut-desc" loading=lazy }
<div class="glightbox-desc ofen-ausgebaut-desc">
    Der Ofen ist nach vorne raus gezogen und steht gekippt vorne auf dem Küchenboden, hinten noch auf dem Brett.
</div>

Aber gut, jetzt konnte ich endlich den Ofen bewegen und das Kochfeld, gespeist aus der Zuleitung des Ofens, abstecken.
Mit dem Blick unter das Kochfeld waren dessen Befestigungen offensichtlich: An jedem Eck ist eine Klammer festgeschraubt die das Feld an die Arbeitsplatte heranzieht.
Völlig klar, dass sich das Kochfeld da kein Stück bewegt hat und stattdessen das Glas nachgegeben hat.  
Also die vier Klammeran abschrauben und das Kochfeld mit Druck von unten nach oben raus heben. :information_desk_person:
Alles völlig straight forward und sollte kein Problem mehr sein, oder? :eyes:
ODER? :pleading_face:

Natürlich nicht....wäre ja auch zu einfach gewesen :unamused:


Erinnert ihr euch an das Bild von oben, mit dem herausgebrochenen Stück am Eck?
Ich habe euch das mal bisschen vergrößert und Markierungen reingemacht.

![Reste der alten Dichtung und Klebereste](./platte-angeklebt.avif){ align=left style="max-width: 50%;" data-description=".platte-angeklebt-desc" loading=lazy }
<div class="glightbox-desc platte-angeklebt-desc">
    Eck des Kochfelds, noch in der Arbeitsplatte befindlich.
    Das Eck ist ausgebrochen.<br>
    <br>
    Rote Markierung: Alte Dichtung die an der Glasplatte angeklebt war.<br>
    Blaue Markierung: getrockneter Klebstoff den es unter der Dichtung herausgedrückt haben muss.
</div>

Das rot markierte ist das umlaufende Dichtmaterial der alten Glasplatte.
An der Stelle des herausgebrochenen Stücks sieht man den Klebestreifen zwischen Glasplatte und Dichtmaterial.
Am Rand links unten ist der Klebstreifen abgegangen und man kann das Dichtmaterial erahnen.

Das Material ist kein Silikon oder eine einfache Gummilippe, sondern wirkt ein bisschen schaumstoffartig und hat damit einen gewissen Federeffekt mit dem es sich von der Glasplatte abdrückt.

Jetzt kommen wir aber zu dem blau markierten.
Das sieht aus und hat sich angefühlt wie simpler Flüssigkleber. :face_with_raised_eyebrow:
Tja, stellt sich raus: Das Kochfeld ist nicht einfach nur festgeklemmt gewesen, sondern es wurde ringsum zwischen die Arbeitsplatte und das Dichtmaterial der Glasplatte Klebstoff aufgetragen.

Ich hätte das ja verstanden, wenn man beispielsweise Unebenheiten in der Pressspanplatte um den Ausschnitt herum nicht traut, dass man auf die Arbeitsplatte Silikon aufträgt.
Das müsste dann aber vorher passend platziert aufgebracht und trocknen gelassen werden.
Dann hätte man eine nicht mehr klebende, aber dichte und flexible Füllung gegen die sich die Glasplatte andrücken kann.

Aber nein, stattdessen sieht es aus als hätte jemand auf die Schaumstoff-Dichtung Klebstoff aufgebracht und dann das Kochfeld befestigt.

![Arbeitsplatte ohne eingelassenes Kochfeld](./arbeitsflaeche-ausschnitt.avif){ align=right style="max-width: 50%;" data-description=".arbeitsflaeche-ausschnitt-desc" loading=lazy }
<div class="glightbox-desc arbeitsflaeche-ausschnitt-desc">
    Ausschnitt in Arbeitsfläche ohne das Kochfeld.
    Ringsum sind noch die Klebereste und die alte Dichtung zu erkennen die auf der Platte zurück geblieben sind.
</div>

Also habe ich mit dem Taschenmesser ringsum durch die Dichtungs-Klebstoff-Masse geschnitten und versucht das Kochfeld zu lösen. :knife:  
Letztlich ist es mir auch gelungen und ich hatte es raus.
Es sind dann eben auf der Arbeitsfläche eine Menge Reste der Klebstoff-Dichtungs-Masse zurückgeblieben, die ich mit einem Schraubenzieher abgeschabt habe. :screwdriver:


## Reparatur

![Kochfeld Innenansicht ohne Glasplatte](./kochfeld-innen.avif){ align=left style="max-width: 50%;" data-description=".kochfeld-innen-desc" loading=lazy }
<div class="glightbox-desc kochfeld-innen-desc">
    Jede "Platte" besteht aus einer Runden Schale in der ZickZack-Metallbänder im Kreis verlaufend untergebracht sind.<br>
    In der Mitte des Kochfeldes ist die Verkabelung untergebracht.
</div>

Nach dem Ausbau war die eigentliche Reparatur dann völlig banal :sweat_smile:
Außenrum sind die Metallränder der Glasplatte und des Kochfeldes einfach mit ein paar Schrauben verbunden.
Raus drehen, Glasplatte abheben, neue drauflegen, festschrauben, fertig. :shrug:

Hatte bisher ja nie ein Kochfeld aufgemacht, also keine Ahnung was ich so erwartet hatte.
Aber so lange man nicht weiß *wie* Dinge funktionieren, sind sie ja immer so eine Art scharze Magie. :mage:  

Muss gestehen, jetzt wo ich es von innen gesehen habe: Sieht eigentlich ziemlich simpel aus :zany_face:
Zylindrische Schamott-Steine, Heizschlange rein, Regler dran, fertig. :magic_wand:


## Montage

![Kochfeld mit neuer Glasplatte wieder eingebaut](./kochfeld-mit-neuer-platte-eingebaut.avif){ align=right style="max-width: 50%;" loading=lazy }

Anschließend hab ich das Kochfeld wieder in den Ausschnitt gehoben und mit den vier Klammern von unten befestigt.

Bevor ich den Rest wieder zusammengebaut habe, wollte ich noch testen wie es mit der Dichtheit um die Glasplatte aussieht. :eyes:  
Also habe ich an eine Seite nach der anderen Wasser geschüttet und darunter geschaut, ob es direkt irgendwo anfängt zu tropfen oder gar zu laufen. :fountain:  
Ergebnis: :white_check_mark: Test Passed

Zum Schluss noch die Anschlüsse vom Kochfeld wieder an den Ofen ran, diesen reinschieben und die beiden Schrauben reindrehen, fertig. :construction_worker:  
Als Letztes die Sicherungen wieder rein und testen, dass auch noch alle vier Platten funktionieren. :saluting_face:

---

Habt ihr sowas auch schonmal gemacht und was hat euch so Schwierikeiten bereitet?
