FROM python:3.13-alpine AS build_env

RUN apk add git
RUN pip install poetry
RUN poetry self add "poetry-dynamic-versioning[plugin]"

RUN mkdir /data

COPY poetry.lock /data/poetry.lock
COPY pyproject.toml /data/pyproject.toml

RUN poetry config virtualenvs.create false

WORKDIR /data

RUN git init
RUN poetry install

RUN find / -type f -name "*.pyc" -exec sh -c "rm -f {}" \;
RUN find / -type d -name "*__pycache__" | xargs rm -rf
RUN find / -type f -name "*.whl" -exec sh -c "rm -f {}" \;
RUN find /usr/local/lib/python3.13/site-packages -type d -name "*tests" | xargs rm -rf

WORKDIR /usr/libexec/git-core

RUN rm -rf \
    *cache* \
    *checkout* \
    *cherry* \
    *fetch* \
    *i18n* \
    *merge* \
    *pack* \
    *pull* \
    *push* \
    *remote* \
    *submodule* \
    *update* \
    *upload*

FROM python:3.13-alpine

# Git
COPY --from=build_env /usr/libexec/git-core /usr/libexec/git-core
COPY --from=build_env /usr/bin/git /usr/bin/
COPY --from=build_env /usr/lib/libpcre2-8.so.0 /usr/lib/

# Poetry
COPY --from=build_env /usr/local/bin/poetry /usr/local/bin/poetry
COPY --from=build_env /usr/local/bin/mkdocs /usr/local/bin/mkdocs
COPY --from=build_env /usr/local/lib/python3.13 /usr/local/lib/python3.13

RUN mkdir /data
RUN adduser -D mkdocs
RUN chown -R mkdocs:mkdocs /data

COPY --from=build_env /root/.config/pypoetry/config.toml /home/mkdocs/.config/pypoetry/config.toml
RUN chown -R mkdocs:mkdocs /home/mkdocs

USER mkdocs
WORKDIR /data
