FROM python:3.13-alpine

RUN apk add git
RUN pip install poetry
RUN poetry self add "poetry-dynamic-versioning[plugin]"
RUN poetry install
