---
tags:
    - Programming
    - CICD
    - Docker
    - Resources
    - Optimization

title: Mutli-Stage custom Docker Image for CI-Pipeline
description:
    "Refactoring: How to Poetry MkDocs Build Pipeline using a custom multi-stage docker image."

date: 2024-12-31 13:00
update: 2024-12-31 13:00
comments:
   host: mastodontech.de
   username: 9Lukas5
   id: 113747322631395319
image: header.avif
list_entry: true
---

# {{ title }}

![](./header.avif){ data-description=".header-desc" loading=lazy }
<div class="glightbox-desc header-desc">
    GitLab-CI Pipeline entry
</div>

<p class="introduction" markdown>
<span class="date">{{ date }}</span>  
{{ description }}
</p>

---

## TL;DR

- I created a custom Docker image with everything needed for the MkDocs static site to be built, already pre-installed.
- The image is stripped down as much as in somewhat reasonable time was possible and runs as non-root.
- CI-Pipeline was heavily modified to re-build the image only if really a change is possible, but still with feature-branch possibility.
- Site build time is now lower even with Poetry, than it was before with Vanilla-PiP

## Motivation

Static site generators.
Got initially introduced to them in form of MkDocs-Material, back in 2019 during my internship with the Siemens Mobility customer support data analysis division.  
Since then, I've made quite a few MkDocs-Material sites all built quite similar:

- Site configuration
- Versioned in Git
- Hosted on a GitLab instance using their pages feature
- Deployed using docker runners through a CI-Pipeline

A few weeks ago I was invited to a company event, where we had some speakers presenting their efforts to create massively stripped down custom docker images for runner pipelines. :hushed:
This made me think about my own pipelines for my pages builds. :thinking:

I usually use the `python3:alpine` or `python3:slim` images, followed by installing my needed packages and finally _actually_ building the page.  
Over time, the tool to handle my projects changed as well: First it was just Vanilla-PiP with a requirements file, today I use poetry.
The before script looked like this:

=== "Vanilla-PiP"

    ```yaml
    before_script:
      - pip install -Ur requirements.txt
    script:
      - cp CHANGELOG.md docs/CHANGELOG.text
      - sed -i "s#../../compare/#$CI_PROJECT_URL/-/compare/#g" docs/CHANGELOG.text
      - sed -i "s#\([a-f0-9]\{40\}\)#$CI_PROJECT_URL/-/commit/\1#g" docs/CHANGELOG.text
    ```

=== "Poetry"

    ```yaml
    before_script:
      - apk add git git-lfs
      - export PATH="/root/.local/bin:$PATH"
      - pip install poetry
      - poetry self add "poetry-dynamic-versioning[plugin]"
      - poetry install
    script:
      - cp CHANGELOG.md docs/CHANGELOG.text
      - sed -i "s#../../compare/#$CI_PROJECT_URL/-/compare/#g" docs/CHANGELOG.text
      - sed -i "s#\([a-f0-9]\{40\}\)#$CI_PROJECT_URL/-/commit/\1#g" docs/CHANGELOG.text
    ```

While it was _just_ installing the packages in the Vanilla-PiP setup, by switching to Poetry I had to first install poetry in multiple steps before I even could start installing the packages.
And remember, this is done _every time_ a pipeline runs.

![GitLab-CI Pipeline Vanilla-PiP](./ci-pipeline-vanilla-pip.avif){ data-description=".ci-pipeline-vanilla-pip-desc" style="max-width: 49%; height: 6em;" loading=lazy }
![GitLab-CI Pipeline Poetry](./ci-pipeline-poetry.avif){ data-description=".ci-pipeline-poetry-desc" style="max-width: 49%; height: 6em;" loading=lazy }


<div class="glightbox-desc ci-pipeline-vanilla-pip-desc">
    Pull base image, PiP install and site build took 55 seconds.
</div>
<div class="glightbox-desc ci-pipeline-poetry-desc">
    Pull base image, setup Poetry, install packages and site build took 74 seconds.
</div>

While the Vanilla-PiP variant managed to pull the base image, install my dependencies and build the site in 55 seconds, the switch to Poetry raised this to 74 seconds. :face_with_diagonal_mouth:

The idea from the talk I listened to, was to make a custom image that doesn't need to install anything beforehand, but already brings everything you need to run your actual build command.
Additionally, the image should (or has to) be stripped down as much as possible.
The benefits then should be:

- No more install steps before building
- Less services incorporated to running a build pipeline (e.g. no more network calls to PyPi)
- The smaller the image size :arrow_forward: the less to download :arrow_forward: the faster your build

## Building a custom image

### V1 - Basic Image

No more install steps beforehand.
So: Just move the install-steps to the docker image creation, right? :shrug:

~~~dockerfile title="Dockerfile V0"
{% include './v0.Dockerfile' %}
~~~

Not quite :face_with_peeking_eye:  
I'll start with a first _buildable_ image here, as there are still enough obstacles coming:

=== "Diff"

    ~~~diff title="Dockerfile V1"
    {% include './v0-v1.diff' %}
    ~~~

=== "Full file"

    ~~~dockerfile title="Dockerfile V1"
    {% include './v1.Dockerfile' %}
    ~~~

- The python base image starts just at the filesystem root, a working directory is needed
- In order to run the Poetry install, the projects config and lock file are required
- Because of the poetry dynamic versioning plugin it needs Git _and_ poetry has to be run in a Git-Repo. So, I just initialized an empty one :shrug:

### V2 - Poetry Paths

!!! warning "Problem: auto created virtual-env names"
    By default, poetry automatically creates Python virtual environments and the names of these environments will not be identical on each machine.

    ![Terminal output locally](./poetry-env-local.avif){ data-description=".poetry-env-local-desc" style="max-width: 49%;" loading=lazy }
    ![Terminal output CI-pipeline](./poetry-env-ci.avif){ data-description=".poetry-env-ci-desc" style="max-width: 49%;" loading=lazy }

    <div class="glightbox-desc poetry-env-local-desc">
        Locally run a container with the locally build image and list the Poetry environments, the environment created during the docker build stage is called <span>blog-vUdBO1wD-py3.13</span>
    </div>
    <div class="glightbox-desc poetry-env-ci-desc">
        Online run in the CI-pipeline a new environment was created, called <span>blog-60Pf0pOX-py3.13</span>
    </div>

After a little searching, it seems Poetry can't be configured an explicit full path to use for a virtual environment.
The only thing that sounded interesting for a short moment, was the coniguration option `virtualenvs.in-project`[^poetry-docs_virt-env-in-project]  
Only Problem: With that option a local environment would be created in the `/data` folder during the docker build, having all the packages installed, just to get masked the moment you mount anything to that place. :upside_down_face:

But the question is: Why even create an _extra_ virtual environment (which also takes up space)?
It's not like there would be any other users or projects with conflicting dependencies in a special-purpose-built image anyways. :information_desk_person:

!!! success "Use the system environment"
    Just don't ever create any virtual environment, but simply use the system environment directly.
    And look at that: That's possible :thumbsup:
    
There is a configuration option called `virtualenvs.create`[^poetry-docs_virt-env-create], which is set `true` by default.
If this option is set `false`, no pre-existing virtual environment is found in the poetry cache and none is active already, poetry will use the systems' environment. :partying_face:


Ok, so what changed? Let's have a look :eyes:

=== "Diff"

    ~~~diff title="Dockerfile V2"
    {% include './v1-v2.diff' %}
    ~~~

=== "Full file"

    ~~~dockerfile title="Dockerfile V2"
    {% include './v2.Dockerfile' %}
    ~~~

And by the way, having a look at the resulting images shows, that just disabling the use of virtual environments saves short of 20 MB in size:

```text title="Docker image list of blog versions until now"
name    tag         size
----------------------------
blog    v2          272MB
blog    v1          290MB
python  3.13-alpine  44.9MB
```

!!! info "Poetry Dynamic-Versioning and mounting local paths"
    If running a container locally and mounting a path, it could happen poetry commands fail, because the Dynamic-Versioning plugin tries to do Git operations and the ownership of the `.git` folder is not matching the active user-ID of the containers' user.

    For the purpose of using the container as CI-runner image, this shouldn't be a problem.
    For local operation a global git option can work around this:  
    `git config --global --add safe.directory .`

### V3 - Image size

!!! warning "Problem: Image size"
    The target image should be as small as possible.
    Just using the image as it is right after installing packages leaves installtion artifacts, not needed to actually run the site build.

I snooped around in the created image using different commands :detective_tone2:

- `du / -d 1 | sort -n` - to see where in the container the space is used
- `find / -name "*git*"` - find places Git files are located (same for e.g. Poetry and mkdocs)

Following, there's the question of what non-needed build-artifacts are left over in the image? :thinking:  

- `.pyc` - These are created during first runtime, but will be re-created if not existing.  
    No need to keep them in the image. :information_desk_person:
- `.whl` - From the installation there could be download left-overs, which shouldn't be needed for runtime after the packages are installed.  
    No need to keep them cached, as I don't want to do e.g. any re-installs of already downloaded packages.
- `__pycache__` - Checked for `cache` using `find`.
    When quickly checking with `du`, there was quite something in them.
    A quick test showed: building the page worked just fine without them being present :face_with_monocle:  
    So, they're gone as well :wastebasket: :shrug:
- `tests` - I also found folders with the word `test` in the site-packages.
    No idea what they're for, but I assumed/hoped they're somehow unittests, not something needed in production. :eyes:
    Quick test: the site build is working fine without :face_with_cowboy_hat:

Finally, I checked `/usr/libexec/git-core` with `du`.
As I only need git to be able to generate the version string from it, there are certain things my toolset hopefully runs without.
Below I capped the list to the largest 10 elements and from these I would hope nothing is needed for my goal. :face_with_open_eyes_and_hand_over_mouth:

```text title="Summary of elements not needed in the site build runtime image"
/data # find / -type f -name "*.pyc" | xargs du -ch | grep -E ".*\s+total$"
2.5M    total
4.2M    total
4.6M    total
5.0M    total
7.4M    total
/data # find / -type f -name "*.whl" | xargs du -ch | grep -E ".*\s+total$"
33.2M   total
/data # find / -type d -name "*__pycache__" | xargs du -ch | grep -E ".*\s+total$"
23.7M   total
/data # find /usr/local/lib/python3.13/site-packages -type d -name "*tests" | xargs du -ch | grep -E ".*\s+total$"
2.7M    total
/data # du /usr/libexec/git-core/ -ahc | sort -n | tail -n10
12.0K   /usr/libexec/git-core/git-mergetool--lib
12.0K   /usr/libexec/git-core/git-sh-setup
12.0K   /usr/libexec/git-core/git-submodule
16.0K   /usr/libexec/git-core/git-filter-branch
20.0K   /usr/libexec/git-core/mergetools/vimdiff
108.0K  /usr/libexec/git-core/mergetools
588.0K  /usr/libexec/git-core/git-sh-i18n--envsubst
668.0K  /usr/libexec/git-core/git-http-fetch
688.0K  /usr/libexec/git-core/git-remote-http
920.0K  /usr/libexec/git-core/git-http-push
```

Now I got a list of what to remove, but simply adding a `RUN <delete this stuff>` to the Dockerfile won't do the job. :face_with_diagonal_mouth:  
Reason for that is Dockers' layers: The files are part of a previous layer.
Deleting them would just be another step on-top of all previous ones.
This means, in the final image the files wouldn't be there, but the overall size to store and download the image, would still include them.

!!! success "Multi-Stage Image"
    The solution to this: build a multi-stage Docker image.

Now, what's that?  
Docker Multi-Stage Images are images, that have multiple `FROM` statements in their Dockerfile.
Everytime this is done, a new stage is started and basically everything done before is "lost".
This also resets the layering to every build step counted since the last `FROM` statement.

The trick in all of this is, you can grab things from previous stages and copy them over to the current active stage.
That's what I used here, to first install the needed things, remove any non-wanted leftovers and find all needed files/folders to be present in the final stage.  
Let's have a look at the updated Dockerfile:

=== "Diff"

    ~~~diff title="Dockerfile V3"
    {% include './v2-v3.diff' %}
    ~~~

=== "Full file"

    ~~~dockerfile title="Dockerfile V3"
    {% include './v3.Dockerfile' %}
    ~~~

Ok, let's break it down:

- first stage (the build)
    - the first `FROM` statement is extended to have a name, that later can be referenced
    - after the installation process, every installation artifact identified earlier is removed
    - additionally, with quite a few patterns I stripped the various parts of git I think are not needed
- second stage (the runtime)
    - new `FROM` statement on the Alpine Python image as base
    - copy the needed parts over from the build stage
    - re-create the empty workdir
    - set the `WORKDIR` as last statement, for when the image is used to create container from

And the result is this: Over 100 MB less than V2 :partying_face: and short of half of V1 :exploding_head:

```text title="Docker image list of blog versions until now"
name    tag         size
----------------------------
blog    v3          153MB
blog    v2          272MB
blog    v1          290MB
python  3.13-alpine  44.9MB
```


### V4 - Non-Root

!!! warning "Superuser not needed"
    If creating such specific containers already anyway: There's no need to be root for generating a static page. :shrug:

!!! success "Use a non-root user"
    Not much to say: Just don't use root :joy:

=== "Diff"

    ~~~diff title="Dockerfile V4"
    {% include './v3-v4.diff' %}
    ~~~

=== "Full file"

    ~~~dockerfile title="Dockerfile V4"
    {% include './v4.Dockerfile' %}
    ~~~

What changed is just:

- create a new user, I named mine `mkdocs`
- take ownership of the workdir
- copy the Poetry config instead to the root user to the newly created users' home directory and make sure of the ownership
- finally, switch over to the new user

!!! danger "Poetry config"
    I opted (in the end) to copy the Poetry configuration from the build stage down to the runtime, instead of just re-run the config command,
    because Poetry itself is a Python program. :point_up_tone2:  
    This means, executing it can potentially create artifacts again. :grimacing:

    [Trust me, I know :neutral_face:][fedi_post-about-python-caches]

## Result

Time to check the outcome:

![GitLab-CI Pipeline Vanilla-PiP](./ci-pipeline-vanilla-pip.avif){ data-description=".ci-pipeline-vanilla-pip-desc" style="max-width: 33%; height: 6em;" loading=lazy }
![GitLab-CI Pipeline Poetry](./ci-pipeline-poetry.avif){ data-description=".ci-pipeline-poetry-desc" style="max-width: 33%; height: 6em;" loading=lazy }
![GitLab-CI Pipeline Poetry](./ci-pipeline-custom-image.avif){ data-description=".ci-pipeline-custom-image-desc" style="max-width: 33%; height: 6em;" loading=lazy }

<div class="glightbox-desc ci-pipeline-custom-image-desc">
    Time for building with the prepared docker image took 44 seconds.
</div>

Compared to the build time of installing Poetry and then all dependencies before building as direct predecessor, the custom Docker image build has a build-time reduction of roughly 60% :exploding_head:

Overall, from Vanilla-PiP the build time went up by ~34% switching to the simple Poetry approach, but was reduced by 20% with the custom Docker image. :partying_face:

## Refactor Pipeline-Script

But wait, there's more :face_with_cowboy_hat:

Now that the custom images' Dockerfile is defined, it's not done by just changing the jobs' image to use.
It would still have the install-steps included and how to handle possible changes in the dependencies and all of that with branching and release-tags included. :hushed:

The old pipeline had just two simple jobs: one to build the site whenever there was a change in the page sources, plus a deploy-job that only ran on tag-pipelines.  
For the custom image solution I wanted the following:

- the images are tagged by the branch name
- the image only gets re-built if there are changes to the project dependencies or the Dockerfile itself
- for tags the image is re-tagged for preservation
- a feature branch that is not triggering a image rebuild should not built its own image, but use the one from the default-branch

??? example "Full Pipeline script Old vs. New"

    === "Old"

        ~~~yaml
        {% include "./pipeline-old.yaml" %}
        ~~~

    === "New"

        ~~~yaml
        {% include "./pipeline-new.yaml" %}
        ~~~

    === "Diff"

        ~~~diff
        {% include "./pipeline.diff" %}
        ~~~

Let me explain what I did.

### New Docker elements
#### Additional pipeline stage

First, I added an additional stage for the docker jobs.

```diff title="Introduce additional stage" linenums="1"
-8<-- "docs/Verzeichnis/2024/17_multi-stage-build-image-for-ci/pipeline.diff:4:7"
```

#### Global variable for image name/tag

Then, I defined a global variable for the final full image name+tag.
This is build from the repos' image registry, the repos' name and the current branch.
These variables are [all predefined by GitLab][gl_docs_predefined-vars]

```yaml linenums="6"
-8<-- "docs/Verzeichnis/2024/17_multi-stage-build-image-for-ci/pipeline-new.yaml:6:7"
```

#### YAML-Anchor for DinD base

For doing Docker operations _inside_ Docker runners (Docker-in-Docker :point_right_tone2: DinD), there's a base setup of a job I found sometime when I did that first.
This was added as reusable YAML-Anchor:

```yaml linenums="14"
-8<-- "docs/Verzeichnis/2024/17_multi-stage-build-image-for-ci/pipeline-new.yaml:14:23"
```

#### New job: Build the Docker image

Next, I defined the job for building the Docker image using the defined anchor whenever something changed.  
For this, I defined the pipeline script, the Dockerfile and the lock-file of my dependencies as the relevant ones to look out for.

Anyone already knowing GitLab-CI a bit, will probably have noticed that if you create a new branch and push that, it will trigger a new pipeline even if you didn't make a single commit yet.  
This is due to the fact, that branch pipelines change rules compare to the previous commit on that branch, which on first push does not exist.[^gl_docs_ci_ruleschanges]

To prevent a Docker image build each time a feature branch is created if there's no change relevant to re-building it, feature branches had to be configured to compare specifically to the default branch.

As that meant the rules have to be different for the default and feature branches, I extracted the paths to another YAML-Anchor.

```yaml linenums="9"
-8<-- "docs/Verzeichnis/2024/17_multi-stage-build-image-for-ci/pipeline-new.yaml:9:12"
```
```yaml {linenums="25" hl_lines="2 11 15-16"}
-8<-- "docs/Verzeichnis/2024/17_multi-stage-build-image-for-ci/pipeline-new.yaml:25:40"
```

#### New job: Re-Tag Docker image on tags

For the deployment on tags, the docker image should be re-tagged, that I'll be able to grab the exact build environment for that tag again if needed for whatever reason.

So, there's a new job just pulling the default-branches image, re-tagging and pushing it again.  
Conditions are also pretty simple: On tag pipelines only :ok_hand_tone2:

```yaml {linenums="42"}
-8<-- "docs/Verzeichnis/2024/17_multi-stage-build-image-for-ci/pipeline-new.yaml:42:50"
```

### Updated site build
#### Use new image

Now that the image is there, it's time to change the site generation job.
First up, the jobs' image was switched for the global variable built at the start and the nolonger needed `before_script` was retired.

```diff {linenums="59"}
-8<-- "docs/Verzeichnis/2024/17_multi-stage-build-image-for-ci/pipeline.diff:62:76"
```

#### Rules General

Now the most complicated part of this pipeline script follows: The `mkdocs_build` jobs' rules.  
As there will be multiple ones, and all should use the same change paths to look out for, the file list was extracted to a YAML-Anchor again:

```yaml {linenums="52"}
-8<-- "docs/Verzeichnis/2024/17_multi-stage-build-image-for-ci/pipeline-new.yaml:52:57"
```

The `rules` keyword in a GitLab-CI is something quite powerful, if it gets to selecting a job to be run and also to manipulate its behaviour _if_ it is selected. :eyes:  
The rules are evaluted top-to-bottom: The first rule evaluating to `true` will select the job and everything following will not be evaluated anymore.[^gl_docs_ci_rules]

#### Rule 1 - Tags

For this I used the power of the rules to completely replace the jobs `needs` keyword, if the rule matched.[^gl_docs_ci_rulesneeds] :hushed:  
This makes the job dependent on the image-retag job, but just for tag-pipelines.

```yaml {linenums="69"}
-8<-- "docs/Verzeichnis/2024/17_multi-stage-build-image-for-ci/pipeline-new.yaml:69:72"
```

#### Rule 2 - Feature branch + image change

In this case build the site and depend on the docker image build job.

```yaml {linenums="73"}
-8<-- "docs/Verzeichnis/2024/17_multi-stage-build-image-for-ci/pipeline-new.yaml:73:80"
```

#### Rule 3 - Feature branch + site change

As the second rule didn't match, there's no image relevant change.
Therefore, a rebuild of the image is not needed and the already present image for the default branch can be reused.  
The image the job uses is read from the variable `CI_IMAGE_TAG`.
And look at that: You can also change variables conditional to rules.[^gl_docs_ci_rulesvariables]

I first didn't believe this would already be evaluated for the `image` keyword of the job, as it just seemed too good.
But it works :clap_tone2:  
This changes the runner image this job uses if this rule matches :nerd_face:

```yaml {linenums="81"}
-8<-- "docs/Verzeichnis/2024/17_multi-stage-build-image-for-ci/pipeline-new.yaml:81:87"
```

#### Rule 4 - Default branch + image change

It's the same as for feature branches: the job has to depend on the image build.
But as feature branches need a different comparison base as the default branch, it needs its own rule. :information_desk_person:

```yaml {linenums="88"}
-8<-- "docs/Verzeichnis/2024/17_multi-stage-build-image-for-ci/pipeline-new.yaml:88:94"
```

#### Rule 5 - Default branch + site change

Same as for the fourth rule: different comparison base, separate rule.

```yaml {linenums="95"}
-8<-- "docs/Verzeichnis/2024/17_multi-stage-build-image-for-ci/pipeline-new.yaml:95:98"
```

## Conclusion

My setup would have kept working just fine as it was.
Changing it has cost me easily two days of the holidays.  
But I found the idea of resource optimization interesting.

In general, I think we should have a more critical view at how we use the resources again, with the things we program.
Sure they work, but at what cost?  
Thinking e.g. of how AI usage has rocketed, I doubt that the majority of its users is aware of its energy needs. :information_desk_person:

Me as an individual has saved a few seconds on a single repo, that doesn't even run that often anyway, yes.  
But now imagine e.g. how many static pages are out there on GitLab, GitHub, etc?
If everyone optimized there pipelines, this could have a measurable impact on the runner utilization I would assume. :chart_with_downwards_trend:  
Probably this then also raises the storage needs significantly? :chart_with_upwards_trend: :thinking: I don't know :shrug:

What do you think?
Tell me in the comments :envelope_with_arrow:

[^gl_docs_ci_rules]: [GitLab Docs: CI: Rules][gl_docs_ci_rules]
[^gl_docs_ci_ruleschanges]: [GitLab Docs: CI: Rules: Changes][gl_docs_ci_ruleschanges]
[^gl_docs_ci_rulesneeds]: [GitLab Docs: CI: Rules: Needs][gl_docs_ci_rulesneeds]
[^gl_docs_ci_rulesvariables]: [GitLab Docs: CI: Rules: Variables][gl_docs_ci_rulesvariables]
[^poetry-docs_virt-env-create]: [Poetry Docs: Create Virtual-Environments][poetry-docs_virt-env-create]
[^poetry-docs_virt-env-in-project]: [Poetry Docs: Virtual-Environments in-project][poetry-docs_virt-env-in-project]

[fedi_post-about-python-caches]: https://mastodontech.de/@9Lukas5/113744207009599972
[gl_docs_ci_rules]: https://docs.gitlab.com/ee/ci/yaml/#rules
[gl_docs_ci_ruleschanges]: https://docs.gitlab.com/ee/ci/yaml/#ruleschanges
[gl_docs_ci_rulesneeds]: https://docs.gitlab.com/ee/ci/yaml/#rulesneeds
[gl_docs_ci_rulesvariables]: https://docs.gitlab.com/ee/ci/yaml/#rulesvariables
[gl_docs_predefined-vars]: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
[poetry-docs_virt-env-create]: https://python-poetry.org/docs/configuration/#virtualenvscreate
[poetry-docs_virt-env-in-project]: https://python-poetry.org/docs/configuration/#virtualenvsin-project
