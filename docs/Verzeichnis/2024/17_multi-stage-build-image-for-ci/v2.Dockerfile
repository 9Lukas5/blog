FROM python:3.13-alpine

RUN apk add git
RUN pip install poetry
RUN poetry self add "poetry-dynamic-versioning[plugin]"

RUN mkdir /data

COPY poetry.lock /data/poetry.lock
COPY pyproject.toml /data/pyproject.toml

RUN poetry config virtualenvs.create false

WORKDIR /data

RUN git init
RUN poetry install
