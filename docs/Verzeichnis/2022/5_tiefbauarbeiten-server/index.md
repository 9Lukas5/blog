---
tags:
    - Linux
    - Ubuntu
    - Server
    - ButterFS
    - BTRFS
    - LUKS
    - Subvolumes
    - Snapshots
    - Filesystems

title: Tiefbauarbeiten am Linux Server
description:
    Ich wollte <i>eigentlich</i> nur einen Snapshot von meinem ButterFS Root-Subvolume machen, bevor ich das Release-Upgrade starte.
    Blöd nur, dass das System gar nicht in einem Subvolume installiert war. :see_no_evil:<br>
    Dieser Beitrag beschreibt den Prozess das direkt in der ButterFS Root-Partition liegende System in ein Subvolume zu verschieben.
    Prinzipiell kann aber jedweder Umzug von A nach B eines bestehenden Linux-Systems so vollzogen werden.
image: ubuntuserver-live-ssh-help.avif
date: 2022-11-07 00:00
update: 2022-11-07 00:00
list_entry: true
---

# {{ title }}

<p class="introduction" markdown>
<span class="date">{{ date }}</span>  
{{ description }}
</p>

---

## Beschreibung des Systems

Der Server ist ein Ubuntu-Server 20.04 der auf einer Sata-SSD liegt.
Ursprünglich muss ich den mit 18.04 installiert haben und 20.04 war bereits ein Upgrade.
In dem Server ist außer den Partitionen für EFI und Grub alles andere vollverschlüsselt.

Bei der Installation bin ich damals (teilweise) einer Antwort bei AskUbuntu gefolgt[^askubuntu-custom-encrypt-install].
Darin wurde mit LVM gearbeitet, um innerhalb der verschlüsselten Partition getrennte Partitionen erstellen zu können.
In meinem Fall habe ich eine 512M EFI-Partition, 1G-Grub Partition und die restliche Platte als LUKS Volume formatiert.
Im LUKS Volume habe ich dann, wie in der Anleitung beschrieben, mit LVM gearbeitet, wobei ich aber nicht mehrere, sondern einfach eine große Partition angelegt habe.
Darauf landete dann das Wurzelverzeichnis meiner Installation mit ButterFS formatiert.

Die Systemplatte sowie alle Datenplatten haben in ihren LUKS-Headern eine individuelle Schlüsseldatei in einem der Keyslots.
Die Datei für die Systemplatte ist auf einem unverschlüsselten USB-Stick der immer vorne im Server steckt, die aller anderen Platten liegen auf der verschlüsselten Systemplatte.
Im Fall der Fälle müsste also nur unauffällig der Stick abgezogen werden und dann sollte erstmal keiner etwas damit anfangen können. :smirk:



## Bisheriger Zustand und Ziel

Wie oben beschrieben: bisher 512M EFI, 1G Grub, der Rest ein LUKS Volume und darin ein ButterFS als Wurzelverzeichnis auf einer logischen Partition.

Hier ist an den Ausgaben von `lsblk` und `blkid` das LVM zu sehen.
Mit `btrfs subvolume list <Pfad>` sind dann nur eine ganze Reihe Subvolumes von Docker zu sehen die alle im Top-Level 5 angelegt sind.
Das heißt wiederum, dass das System direkt das ButterFS Top-Level als Wurzelverzeichnis hat, wodurch widerum kein Snapshot davon möglich ist.

Ziel war, dass das Wurzelverzeichnis in einem dedizierten Subvolume wohnt, womit zum Beispiel das Erstellen von Snapshots möglich wird.
Das allein wäre sogar innerhalb der bestehenden ButterFS Partition möglich gewesen, ohne dass überhaupt ein separater Ort zum Zwischenspeichern benötigt worden wäre. :point_up_tone2:
Ich wollte aber die Gelegenheit direkt nutzen, um das sowieso überflüssige, und von mir sonst nirgends verwendete, LVM Setup abzuschaffen. :information_desk_person:

=== "lsblk"

    ```
    NAME                                        SIZE  TYPE  MOUNTPOINT
    ...
    sdd                                       223,6G  disk
    ├─sdd1                                      512M  part  /boot/efi
    ├─sdd2                                      929M  part  /boot
    ├─sdd3                                    222,1G  part
    │ └─system-crypt                          222,1G  crypt
    |   └─ vg_Server1_system-lv_server1_root  222,1G  lvm   /
    ...
    ```

=== "blkid"

    ```
    ...
    /dev/sdd1:                                      ... TYPE="vfat"        ...
    /dev/sdd2:                                      ... TYPE="ext4"        ...
    /dev/sdd3:                                      ... TYPE="crypto_LUKS" ...
    /dev/mapper/system-crypt:                       ... TYPE="LVM2_member" ...
    /dev/mapper/vg_Server1_system-lv-server1_root:  ... TYPE="btrfs"       ...
    ...
    ```

=== "ButterFS Subvolumes"

    ```
    ID 10681 gen 2082934 top level 5 path var/lib/portables
    ID 10682 gen 2082935 top level 5 path var/lib/machines
    ID 10731 gen 2087302 top level 5 path var/lib/docker/btrfs/subvolumes/<uuid>
    ID 10732 gen 2608058 top level 5 path var/lib/docker/btrfs/subvolumes/<uuid>
    ID 10867 gen 2541102 top level 5 path var/lib/docker/btrfs/subvolumes/<uuid>
    ID 10899 gen 2243967 top level 5 path var/lib/docker/btrfs/subvolumes/<uuid>
    ID 10900 gen 2177551 top level 5 path var/lib/docker/btrfs/subvolumes/<uuid>
    ...
    ```

    !!! tip "Bereits existierende Subvolumes"
        In einer Textdatei ablegen, sollte später wieder genauso rekonstruiert werden

## Vorbereitungen

Was man für so ein Vorhaben haben sollte:

1. Ausreichend Platz auf anderer Festplatte

    Bei mir waren das knapp 130G.
    Da der Server auch als Samba-Server dient, war Platz auf anderen Festplatten kein Problem.

1. Die benötigten Befehle parat haben.

    Beim Kopieren ganzer Betriebssysteme ist natürlich wichtig, dass alles am Ende so exakt wie möglich am Quell-Zustand ist.
    Also Dinge wie Soft-/Hardlinks, Dateirechte, etc.  
    Ich hab das schon hin und wieder bei Bastelprojekten gemacht und dafür rsync verwendet.
    Nochmal schnell online gesucht und eine Seite bei Baeldung dazu gefunden von der ich die Parameterliste übernommen habe[^baeldung-copy-over-whole-linux-system].

1. Ein Live-System für die Umsetzung.

    Man kann (und sollte es auch nicht probieren) ein laufendes System im Betrieb schlecht irgendwo anders hin verfrachten.
    Daher braucht es ein anderes System von dem aus man auf die Festplatte(n) zugreifen kann.
    In den meisten Fällen dürfte daher ein Live-System die beste Wahl sein.  
    Ich habe sowieso einen bootfähigen Stick mit mehreren ISOs verschiedener Distributionen auf dem Tisch liegen (anderes Projekt dessen Ergebnis echt praktisch ist :ok_hand_tone2:) der dafür zum Einsatz kam.

1. Zeitpunkt, an dem keiner da ist, der das System benutzen will :grimacing:

## Umsetzung

### Live-Umgebung

Erstmal Zielsystem aus und vom Live-System booten.
In meinem Fall habe ich das aktuelle Ubuntu-Server 22.04 ISO genommen, da ist direkt ein SSH-Server aktiv.
Es gab zwei Gründe, warum ich das nicht direkt an der Maschine machen wollte.
Zum einen steht das Rack im Treppenhaus: da ist es zurzeit kalt, es gibt keinen Stuhl davor, etc.
Zum anderen muss man ja unter Umständen das ein oder andere hin und her kopieren, was bestimmt auch auf einem keyboard-only Terminal geht, aber ich weiß nicht wie und hab auch nicht das Bedürfnis es zu wissen.

Ich hatte mich schon darauf eingestellt, dass ich meinen Schreibtisch dafür temporär ins DMZ aufpatchen muss, da der Server eigentlich nur darüber erreichbar ist.
Allerdings hat die Maschine drei Netzwerkkarten, wovon eine am Switch in einem Multi-Port steckt der als Standard-VLAN das Heimnetz hat (der gehört der Firewall-VM). :face_with_spiral_eyes:
Die Live-Umgebung hat natürlich einfach alle Schnittstellen angesprochen, somit war sie auch im Heimnetz und hat von den beiden verbliebenen WLAN-APs auch eine Adresse zugewiesen bekommen. :hugging_face:

<figure markdown>
  ![Hilfestellung zum SSH-Zugriff auf das Livesystem mit Benutzername, IP-Adresse und generiertem Passwort](./ubuntuserver-live-ssh-help.avif){ loading=lazy }
  <figcaption>SSH-Zugriffsdetails des Livesystems</figcaption>
</figure>

Im Hilfe-Menü rechts oben konnte ich die obige Info aufrufen, wie ich mich von einer anderen Maschine im Netz anmelden kann.
Bisschen nervig, dass es ein generiertes Passwort ist, aber letztlich wahrscheinlich gar nicht schlecht.
Ist auch eigentlich ganz gut verschmerzbar, denn es ist rein alphanumerisch und nicht etwa mit irgendwelchen ganz wilden Sonderzeichen gespickt.

Als Mountpunkte habe ich die folgenden drei Ordner angelegt:

| Mountpunkt      | Nutzung                                                |
| :-------------- | :----------------------------------------------------- |
| `/source`       | Bestandssystem das gesichert werden sollte             |
| `/intermediate` | Ort zum Zwischenspeichern                              |
| `/target`       | Nach Neustrukturierung der Systemplatte als neues Ziel |

### Zwischenspeicher vorbereiten

Mein gewählter Zwischenspeicher ist keine freie Festplatte/Partition, die ich einfach kurz formatieren kann, sondern ist mit ACLs gespickt.
Für eine akkurate Sicherung der Rechte und ein intakt lassen des Rechtemanagements auf dem Samba-Share eher ungünstig.

Deshalb habe ich mir ein neues Dateisystem in einer Datei angelegt:

=== "Schrittfolge"

    - Leere Datei anlegen auf Zielgerät
    - _Nur bei CoW Dateisystem:_ Copy-on-Write ausschalten
    - Datei auf die benötigte Größe bringen
    - Dateisystem IN dieser Datei anlegen
    - Zwischenspeicher einhängen

=== "Befehle"

    ```sh
    touch /samba/intermediate
    chattr +C /samba/intermediate
    dd if=/dev/zero of=/samba/intermediate bs=1G count=130 status=progress
    mkfs.btrfs /samba/intermediate
    mount /samba/intermediate /intermediate
    ```

=== "Screehnshot der Ausgabe"

    !!! info "Ups :shrug:"
        Ich hab hier aus versehen 230 in den count geschrieben, was der Gesamtgröße der Partition entspricht, statt nur der tatsächlich belegten ca 130G was ja gereicht hätte.

    <figure markdown>
      ![Terminal Ausgabe Schritte für Erstellung Datei-Dateisystem](./create-filesystem-in-file.avif){ loading=lazy }{ loading=lazy }
    </figure>

### Daten Zwischenspeichern

Wie oben erwähnt, die Parameter-Zusammenstellung habe ich aus dem Baeldung-Eintrag [^baeldung-copy-over-whole-linux-system] übernommen:

```sh
rsync -axHAWXS --numeric-ids --info=progress2 /source/ /intermediate/
```

!!! danger "Keinen Slash vergessen"

    Der Abschließende Slash am `/source/` ist wichtig.
    Ohne wird nicht der Inhalt von `/source`, sondern das Elternverzeichnis übertragen.

??? info "Einzelne Bedeutung der Rsync-Parameter"

    | Parameter        | Beschreibung                                                                                                                                                                                                                      |
    | :--------------- | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
    | `a`              | Archiv-Modus: arbeitet rekursiv, behält Symlinks, Rechte, Eigentümer, Gruppen, Änderungszeit, Geräte- und Spezielle Dateien                                                                                                       |
    | `x`              | Verhindert Durchgreifen auf andere Dateisysteme                                                                                                                                                                                   |
    | `H`              | Behält Hardlinks                                                                                                                                                                                                                  |
    | `A`              | Behält ACLs                                                                                                                                                                                                                       |
    | `W`              | Schaltet den Delta-Transfer Algorithmus aus: Geht schneller ohne, wenn Quelle und Ziel auf demselben System sind                                                                                                                  |
    | `X`              | Setzt die selben erweiterteten Attribute an der Zieldatei wie bei der Quelldatei                                                                                                                                                  |
    | `S`              | Versucht 'Sparse'-Dateien effizient zu handhaben, damit diese weniger Platz brauchen                                                                                                                                              |
    | `numeric-ids`    | Nutzt numerische IDs statt zu versuchen diese zu mappen. Insbesondere für Backups von Gefängis-Systemen (jailed-systems) wichtig, da die IDs da nicht immer stimmen, da diese vom Host-System aus betrachtet eigene ID-Maps haben |
    | `info=progress2` | Gibt Transfer-Statistik für den gesamten Transfer, statt einzelne Dateien aus                                                                                                                                                     |

!!! tip "Tipp"
    Nach dem erfolgreichen Sichern müssen der Zwischenspeicher und die Partition in dem dieser lebt nur noch gelesen werden.
    Um hier versehentliches Ändern von irgendwas zu verhindern, ist jetzt ein guter Zeitpunkt diese nur lesend neu einzuhängen.

### 1. Hö :face_with_monocle: (Zu wenig Daten)

Am Ende des Transfers hat rsync um die 112GB übertragen.
Erwartet hatte ich aber ~130GB. :eyes:

<figure markdown>
  ![Terminal Ausgabe des rsync Aufrufes mit ca 112GB die übertragen wurden, bei ø 73 MB/s in guten 24 Minuten](./rsync-stay-on-fs.avif){ loading=lazy }
  <figcaption>Rsync Ausgabe mit nur 112GB erfolgreich beendet</figcaption>
</figure>

Der Groschen ist mir beim nochmaligen Anschauen der eingesetzten Parameter aber sofort gefallen, und zwar beim Parameter `x`.
Wer erinnert sich noch an die Auflistung der ButterFS Subvolumes die im Ausgangszustand existierten?  
Docker nutzt den Subvolume Mechanismus, wenn er erkennt, dass er auf einem ButterFS läuft.
Subvolumes werden wie ein anderes Dateisystem gesehen, daher wurden durch das `x` die ganzen Docker-Container ausgelassen.

Den Rsync-Befehl nochmal ohne die Dateisystembegrenzung aufrufen hat die noch fehlenden Teile dann auch noch erfasst.

### Neue Partitionierung

Erster Versuch war einfach direkt mit `mkfs.btrfs` nach dem offenen LUKS Volume zu werfen.
So einfach wollten sich die LVM Komponenten aber nicht geschlagen geben.
Also erstmal damit auseinandergesetzt. :rolling_eyes:  
Jemand der damit regelmäßig umgeht, wird jetzt mit den Schultern zucken.
Würd ich ja auch. :shrug:
Da ich das aber an der einen Maschine einmal eingerichtet hab und seitdem nie wieder anfassen musste... :information_desk_person:  
Hab das Ding irgendwie nicht inaktiv bekommen, keine Ahnung. :thinking:
Aber der Befehl fürs Löschen hat wenigstens geklappt :ok_hand_tone2:

So, jetzt aber ein neues ButterFS diesmal ohne LVM oder sonst was direkt auf das gesamte LUKS Volume:

```sh
mkfs.btrfs -f -L system /dev/mapper/system-crypt
```

Der Force-Flag war notwendig, da am Ziel ja bereits _etwas_ war.
Daher würde ohne das Flag nur eine Warnmeldung diesbezüglich ergehen, aber sonst nichts weiter passieren.

Danach habe ich das neue Dateisystem nach `/target` eingehängt und dort einige Subvolumes angelegt mit `btrfs subvolume create /target/<subvol-name>`:

| Name          | Zweck                                                                                                              |
| :------------ | :----------------------------------------------------------------------------------------------------------------- |
| `root`        | Wurzelverzeichnis                                                                                                  |
| `home`        | `/home`                                                                                                            |
| `docker`      | Für den `/var/lib/docker/btrfs/subvolumes` gedacht                                                                 |
| `libvirt`     | Der Ordner der Festplattenabbilder meiner VMs                                                                      |
| `server-data` | Der Ordner in dem ich von meinen Docker-Containern die Volumes hin mounte, damit die Nutzdaten auf dem Host liegen |

Danach wieder ausgehängt und diesmal das Subvolume `root` dorthin eingehängt:

=== "Befehl"

    ```sh
    mount -o subvol=root,space_cache=v2,compress-force=zstd:1,noatime
    ```

=== "Mount-Optionen Erläuterung"

    | Option                  | Erklärung                                                                                                                                                                                                                                                                                                                   |
    | :---------------------- | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
    | `subvol=<name>`         | Gibt den Namen des einzuhängenden Subvolumes an. Ohne wird das Default-Subvolume eingehängt, was nicht zwingend das ButterFS Top-Level sein muss.                                                                                                                                                                           |
    | `space_cache=v2`        | ButterFS als CoW Dateisystem hält einen Cache vor, wo freier Platz ist. Ohne wäre die Schreibperformance ziemlich schlecht. Es gibt hier die originle Version und eine neuere die auch "Free Space Tree" genannt wird, was ein B-tree ist.<br>Die neuere Variante wird standardmäßig ab den ButterFS-Progs 5.15 eingesetzt.<br>Das ist eine dauerhafte Änderung und muss bei zukünftigem Einhängen nicht mehr extra angegeben werden.[^ButterFS-space-cache-v2] |
    | `compress-force=zstd:1` | zstd-Kompression auf unterster Stufe. Das Erzwingen schaltet einen internen Erkennungsmodus, ob sich eine Datei zu komprimieren lohnt, aus und komprimiert einfach alles.                                                                                                                                                    |
    | `noatime`               | Es werden keine Zugriffszeiten an Dateien und Ordnern vermerkt.                                                                                                                                                                                                                                                             |

Jetzt musste ich nur noch Ordner für die anderen Subvolumes anlegen und diese dort einhängen.
Danach konnte ich mit demselben Rsync-Befehl wie zuvor von `/intermediate` nach `/target` alles zurückspielen.

!!! danger "Bereits existierende ButterFS Subvolumes im Altsystem"

    Beim kopieren vom Altsystem an den Sicherungsort ohne die Beachtung der Dateisystemgrenzen ist die Unterteilung in verschiedene Subvolumes verloren gegangen.

    Das heißt beim Zurückkopieren wären die ganzen einzelnen Subvolumes die vom Docker angelegt wurden keine eigenständigen Subvolumes mehr.  
    Um dem entgegen zu wirken habe ich innerhalb des angelegten Subvolumes `docker` dieselbe Struktur der Subvolumes wieder angelegt, die Docker zuvor gemacht hatte.

## Nacharbeiten

Durch diese Änderungen im Partitionslayout ergaben sich natürlich andere Einhängepunkte und UUIDs die im Anschluss richtig gesetzt werden mussten.
Zum einen in der `/etc/fstab` und, da ich ein verschlüsseltes System umgezogen habe, auch in der `/etc/crypttab`.

### Fstab

Im Ausgangszustand gab es einfach nur das Wurzelverzeichnis mit den Standardoptionen.
Mit dem Umbau habe ich die Mount-Parameter gleich etwas angepasst:
Keine Zugriffszeiten schreiben, Kompression und natürlich das Subvolume welches eingehängt werden soll.

Die neue Fstab hat ganze fünf Einträge die alle aus derselben ButterFS Partition kommen.
Damit liegen die Nutzerverzeichnisse, sowie die Nutzdaten der VMs und Docker-Container auf separaten Subvolumes.
Damit muss bei schiefgegangenen Veränderungen am Host-System auch wirklich nur das wieder zurückgespult werden.

=== "alt"

    ```sh
    /dev/disk/by-uuid/<uuid>    /                          btrfs   defaults                                           0 1

    /dev/disk/by-uuid/<uuid>    /boot                      ext4    noatime                                            0 2
    /dev/disk/by-uuid/<uuid>    /boot/efi                  vfat    defaults                                           0 0

    # Samba share
    /dev/disk/by-uuid/<uuid>    /samba                     btrfs   noatime,nofail,subvol=@samba,compress-force=zstd:1 0 2

    # Backup disk
    /dev/disk/by-uuid/<uuid>    /backup                    btrfs   noatime,nofail,compress-force=zstd:1               0 2
    ```

=== "neu"

    ```sh
    /dev/disk/by-uuid/<uuid>    /                               btrfs   noatime,discard,subvol=root,compress-force=zstd:1          0 1
    /dev/disk/by-uuid/<uuid>    /home                           btrfs   noatime,discard,subvol=home,compress-force=zstd:1          0 1
    /dev/disk/by-uuid/<uuid>    /var/lib/docker/btrf/subvolumes btrfs   noatime,discard,subvol=docker,compress-force=zstd:1        0 1
    /dev/disk/by-uuid/<uuid>    /var/lib/libvirt/images         btrfs   noatime,discard,subvol=libvirt,compress-force=zstd:1       0 1
    /dev/disk/by-uuid/<uuid>    /srv                            btrfs   noatime,discard,subvol=server-data,compress-force=zstd:1   0 1

    /dev/disk/by-uuid/<uuid>    /boot                           ext4    noatime                                                    0 2
    /dev/disk/by-uuid/<uuid>    /boot/efi                       vfat    defaults                                                   0 0

    # Samba share
    /dev/disk/by-uuid/<uuid>    /samba                          btrfs   noatime,discard,nofail,subvol=@samba,compress-force=zstd:1 0 2

    # Backup disk
    /dev/disk/by-uuid/<uuid>    /backup                         btrfs   noatime,discard,nofail,compress-force=zstd:1               0 2
    ```

### Crypttab

In der Crypttab mussten primär die Einträge der Daten und Backup Platten geändert werden, da deren Schlüsseldateien ja im Wurzelverzeichnis abgelegt sind.
Zusätzlich habe ich aber auch den Mapping-Namen für die Systemplatte geändert der zuvor einfach `dm-0` war.

```sh
# target        source          keyfile                                                     options
system-crypt    UUID=<uuid>     /dev/disk/by-uuid/<usb-drive-uuid>:/rootfskey               luks,keyscript=/usr/local/sbin/decrypt.sh

# data
Data0Crypt      UUID=<uuid>     /dev/disk/by-uuid/<rootfs-uuid>:/root/.cryptkeys/data0      luks,initramfs,keyscript=passdev
Data1Crypt      UUID=<uuid>     /dev/disk/by-uuid/<rootfs-uuid>:/root/.cryptkeys/data1      luks,initramfs,keyscript=passdev
Data2Crypt      UUID=<uuid>     /dev/disk/by-uuid/<rootfs-uuid>:/root/.cryptkeys/data2      luks,initramfs,keyscript=passdev

# backup
Backup0Crypt    UUID=<uuid>     /dev/disk/by-uuid/<rootfs-uuid>:/root/.cryptkeys/backup0    luks,initramfs,keyscript=passdev
```

### Bootkonfiguration

Nach dem Anpassen der Fstab und Crypttab mussten noch die fürs Booten relevanten Dateien neu erzeugt werden, damit die gemachten Änderungen dort auch reflektiert werden.
Um diese zu neu zu generieren ging es einmal ins Chroot-Gefängnis :nerd_face:

```sh
mount -o bind /dev/ /target/dev/
mount -o bind /proc/ /target/proc/
mount -o bind /sys/ /target/sys/
mount -o bind /run/ /target/run/
chroot /target bash
```

Danach musste ich erstmal alle Crypto-Disks aufmachen, da sonst lauter Warnmeldungen über die nicht vorhandenen Volumes im Mapper, die aber in der Crypttab verzeichnet sind, gekommen wären.
Bin nicht sicher ob es auch ohne funktionieren würde, habs nicht ausprobiert. :thinking:

Nachdem alle LUKS-Volumes offen waren, konnte ich mit einem einfachen `mount -a` alles wie definiert einhängen und dadurch auch gleich prüfen, ob alles richtig ist.

Um die Bootdateien zu erneuern zum Schluss noch:

```sh
update-initramfs -k all -c
update-grub
```

Damit _sollte_ jetzt alles gemacht gewesen sein, damit das System wieder startet.

## Bugfixing

Konnte ja nicht ganz ohne Fehler laufen, oder?
Wäre ja wie ein rießen Code-Refactoring das ohne den kleinsten Compiler-Fehler einfach läuft :zany_face:

### 2. Hö :face_with_monocle: (LUKS)

Am Bootloader bin ich zwar auf Anhieb vorbeigekommen, aber nach dem erfolgreichen Entschlüsseln der Systemplatte hats dann einfach aufgehört. :thinking:

Also nochmal zurück ins Live-System und einen Blick in die Crypttab geworfen.
Ich habe zwar die UUID in den Pfaden zu den Schlüsseldateien angepasst, aber dahinter hat sich ja auch noch etwas geändert.
In dieser Situation betrachtet blickt man ohne Angabe eines Subvolumes auf die Partition.
Damit muss der Pfad vom Standard-Subvolume ausgehen, was das ButterFS Top-Level der Partition ist.
Da mein Wurzelverzeichnis jetzt aber im Subvolume `root` liegt, muss der Pfad das natürlich auch enthalten.

=== "Crypttab wie oben"

    ```sh
    # target        source          keyfile                                                     options
    system-crypt    UUID=<uuid>     /dev/disk/by-uuid/<usb-drive-uuid>:/rootfskey               luks,keyscript=/usr/local/sbin/decrypt.sh

    # data
    Data0Crypt      UUID=<uuid>     /dev/disk/by-uuid/<rootfs-uuid>:/root/.cryptkeys/data0      luks,initramfs,keyscript=passdev
    Data1Crypt      UUID=<uuid>     /dev/disk/by-uuid/<rootfs-uuid>:/root/.cryptkeys/data1      luks,initramfs,keyscript=passdev
    Data2Crypt      UUID=<uuid>     /dev/disk/by-uuid/<rootfs-uuid>:/root/.cryptkeys/data2      luks,initramfs,keyscript=passdev

    # backup
    Backup0Crypt    UUID=<uuid>     /dev/disk/by-uuid/<rootfs-uuid>:/root/.cryptkeys/backup0    luks,initramfs,keyscript=passdev
    ```

=== "Crypttab korrigiert"

    ```sh
    # target        source          keyfile                                                     options
    system-crypt    UUID=<uuid>     /dev/disk/by-uuid/<usb-drive-uuid>:/rootfskey               luks,keyscript=/usr/local/sbin/decrypt.sh

    # data
    Data0Crypt      UUID=<uuid>     /dev/disk/by-uuid/<rootfs-uuid>:/root/root/.cryptkeys/data0      luks,initramfs,keyscript=passdev
    Data1Crypt      UUID=<uuid>     /dev/disk/by-uuid/<rootfs-uuid>:/root/root/.cryptkeys/data1      luks,initramfs,keyscript=passdev
    Data2Crypt      UUID=<uuid>     /dev/disk/by-uuid/<rootfs-uuid>:/root/root/.cryptkeys/data2      luks,initramfs,keyscript=passdev

    # backup
    Backup0Crypt    UUID=<uuid>     /dev/disk/by-uuid/<rootfs-uuid>:/root/root/.cryptkeys/backup0    luks,initramfs,keyscript=passdev
    ```

Nach einem weiteren `update-initramfs -k all -c` wieder Neugestartet und siehe da, alle LUKS-Volumes wurden entschlüsselt und das System ist anstandslos wieder angefahren.

### 3. Hö :face_with_monocle: (Docker)

Die VMs sind direkt wieder online gewesen: Ohne wäre ich aus dem LAN gar nicht erst ins DMZ gekommen, da eine der VMs ja der Router ist.
Ein `docker ps -a` Aufruf hat dann aber offenbart, dass die Docker-Container allesamt offline geblieben sind und sich auch nicht starten ließen, denn: Ihre Volumes fehlten :face_with_spiral_eyes:

Also mal schnell mit einem List-Befehl in `/var/lib/docker/btrfs/subvolumes` geblickt und: leer :dizzy_face:  
Daraufhin das Subvolume das dort eingehängt gehört in `/mnt` eingehängt und: Alles da :neutral_face:  
Manueller Aufruf von `mount /var/lib/docker/btrfs/subvolumes`: Ist an besagter Stelle bereits eingehängt :clown_face:

Da mir `lsblk` für das `system-crypt` volume allerdings nicht alle Einhängepunkte, sondern nur einen anzeigt (warum auch immer? :shrug:), mal einen Blick in die `/proc/mounts` geworfen:

```sh
...
/dev/mapper/system-crypt /                                  btrfs ...subvol=/root 0 0
/dev/mapper/system-crypt /var/lib/docker/btrfs/subvolumes   btrfs ...subvol=/docker 0 0
/dev/mapper/system-crypt /home                              btrfs ...subvol=/home 0 0
/dev/mapper/system-crypt /var/lib/libvirt/images            btrfs ...subvol=/libvirt 0 0
/dev/mapper/system-crypt /var/lib/docker/btrfs              btrfs ...subvol=/root 0 0
/dev/mapper/system-crypt /var/lib/docker/btrfs/subvolumes   btrfs ...subvol=/root 0 0
/dev/mapper/system-crypt /srv                               btrfs ...subvol=/server-data 0 0
...
```

Stellte sich raus: Irgendwas hat das Subvolume des Wurzelverzeichnises an zwei Stellen im Docker-Pfad eingehängt.
Und zwar nachdem das von mir in der Fstab definierte Subvolume bereits eingehängt war, wodurch dieses überlagert wurde.
Komm da mal einer drauf :zipper_mouth_face:

Eine schnelle Recherche hat etwas Systemd Magie offenbart: `var-lib-docker-btrfs.mount` und `var-lib-docker-btrfs-subvolumes.mount`.
So ganz durchblickt hab ich das nicht, die scheinen aber irgendwie automtisch zu einem bestimmten Zeitpunkt erstellt zu werden.
Meine pragmatische Lösung: Ich pack nicht nur den Sub-Pfad `/var/lib/docker/btrfs/subvolumes` auf das erstellte ButterFS Subvolume, sondern bereits `/var/lib/docker` :shrug:

=== "Schrittfolge"

    - Den Docker-Dienst anhalten
    - Alles in und unterhalb von `/var/lib/docker` aushängen
    - Mein erstelles Subvolume für Docker unter `/mnt` einhängen
    - Den Sub-Pfad `btrfs/subvolumes` in `/mnt` anlegen
    - Alles andere unter `/mnt` nach `/mnt/btrfs/subvolumes` verschieben
    - `/mnt` aushängen
    - `/var/lib/docker` umbenennen und dafür einen neuen Ordner als Einhängepunkt anlegen
    - Fstab nochmal anpassen, dass der Mount-Pfad für das Docker-Volume `/var/lib/docker` ist
    - `mount -a` aufrufen
    - Prüfen, dass alles da ist wo es sein soll
    - Docker-Dienst starten

=== "Befehle"

    ```sh
    systemctl stop docker.{service,socket}

    # zweimal, da mehrere überlagernde Mounts
    umount /var/lib/docker/btrfs/subvolumes
    umount /var/lib/docker/btrfs/subvolumes
    umount /var/lib/docker/btrfs

    mount -o subvol=docker /dev/mapper/system-crypt /mnt
    mkdir -p /mnt/btrfs/subvolumes
    mv /mnt/* /mnt/btrfs/subvolumes
    umount /mnt
    mv /var/lib/docker /var/lib/docker-old
    mkdir /var/lib/docker

    # hier die /etc/fstab anpassen
    vi /etc/fstab

    mount -a
    ls -hal /var/lib/docker/btrfs/subvolumes

    systemctl start docker.service
    ```

Danach sind die Docker-Container ohne weitere Probleme wieder angelaufen und alles hat funktioniert wie es sollte.
Nach einem weiteren Neustart des Systems, zur Kontrolle, ob jetzt auch aus einem Kaltstart alles richtig funktioniert, war ich dann endlich fertig.
Mittlerweile war es bereits halb zwei Mittags, Zeit für :sandwich: also :relieved:

Der `var-lib-docker-btrfs-subvolumes.mount` hat nach dem Neustart übrigens nicht mehr existiert, der `var-lib-docker-btrfs.mount` dagegen schon und ist auch in der `/proc/mounts` immer noch zu sehen, aber scheint keine Probleme zu machen :thinking: :shrug:

## ButterFS Vorher/Nachher

Das `top level 5` ist das oberste Level in einem ButterFS Dateisystem.
Im Ausgangszustand war dieses direkt als Wurzelverzeichnis eingehängt und die vom Docker erstellten Subvolumes waren deshalb auch alle im `top level 5`.

Nach der Umstrukturierung ist stattdessen ein dediziertes Subvolume als Wurzelverzeichnis eingehängt, sowie ein auf derselben Ebene angelegtes Subvolume für das Docker-Verzeichnis.
Die Subvolumes die vom Docker angelegt wurden habe ich dagegen unterhalb dieses Subvolumes wieder angelegt.

Damit sind jetzt die von mir manuell angelegten Subvolumes alle unabängig voneinander auf dem `top level 5`, während die ursprünglich vom Docker angelegten im `top level 258` sind, was der ID des Subvolumes `docker` entspricht.

=== "Vorher"

    ```
    ID 10681 gen 2082934 top level 5 path var/lib/portables
    ID 10682 gen 2082935 top level 5 path var/lib/machines
    ID 10731 gen 2087302 top level 5 path var/lib/docker/btrfs/subvolumes/<uuid>
    ID 10732 gen 2608058 top level 5 path var/lib/docker/btrfs/subvolumes/<uuid>
    ID 10867 gen 2541102 top level 5 path var/lib/docker/btrfs/subvolumes/<uuid>
    ID 10899 gen 2243967 top level 5 path var/lib/docker/btrfs/subvolumes/<uuid>
    ID 10900 gen 2177551 top level 5 path var/lib/docker/btrfs/subvolumes/<uuid>
    ...
    ```

=== "Nachher"

    ```
    ID 256 gen 3850 top level 5 path root
    ID 257 gen 3354 top level 5 path home
    ID 258 gen 3850 top level 5 path docker
    ID 259 gen 3850 top level 5 path libvirt
    ID 342 gen 3850 top level 5 path server-data
    ID 260 gen 140 top level 258 path docker/btrfs/subvolumes/<uuid>
    ...
    ```

[^askubuntu-custom-encrypt-install]: [AskUbuntu Antwort für angepasste verschlüsselte Installation :link:](https://askubuntu.com/questions/918021/encrypted-custom-install/918030#918030){target="_blank"}
[^baeldung-copy-over-whole-linux-system]: [Baeldung: Clone File System Hierarchy to Another Disk With Rsync :link:](https://www.baeldung.com/linux/rsync-clone-file-system-hierarchy){target="_blank"}
[^ButterFS-space-cache-v2]: [Btrfs/Space Cache :link:](https://wiki.tnonline.net/w/Btrfs/Space_Cache){target="_blank"}
