---
tags:
  - Linux
  - Ubuntu 22.10
  - Pipewire
  - Audio
title: Ubuntu 22.10 bringt Pipewire-Audio, aber Alsa ist kaputt
description: 
  Wärend bis einschließlich Ubuntu 22.04 noch Pulseaudio als Soundserver zum Einsatz kam, wurde mit 22.10 auf Pipewire gewechselt.
  Allerdings ist es in einem Zustand erschienen, bei dem zumindest ich nach einer neuen Installation erst händisch nachgreifen muss, damit alles einwandfrei läuft beim Ton.
date: 2022-10-28 00:00
update: 2022-10-28 00:00
list_entry: true
---

# {{ title }}

<p class="introduction" markdown>
<span class="date">{{ date }}</span><br>
{{ description }}
</p>

---

## Vorgeschichte

Den Erstkontakt mit Pipewire-Audio und generell dem Thema Audioserver auf Linux hatte ich durch den Wechsel ins Home-Office 2020. :person_tipping_hand:
Damals war das Problem, dass ich lösen wollte die schlechte Audioqualität mit Bluetooth-Headsets unter Linux.
Letztlich hat das nicht so ganz problemlos geklappt den Audioserver auf den vielversprechenden, hochgeprießenen Nachfolger von Pulseaudio umzustellen. :rolling_eyes:

Für das Thema Bluetooth Headset hatte sich das dann auch zeitnah erledigt, da ich in der Zwischenzeit eine kabelgebundene Lösung beschafft habe.
Der Wunsch den modernen neuen Audioserver zu nutzen blieb aber bestehen. :eyes:  
Nach weiterem Testen und letztlich schlicht Zufall, dass ich in einer aktualisierten Anleitung für den Wechsel einen entscheidenden Konfigurationsschritt entdeckt habe, konnte ich Pipewire auf meinen Systemen reproduzierbar funktionstüchtig einrichten. :partying_face:  
Die Details dazu sind in einem [anderen Eintrag hier zu finden](../1_pipewire-alsa-finally-fixed/index.md)

Mittlerweile war Ubuntu 22.04 erschienen und es ging auf die Oktober Version 22.10 zu.
Im Ubuntu-Bugtracker gab es ein Ticket der den Wunsch auf Pipewire-Audio zu wechseln dafür anbrachte (find es aber grad im Bugtracker nicht mehr :face_with_monocle:).

## Test der Daily Builds

Der erste Schritt war eine Installation des Daily Builds von 22.10 in einer VM.
Auffällig im Vergleich zum 22.04 Bestandssystem: Der Zoo von Konfigurationsdateien in `/etc/alsa/conf.d` war inexistent.
Es gab schon den Ordner `/etc/alsa` in gänze nicht.

### Testsetup

Der eine Test war nach wie vor der Stellwerksim(STS), da dass die Anwendung ist, die ich mitunter am ehesten auch tatsächlich mal nutze und das erste Programm war, mit dem ich auf das Problem gestoßen bin.
Allerdings jedes Mal den STS mehrfach zu starten, um dessen Selbsttest laufen zu lassen und alle anderen Vorbedingen zu schaffen die dieser braucht, war etwas aufwändig.
Daher: Ein altes Tetris in Java programmiert aus meinem Studium nutzt denselben Mechanismus und ist mit kleinerem Aufwand zum Laufen zu bringen.

### Unterschiede in VM :left_right_arrow: auf Hardware

In der VM hat der Ton augenscheinlich Out-of-the-Box einwandfrei funktioniert.
Bei meinen vergangenen Tests für den Wechsel zu Pipewire hatte ich bereits festgestellt, dass ich mit mehreren Audioausgängen zum Teil komisches Verhalten hatte.
Ein Test direkt auf meiner Hardware war daher unumgänglich. :point_up_tone2:

Da ich sowieso schon ausprobiert hatte auf einem USB-Stick mit BTRFS-Dateisystem mehrere Distros so parallel zu installieren, dass alle auf den gesamten Platz zugreifen können, habe ich dort einfach das Ubuntu 22.10 Daily noch dazu installiert.
Hier hat sich dann gezeigt, dass ganz ohne eine Konfiguration in `/etc/alsa/conf.d` Alsa Anwendungen nicht richtig funktionieren. :see_no_evil:  

Auf meiner tatsächlichen Hardware am Laptop habe ich mindestens 3 mögliche Audioausgänge: die eingebauten Lautsprecher, den Monitor über HDMI und die Stereoanlage am USB-Dock.
Ubuntu 22.10 Daily frisch installiert und nichts verstellt, hat mein Java Tetris immer mit voller Lautstärke über den HDMI Ton ausgegeben.
Der im GUI gewählte Ausgang oder die Systemlautstärke wurden völlig ignoriert.

Nach dem Installieren des Paketes `pipewire-audio-client-libraries` und kopieren der dabei mitgelieferten Beispiel-Konfiguration von `/usr/share/doc/pipewire/examples/alsa.conf.d/99-pipewire-default.conf` nach `/etc/alsa/conf.d` funktioniert es dann aber.
Nicht der Zustand den man sich als Nutzer so vorstellt, oder?

## Bugreport

??? info "Diskrepanzen im oben beschriebenen Verhalten und den Kommentaren im Ticket"
    Gab hier ein paar zeitliche Überschneidungen und in den ersten Daily Versionen von 22.10 war Pulseaudio und die Konfigurationen noch vorinstalliert, in späteren Versionen dann nicht mehr.
    Daher hat sich das beschriebene Verhalten natürlich auch etwas verändert und ich uU jetzt beim Aufschreiben des ganzen vermutlich auch eine etwas schwammige Wiedergabe der Erinnerungen.

Ich habe daher [ein Ticket :link:][lp-missing-conf-ticket]{:target="_blank"} für die notwendige manuelle Alsa Konfiguration angelegt, in der Hoffnung, dass das bis zum Erscheinen der finalen Version behoben werden würde.
Das hat dann noch ein [Upstream Ticket in Debian :link:][debbug-missing-conf-ticket]{:target="_blank"} bekommen.

Da ich etwas genauer versucht habe rum zu stochern, ist meine ursprüngliche Problembeschreibung im Ticket sehr spezifisch auf einen blockierenden Audio-Konsumer-Prozess geraten.
Das sollte sich später als Kommunikationshindernis herausstellen, denn eigentlich war mein final zu lösender Fehler ja nicht ein blockierender Prozess, sondern dass Alsa Anwendungen nicht wie gedacht agieren.  

Im Verlauf der Kommentare wurde irgendwann gemeldet der Fehler sei in pipewire `0.3.58-2ubuntu1` behoben.
Nach einem Testen des Daily-Build mit dieser Pipewire Version war das eigentliche Problem noch immer nicht behoben, ohne Alsa über Pipewire zu konfigurieren.
Und da hat sich offenbart, dass etwas aneinander vorbeigeredet wurde:
Pipewire hatte tatsächlichen einen Fehler, der zu einem Blockieren geführt hat und der wurde in genanner Version behoben.
Mein eigentliches Problem, hinter dem ich her bin, hat das natürlich nicht behoben und zu allem Überfluss kam der behobene Fehler mit der von mir eingesetzten Konfiguration gar nicht zum Tragen.
Daher hat sich aus meiner Sicht mit der neueren Pipewire Version gar nichts am Problem geändert und die Maintainer hatten gar nicht erfasst, was mein eigentlich verfolgtes Ziel ist, weil die ursprüngliche Fehlerbeschreibung zu spezifisch war.

## Status Quo

In den bisherigen Ubuntu Versionen mit Pulseaudio war Alsa vorkonfiguriert über Pulseaudio Ton auszugeben.
Mit dem Wechsel zu Pipewire wurde aber vergessen dieses Standard-Ton-Routing für Pipewire vorzukonfigurieren.
Das von mir erstellte Ticket hat zur Wichtigkeit dieses Umstands nicht wirklich beigetragen, sondern eher Verwirrung gestiftet, was denn eigentlich das Problem in meinem Ticket ist.

Im Upstream Ticket von Debian wurde aber bemerkt, dass das Platzieren der Alsa Konfigurationen für Pipewire [bereits auf der Todo-Liste steht :link:][debbug-missing-conf-ticket-msg61]{:target="_blank"}.
Also hoffen wir einfach mal, dass das bald nachgereicht wird.
Was ich mich dabei gefragt habe: gibt es nur wenige Alsa Anwendungen, dass das nicht breiter aufgefallen ist, oder habe ich einfach eine komische Hardware Kombination und bei anderen tritt das gar nicht auf? :thinking:

[debbug-missing-conf-ticket]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1013276
[debbug-missing-conf-ticket-msg61]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1013276#61
[lp-missing-conf-ticket]: https://bugs.launchpad.net/ubuntu/+source/pipewire/+bug/1975823
