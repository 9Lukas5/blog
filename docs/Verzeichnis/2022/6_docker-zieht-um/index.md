---
tags:
    - Linux
    - Ubuntu
    - Server
    - Libvirt
    - Docker

title: Docker-Dienste ziehen um
description:
    Nachdem sich der Parallelbetrieb von Libvirt und Docker bereits mehrfach als ungeschickt erwiesen hat, habe ich die Docker-Dienste jetzt in eine separate VM umgezogen.
date: 2022-12-10 00:00
update: 2022-12-10 00:00
list_entry: true
---

# {{ title }}

<p class="introduction" markdown>
<span class="date">{{ date }}</span>  
{{ description }}
</p>

---

Im [letzten Beitrag zu dem Thema][letzterEintrag] habe ich bereits überlegt, die Docker-Dienste in eine eigene VM umzuziehen.
Da ich mit dem Ipv6-Betrieb erneut einen Fall hatte, bei dem der Parallelbetrieb mit Libvirt-VMs ein Problem war, habe ich das jetzt tatsächlich umgesetzt.
Dabei gab es ein paar Kleinigkeiten, die dann noch zutage getreten sind, nachdem die in meinem Setup aktiven Docker-Container plötzlich auch eine Ipv6 Adresse hatten.

## Altes vs. Neues Setup

=== "Alt"

    ```plantuml
    @startuml

    object Host

    object Libvirt
    object OPNSense
    object Asterisk

    object Docker
    object "Container (Mail/GitLab/...)" as container

    Host <|-- Libvirt
    Host <|-- Docker

    Libvirt <|-- OPNSense
    Libvirt <|-- Asterisk

    Docker <|-- container

    @enduml
    ```

=== "Neu"

    ```plantuml
    @startuml

    skinparam object {
        BackgroundColor<<neu>> LightGreen
    }

    object Host

    object Libvirt
    object OPNSense
    object Asterisk
    object "Linux VM" as dockerHostVM<<neu>>

    object Docker
    object "Container (Mail/GitLab/...)" as container

    Host <|-- Libvirt

    Libvirt <|-- OPNSense
    Libvirt <|-- Asterisk
    Libvirt <|-- dockerHostVM

    dockerHostVM <|-- Docker
    Docker <|-- container

    @enduml
    ```

## Änderungen drumherum

### Docker

Der Docker-Dienst auf der neuen VM wurde mit folgender `/etc/docker/daemon.json` versehen:

```json
{
    "userland-proxy": false,
    "ipv6": true,
    "fixed-cidr-v6": "<<ULA-Prefix>>",
    "experimental": true,
    "ip6tables": true
}
```

Und die Docker-Compose Konfiguration wurde im Bereich der Netzwerke jeweils um folgende Einträge ergänzt:

```yaml
networks:
  <<name>>:
    ...
    enable_ipv6: true
    ipam:
      config:
        - subnet: <<ULA-Prefix>
```

### TLS-Zertifikat

Da der Reverse-Proxy das TLS-Zertifikat und Schlüssel braucht musste auch entsprechend, wie auf dem bisherigen Host, alles eingerichtet werden, dass die OPNSense per SCP die erneuerten Zertifikate und Schlüssel auf der Maschine ablegen kann.

### DynDNS

Genauso musste der DynDNS-Dienst auf die neue VM umziehen, da diese ja dann mit ihrer eigenen IPv6-Adresse arbeitet und dementsprechend diese im DNS-Eintrag landen muss.

## Überraschungseffekte

Ein unerwarteter Effekt kam dann noch beim Dovecot-Server fürs IMAP zutage.
Der Dovecot war plötzlich von außen nur noch über Ipv4 ansprechbar, auf Ipv6 war der Port geschlossen.
Stellte sich raus: In meiner Konfiguration stand `listen = *`, womit der Dovecot allerdings NUR auf Ipv4 Adressen hört.

Im alten Setup war das kein Problem, da der Dovecot in seinem Container-Netzwerk auch nur eine Ipv4 hatte und alle Anfragen an den Host, unabhängig ob über Ipv4 oder Ipv6 empfangen, an die Ipv4 Adresse des Containers ging.
Im neuen Setup gehen Ipv4 Anfragen am Host auf die Ipv4 Adresse im Container und Ipv6 Anfragen auf die Ipv6 im Container.
Daher musste hier die Listen-Anweisung entsprechend auf `listen = [::]` geändert werden, damit der Dovecot tatsächlich auf Ipv4 & Ipv6 hört.

[letzterEintrag]: ../2_docker-ipv6-attempts/index.md
