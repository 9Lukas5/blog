---
tags:
  - Docker
  - Ipv6
  - NAT
  - Networking
  - Server
  - Operations
  - Linux
title: Docker Ipv6 NAT und KVM VMs geht nicht
description: 
  Auf meinem Server laufen verschiedene Dienste. Die meisten als Docker-Container, sowie zwei als virtuelle Maschine in KVM mit vollwertiger Netzwerkbrücke.
    Die Docker Container können, als Client agierend, aber kein Ipv6.
    Das wollte ich ändern, geklappt hat das allerdings nicht.
date: 2022-10-27 00:00
update: 2022-10-27 00:00
list_entry: true
---

# {{ title }}

<p class="introduction" markdown>
<span class="date">{{ date }}</span>  
{{ description }}
</p>

---

## Ausgangssituation

Der Server ist ein Ubuntu 20.04 auf dem mehrere Dienste laufen.
Zwei esentielle davon sind VMs: Der Router ([OPNSense :link:][opnsense]{:target="_blank"}) und die Telefonanlage ([FreePBX/Asterisk :link:][freepbx]{:target="_blank"}).
Der Rest ist im Docker: Verschiende Dienste für E-Mail ([Postfix :link:][postfix]{:target="_blank"}, [Dovecot :link:][dovecot]{:target="_blank"}, [OpenDKIM :link:][opendkim]{:target="_blank"}), eine [GitLab :link:][gitlab]{:target="_blank"} Instanz, ein eigenes [FDroid Repo :link:][fdroid]{:target="_blank"} und zum Schluss einen [Nginx Reverse-Proxy :link:][nginx-proxy]{:target="_blank"} für alle mit HTTP Verkehr.

Der Host läuft mit Full-DualStack, also hat IPv4 sowie Ipv6 Konnektivität.
Innerhalb der Docker-Netzwerke gibt es standardmäßig nur IPv4 Adressen.

```plantuml
@startuml

left to right direction

rectangle "Client" as client
rectangle "Switch" as switch

rectangle "Server" as server {

    port "Host" as if_host
    port "KVM\n\tNetzwerkbrücke\t" as if_kvmBridge

    rectangle "Docker" as docker {

        port "NAT" as if_dockerHost

        rectangle "E-Mail" as mail
        rectangle "GitLab" as gitlab
        rectangle "FDroid" as fdroid
        rectangle "Rev-Proxy" as proxy


        if_dockerHost -- proxy: "80,443"
        if_dockerHost -- mail: "25,993,465,587"
        if_dockerHost -- gitlab: "7022"
        proxy -- gitlab: 80
        proxy -- fdroid: 80
        proxy -- mail: 80
    }

    rectangle "VMs" as kvm {
        rectangle "Router" as router
        rectangle "FreePBX" as freepbx
    }
}

client -r- switch: "IPv4\nIpv6"
switch -- "IPv4\t\nIpv6\t" if_host
switch -r- if_kvmBridge
if_kvmBridge -- "IPv4\nIpv6" router
if_kvmBridge -- "IPv4" freepbx

if_host -- if_dockerHost


@enduml
```
<span class="caption">Diagramm: Dienste-Übersicht</span>


## Problem

Für den Zugriff von Clients auf die Server-Dienste spielt die IP-Version innerhalb des Containers keine Rolle.
Für eingehenden Verkehr sprechen diese eine Host-Adresse an und dieser ist in beiden Protokoll-Versionen erreichbar.
Von da an werden die Pakete in den Ziel-Container genattet.

Eine Einschränkung gibt es erst, wenn ein Server-Dienst als Client agieren will.
Beispiel: Der SMTP-Server, wenn er ausgehende Mails ausliefert.
Da im Container nur eine IPv4 Adresse existiert, kann dieser auch nur IPv4 Adressen ansprechen.
Sollte ein Ziel allerdings nur auf Ipv6 erreichbar sein, kann die Verbindung nicht hergestellt werden.

## Lösungsansatz

Aktuell stellt das ganze keine unmittelbare Betriebseinschränkung dar, aber als Nerd stört es mich natürlich, dass eines meiner zentralen Tools technisch nicht in der Lage ist Ipv6 zu sprechen.
Glücklicherweise ist es nicht völlig unmöglich Docker-Container Ipv6 beizubringen, es ist aber eben noch nicht by Default aktiviert.

Die Idee das Problem zu beheben bestand quasi aus zwei Schritten, auf die man in der [Readme des nginx-proxy verwiesen wird :link:][nginx-proxy-ipv6]{:target="_blank"}:

??? example "1. Grundsätzlich Ipv6 in der Docker Konfiguration einschalten (1)"

    Konfiguration in `/etc/docker/daemon.json` ergänzen um den Schalter fürs Ipv6 und ein ULA Präfix festlegen, aus dem sich die Container dann Adressen konfigurieren.

    ```json
    {
        "ipv6": true,
        "fixed-cidr-v6": "2001:db8:1::/64"
    }
    ```
    Quelle: [Docker Dokumentation :link:](https://docs.docker.com/config/daemon/ipv6/){:target="_blank"}

??? example "2. Einen zusätzlichen Container laufen lassen für Ipv6 NAT des ausgehenden Verkehrs, da Docker das scheinbar (noch?)nicht implementiert hat"

    Der Ipv6NAT Container muss im Host-Netzwerk laufen und braucht einige Sonderrechte.
    Der einfachste Weg ist den Container einfach mit `--privileged` laufen zu lassen, das Readme weist aber darauf hin, dass es ausreicht `--cap-add NET_ADMIN --cap-add SYS_MODULE` zu nutzen.  
    Da bei mir die gesamte Konfiguration mit [Docker-Compose :link:][compose]{:target="_blank"} stattfindet, habe ich für diesen Container entsprechend einen zusätzlichen Dienst in meine Konfiguration geplant:

    ```yaml
    ...
    services:
        compose_v6nat:
        container_name: compose-v6nat
        image: 'robbertkl/ipv6nat'
        logging:
        options:
            max-size: "100k"
            max-file: "10"
        restart: unless-stopped
        network_mode: host
        cap_drop:
        - ALL
        cap_add:
        - NET_ADMIN
        - NET_RAW
        volumes:
        - /var/run/docker.sock:/var/run/docker.sock:ro
    ...
    ```

    Quelle: [Docker-Ipv6NAT Readme :link:](https://github.com/robbertkl/docker-ipv6nat#usage){:target="_blank"}

Die gesamten Veränderungen in meiner Docker-Compose Konfiguration sind wie folgt:

??? example "Git Diff der Änderungen"
    ```diff
    diff --git a/docker-compose.yml b/docker-compose.yml
    index fba162d..2fe712b 100644
    --- a/docker-compose.yml
    +++ b/docker-compose.yml
    @@ -1,16 +1,34 @@
    -version: '3'
    +version: '2.2'
    
    services:
    +  IPV6NAT:
    +    container_name: IPV6NAT
    +    image: 'robbertkl/ipv6nat'
    +    logging:
    +      options:
    +        max-size: "100k"
    +        max-file: "10"
    +    restart: unless-stopped
    +    network_mode: host
    +    cap_drop:
    +      - ALL
    +    cap_add:
    +      - NET_ADMIN
    +      - NET_RAW
    +    volumes:
    +      - /var/run/docker.sock:/var/run/docker.sock:ro
    +
    NGINXPROXY:
        container_name: NGINXPROXY
        image: 'nginxproxy/nginx-proxy'
        logging:
        options:
            max-size: "100k"
            max-file: "10"
        restart: unless-stopped
        environment:
    -      SSL_POLICY: "Mozilla-Modern"
    +      - ENABLE_IPV6=true
    +      - SSL_POLICY=Mozilla-Modern
        ports:
        - "80:80"
        - "443:443"
    @@ -256,7 +274,19 @@ services:
    
    networks:
    private-services:
    +    enable_ipv6: true
    +    ipam:
    +      config:
    +        - subnet: 2001:db8:0::/64
    mail:
        driver_opts:
          com.docker.network.driver.mtu: 1460
    +    enable_ipv6: true
    +    ipam:
    +      config:
    +        - subnet: 2001:db8:1::/64
    ```

## Ergebnis

!!! info "TL;DR"
    :white_check_mark: Container haben Ipv4 & Ipv6 Adresse  
    :white_check_mark: Container ausgehend funktioniert über beide  
    :x: Container eingehend kommt im Container an

    Die Dienste können jetzt als Clients Ipv6 sprechen, aber die Primärfunktion des angesprochen werdens funktioniert nicht mehr.

Die Container haben aus den konfigurierten ULA-Präfixen Adressen erzeugt und können ausgehend auf beiden Ip-Versionen sprechen.
Der eingehende Verkehr klappt allerdings überhaupt nicht mehr.
Im Log des Reverse-Proxy ist einfach überhaupt nichts zu sehen: Der Verkehr kommt also nicht mal bis dort, sondern versickert schon irgendwo davor.

In einem Testaufbau auf meinem normalen Rechner funktioniert tadellos.
Ist zwar nicht 100%ig gleich, da der Server ein Ubuntu 20.04 ist und mein Rechner ein Fedora 36, aber das glaube ich dürfte das geringste Problem sein.  
Meine Vermutung: __Iptables__

Auf dem Server laufen ja auch zwei VMs in KVM für die ich eine vollwertige Netzwerkbrücke eingerichet habe.
Das gemeinsam mit Docker verträglich einzurichten war auch schon ein Ärgernis, bei dem ich im Docker-Dienst ein paar Iptables Kommandos in den Startaufruf einbetten musste:

```sh
[Service]
# make Docker containers and KVM bridge work nicely alongside
ExecStartPre=/usr/sbin/iptables -A FORWARD -i firewall-wan -o firewall-wan -j ACCEPT
ExecStartPre=/usr/sbin/iptables -A FORWARD -i firewall-lan -o firewall-lan -j ACCEPT
ExecStartPre=/usr/sbin/iptables -t nat -A POSTROUTING -s 192.168.0.0/16 -d 192.168.0.0/16 -j ACCEPT
ExecStartPre=/usr/sbin/iptables -t nat -A PREROUTING -s 192.168.0.0/16 -d 192.168.0.0/16 -j ACCEPT
```

Das ist ein Punkt an dem ich ehrlicherweise noch nicht so ganz verstanden hab, was die Befehle genau tun, aber sie sorgen dafür, dass beide miteinander funktionieren.
Aber da diese Kombination Docker und KVM-Netzwerkbrücke schon einmal Grund für Netzwerk-Probleme war, geht mein Tipp ganz stark dahin als Grund dafür, dass es nicht funktioniert wie in der Testumgebung ohne KVM-Netzwerkbrücke.

## Status Quo

Die Container bleiben erstmal so wie sie sind. :shrug:
Ich müsste jetzt eine Testumgebung aufsetzen, in der ich auch eine KVM VM mit Netzwerkbrücke einrichte und dort das Docker Setup testen, um sicher sagen zu können, dass das der Grund für die Probleme ist. :face_with_monocle:  
Mach ich irgendwann vielleicht.
Mal sehen.
Alternativ: Eine dritte VM in der ich die Docker-Dienste laufen lasse, dann würden Docker und KVM nicht mehr nebeneinander, sondern ineinander laufen.
Aber ist das sinnvoll? :thinking:

[compose]: https://docs.docker.com/compose/
[dovecot]: https://www.dovecot.org/
[fdroid]: https://gitlab.com/fdroid/fdroidserver
[freepbx]: https://www.freepbx.org/
[gitlab]: https://gitlab.com/
[nginx-proxy-ipv6]: https://github.com/nginx-proxy/nginx-proxy#ipv6-nat
[nginx-proxy]: https://github.com/nginx-proxy/nginx-proxy
[opendkim]: http://www.opendkim.org/
[opnsense]: https://opnsense.org/
[postfix]: https://www.postfix.org/
