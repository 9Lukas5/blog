---
tags:
  - Elektronik
  - Löten
  - Akkus
  - Werkstatt
  - Basteln
  - Reparieren

title: Akkus bei Schallzahnbürste tauschen
description:
    Ich habe eine elektrische Schallzahnbürste, deren Akku in letzter Zeit rapide schlechter geworden ist.
    Nachdem eine Rekonditionierung keinen Erfolg gebracht hat, habe ich jetzt andere Akkus verbaut, die ich sowieso da hatte.
image: akkus-geloest.avif
date: 2022-11-01 00:00
update: 2022-11-01 00:00
list_entry: true
---

# {{ title }}

<p class="introduction" markdown>
<span class="date">{{ date }}</span>  
{{ description }}
</p>

---

## Grundsätzlicher Aufbau

<figure markdown>
  ![Zahnbürste ausgebaut auf Holztisch](./zahnbuerste-ueberblick.avif){ loading=lazy }
  <figcaption>Zahnbürste nach Ausbau aus Gehäuse</figcaption>
</figure>

Die Zahnbürste besteht aus zwei großen Bauteilen:
Dem oberen Teil (hier links im Bild) auf das der Bürstenaufsatz aufgesteckt wird und dem unteren Teil der aus Platine, Akkus und Induktionsspule besteht.  
Der obere Teil ist quasi die Steckaufsatz-Aufnahme mit einer magnetischen Gabel dran.
Der untere Teil ist die gesamte Platine, mit einem Elektromageneten um den die Gabel des Oberteils herumgreift.
Es gibt keine feste Verbindung beider Teile.

Mechanische Bauteile gibt es keine: Die Wirkung wird nur durch die magnetische Gabel des Oberteils um die Spule des Unterteils erzeugt.
Ich habe die Schaltung jetzt nicht genauer untersucht im Versuch diese exakt nachzuvollziehen.
Meine Vermutung ist eine schlichte Kippschaltung für die Spule, womit deren Magnetfeld ständig wechselt und die magnetische Gabel dadurch hin und her wirft.

Eine erfreuliche Überraschung war der Akku.
Ich hatte hier irgendetwas Spezielles erwartet, was sowieso den Kauf bestimmter Ersatzteile erfordert oder eine deutlich kreativere Bastelei, falls man das auf Teufel komm raus selbst bauen will.
Aber zu meiner Freude waren dort zwei 0815 AAA-Akkus in Reihe geschaltet verbaut. :partying_face:

## Akku-Rekonditionierung

Erste Idee einer Verbesserung mit Mitteln, die im Haus sind, war die Akkus mit einem externen Ladegerät, statt der verbauten Schaltung, zu laden.
Ich hatte im Kopf, dass Nickel-MetallHydrid Akkus oft an Kapazität verlieren, was man durch mehrmalig vollständig Entladen und wieder Aufladen teilweise wieder aufheben kann.
Nach einer schnellen Suche: Das Internet kennt das als Batterieträgheitseffekt, oder auch Memory-Effekt[^1].

Ich habe so ein 0815 Ladegerät für vier Akkus, dass auch eine "Refresh" Funktion hat.
Dabei geht es mit mehreren Zyklen über einen Akku, wobei ich jetzt nicht sagen kann, ob es einfach eine festgelegte Anzahl Zyklen sind (glaube ich eher nicht), oder ein Ladungs-Delta der letzten beiden Zyklen vergleicht bis ein gewisser Wert unterschritten wird (meine Vermutung).

<figure markdown>
  ![Zahnbürste auf Karton, Ladegerät daneben. Anschluss mit zwei Kabeln einfach ins Ladegerät und Zahnbürste geklemmt](./versuch-rekonditionierung.avif){ loading=lazy }
  <figcaption>Etwas hemdsärmliche Ladeverkabelung</figcaption>
</figure>

Da die Akkus nicht herausnehmbar, sondern fest verlötet sind, habe ich mir einfach ein Kabel aus der Werkstatt genommen und ins Ladegerät geklemmt, sowie an einen der in Reihe geschalteten Akkus,
um so einen nach dem anderen mit dem Refresh-Programm des Ladegerätes zu bearbeiten.

Der Refresh an sich hat so weit funktioniert wie er soll: Das Ladegerät hat irgendwann aufgehört und den Akku als "Voll" gemeldet.
Aber auch direkt nach dem Refresh war nur einmal Zähneputzen möglich, bevor die Status LED schon wieder gelb geleuchtet und damit einen niedrigen Akkustand signalisiert hat.
So einfach waren die alten Akkus also nicht zu retten :rolling_eyes:

## Einbau anderer Akkus

Da ich hier sowieso einige NiMH-Akkus herumliegen hab, die mehr liegen, als dass ich sie tatsächlich mal wo brauche, war die nächste Idee zwei davon in die Zahnbürste zu versetzen, in der Hoffnung, dass die noch in besserem Zustand sind.

<figure markdown>
  ![Nahaufnahme Zahnbürste, Elektromagnet links, Akkus rechts. Akkus sind aus Plastik Nase die sie festhalten bereits aushängt. Die Nase und die beiden Lötstellen auf der Platine sind markiert.](./akkus-geloest.avif){ loading=lazy }
  <figcaption>Akkus aus Fixierung(Markierung) gelöst, Anschlusspunkte der Akkus markiert</figcaption>
</figure>

Angefangen hab ich mal damit die verbauten Akkus zu lösen, damit ich mir auch sicher bin, dass ich die ausgebaut bekomme, bevor ich die Anschlüsse ablöte.

Danach hab ich die beiden Tauschakkus vorbereitet.
Dafür hab ich sie in einen kleinen Tischschraubstock geklemmt und erstmal beide in Reihe verbunden.
Um das Kabel auf die Akkus zu bekommen, hab ich Lötfett benutzt und erst mal ein bisschen Lot angebracht.
Don't judge: Ich hab das erste Mal mit Lötfett experimentiert, ich weiß die ersten beiden Lötpunkte sind scheiße :see_no_evil:

!!! tip "Lesson Learned"
    Beim Lötfett nicht klotzen, sondern nur ganz, ganz, wenig kleckern

<figure markdown>
  ![Tauschakkus in Schraubstock geklemmt. Senkrecht stehend, nebeneinander einer mit Plus einer mit Minus oben. Linke Bildhälfte vorher, rechte Bildhälfte nachher. Lötpunkte mit viel Lot und Lötfett Resten](./neue-akkus-verloeten.avif){ loading=lazy }
  <figcaption>Verbindung zwischen beiden Akkus gelötet</figcaption>
</figure>

Ich hab dann auf der anderen Seite noch einen roten Draht an Plus und einen braunen an Minus gelötet und versucht in den Schacht zu setzten.
Blöd nur: Die originalen Zellen haben auf der Plusseite keine herausstehende Nase und erst recht nicht so einen Klotz Lötzinn oben drauf.  
Also die Kabel erstmal nochmal abgelötet und mit deutlich hübscheren und flacheren Lötstellen neu angebracht.
Bei der Minusseite hab ich es einfach versucht mit wenig aber ausreichend Zinn flach anzulöten, bei der Plusseite hab ich es dann seitlich an der Nase angelötet.

<figure markdown>
  ![Selbe Ansicht wie zuvor auf die Zahnbürste, jetzt mit den Tauschakkus im Schacht und den angelöteten Kabeln an der Platine angelötet](./endergebnis.avif){ loading=lazy }
  <figcaption>Endergebnis des Akku-Tausches mit Blick auf die Kabellage und Lötstellen an der Platine</figcaption>
</figure>

Mit der Kabelschleife, die beide Akkus miteinander verbindet, hatte ich genug Bewegungsfreiheit die Zellen jeweils bequem von der Seite in den Schacht zu schieben, statt als fest verbundenes Paket an der recht unbeweglichen Plastiknase vorbeipressen zu müssen.
Auf der Anschlusseite sind die Kabel jeweils etwas länger als Notwendig und zwischen Plastik und Platinen Unterseite in einer kleinen Schleife untergebracht, bevor sie dieselbe Stelle wie die alte Verbindung zum Wechsel an die Oberseite nutzen und dort angelötet wurden.

## Ergebnisse

Nach einem halben Tag kann ich auf jeden Fall schon mal sagen, dass es eine deutliche Verbesserung zu den alten Akkus ist.
Die Zeit wird zeigen wie groß die Verbesserung tatsächlich ist und vor allem wie lange sie halten wird.

Sollte das Mittel- bis Langfristig nicht zufriedenstellend bleiben, hatte ich schon überlegt Lithium Akkus zu ordern die durchgängig 1.5V liefern, statt 1.2V die dann abfallen.
Das würde allerdings mit noch ein wenig mehr Aufwand einhergehen, denn bei denen wäre die Ladespannung ja viel höher (glaub ~5V oder so?).
Man müsste dann eine zusätzliche kleine Schaltung bauen, die man an die Induktionsspule anschließt und für die Lithium Akkus eine passende Ladespannung erzeugt.
Wäre noch die Frage, ob man die Lithium-Akkus dann in Reihe laden kann, oder etwas braucht/bauen sollte, dass die Zellen einzeln lädt? :thinking:  
Aber jetzt hoff ich erstmal, dass es mit anderen Akkus erstmal wieder eine Weile gut ist. :fingers_crossed:


[^1]: [Studysmarkter: NiMH Akku :link:](https://www.studysmarter.de/schule/chemie/physikalische-chemie/nimh-akku/){target="_blank"}
