---
tags:
  - Linux
  - Pipewire
  - Pulseaudio
  - Audio
  - Alsa
title: Alsa mit Pipewire funktioniert endlich
description:
    Bei dem Versuch Pulseaudio durch Pipewire zu ersetzen, hatte ich immer Probleme mit Alsa-Anwendungen.
    Nach über 2 Monaten und Versuchen mit mehreren Distributionen ist es mir jetzt endlich gelungen.
date: 2022-06-09 00:00
update: 2022-06-09 00:00
list_entry: true
---

# {{ title }}

<p class="introduction" markdown>
<span class="date">{{ date }}</span>  
{{ description }}
</p>

---

## Erstkontakt mit Pipewire

Der Ursprung dieser Story liegt am Wechsel ins HomeOffice und den damit verbundenen Online-Meetings.
Es hat nicht wirklich geeilt ein gutes Mikrofon Setup zu haben, da ich im Studium als Student doch eher Konsument, als Produzent von Ton bin.
Wenns mal notwendig war, habe ich ein Mono-Bluetooth-Headset eingesetzt.  
Gute Audioqualität über Bluetooth mit Linux ist allerdings ein Albtraum.
Entweder man hat nur Output und kann mit guter Qualität zB Musik hören oder Filme sehen, oder man will In- & Output für Meetings, hat dann aber unglaublich schlechte Tonqualität.

In der Bestrebung mehr aus meiner vorhandenen Peripherie zu holen bin ich auf Pipewire gestoßen, da es den besseren `mSBC` Codec unterstützt.
Während der alte `SBC` Codec unter Pulseaudio nur 8kHz Abtrastrate im Headset-Modus untestützt, kann `mSBC` 16kHz.
Android und Windows machen das schon länger.

Aber: Ich hatte unter Pipewire mit meinem BT-Headset allerdings immer wieder komische Verzerrungen, die für Meetings auch nicht gut waren.
Ich konnte das auf den parallel Betrieb von Bluetooth und WLAN zurückführen, wodurch ich erstmal ein Hardware-Problem vermutete.
Zusätzlich hatte ich Stellwerksimulator den ich immer wieder spiele immer ein komisches Knacken im Ton und Audacity hat sich beim starten komisch verhalten.  
Letztlich hat das dazu geführt, dass ich mir eine USB-Webcam mit eingebautem Mikrofon zugelegt habe, zurück zu Pulseaudio bin und das Thema Pipewire für Audio vorerst wieder beerdigt habe.

---

## Forschungstrieb

Der andere Punkt für den ich Pipewire eingerichtet und auch weiter genutzt habe, war um in Konferenzen unter Wayland meinen Bildschirm teilen zu können.
Mich hat dann aber doch der Forschungstrieb gepackt, einen modernen Audioserver einzusetzten.
Ich habe also mit neueren Versionen immer mal wieder zum Testen den Pulseaudio Dienst gestoppt und durch den Pipewire Dienst ersetzt.
Da ich mittlerweile das Mikrofon in der USB-Webcam nutze war, hatte ich das Bluetooth Headset mittlerweile gar nicht mehr so im Fokus, aber der Stellwerksim und Audacity mussten ohne Probleme funktionieren, um dauerhaft zu wechseln.
Irgendwann kam der Stellwerksim allerdings über seinen Selbsttest beim Programmstart nicht mehr hinaus und konnte den Ton-Test nicht mehr beenden.

### Problem Alsa Clients

Der Stellwerksim ist eine Java-Webstart Anwendung, die ich mit OpenWebstart nutze.
Nachdem ich mit dem Decompiler den Java Code für den Ton-Test ausfindig gemacht hatte, habe ich festgestellt der Mechanismus für die Tonausgabe ist derselbe, den ich auch in einer eigenen Bibliothek einsetze: `javax.sound`.  
Ein schneller Test hat gezeigt auch meine Bibliothek funktioniert unter Pipewire aktuell nicht und es scheint sich bei dem Problem wohl pauschal um Alsa-Clients zu handeln.

### Tests auf mehreren Distributionen

Ich habe dann in einer virtuellen Maschine Debian, Ubuntu, Fedora und Manjaro installiert.
Davon war Fedora das einzige das Pipewire bereits standarmäßig für Audio eingesetzt hat.
Auf den anderen habe ich das jeweils selbst installiert und Pulseaudio damit ersetzt.  
Ergebnis: Auf Fedora hat der Stellwerksim einwandfrei funktioniert, auf den anderen nicht.
Dass es irgendeine Konfiguration sein musste, war recht offensichtlich, nur konnte mir offenbar niemand direkt weiterhelfen, wo es sein könnte, oder was man nach und nach abprüfen könnte um es finden.


<span class="caption">Tabelle: Übersicht getesteter Distributionen und eingesetzter Versionen</span>

|                          |                                       |                      |                               |                  |
|--------------------------|---------------------------------------|----------------------|-------------------------------|------------------|
| Distribution and version | Fedora Linux 35 (Workstation Edition) | Ubuntu 20.04.4 LTS   | Debian GNU/Linux bookworm/sid | Manjaro Linux    |
| Pipewire version         | 0.3.48                                | 0.3.48               | 0.3.48                        | 0.3.48           |
| Wireplumber version      | 0.4.9-1                               | 0.4.8.r20.gb95da33-1 | 0.4.9-1                       | 0.4.8-2          |
| Desktop Environment      | Gnome 41 Wayland                      | Gnome 41 Wayland     | Gnome 42 Wayland              | Gnome 41 Wayland |
| Kernel version           | 5.16                                  | 5.16                 | 5.16                          | 5.15             |
| state                    | :white_check_mark:                    | :x:                  | :x:                           | :x:              |

### Lichtblick

Eine genutzte Anleitung um den Audioserver auf Pipewire umzustellen war [ursprünglich für Ubuntu 21.04 und 21.10][ubuntuHandbook-2104] geschrieben.
Dann habe ich dort zufällig eine [aktualisierte Anleitung für Ubuntu 22.04][ubuntuHandbook-2204] gesehen, in der mir ein kleiner Unterschied zur älteren Version aufgefallen ist.

---

## Lösung

Es wurden aus den installierten Pipewire-Docs Beispiel-Konfigurationen für Alsa und Jack Clients in deren entsprechenden Ordner im System kopiert.
Ein Blick in den Ordner für Alsa offenbarte in meinem Ubuntu 20.04 eine ganze Reihe Dateien, unter anderem auch welche für Pipewire.

??? info "Ursprünglicher Inhalt `/etc/alsa/conf.d`"
    ```
    10-samplerate.conf
    10-speexrate.conf
    50-arcam-av-ctl.conf
    50-jack.conf
    50-oss.conf
    50-pipewire.conf
    50-pulseaudio.conf
    60-upmix.conf
    60-vdownmix.conf
    98-usb-stream.conf
    99-pulseaudio-default.conf.example
    99-pulse.conf
    ```

Nach etwas probieren habe ich dann die Konfigurationen die Pulse im Name hatten entfernt, und sichergestellt dass die beiden Pipewire Konfigurationen vorhanden sind.
Ganz ohne die Pipewire Konfigurationen ging es nur teilweise, da bei mehreren möglichen Audio-Ausgängen die Alsa Anwendungen sonst immer einen anderen nutzten, wie eigentlich systemweit ausgewählt war.

??? info "Geänderter Inhalt `/etc/alsa/conf.d`"
    ```
    10-samplerate.conf
    10-speexrate.conf
    50-arcam-av-ctl.conf
    50-jack.conf
    50-oss.conf
    50-pipewire.conf
    60-upmix.conf
    60-vdownmix.conf
    98-usb-stream.conf
    99-pipewire-default.conf
    99-pulseaudio-default.conf.example
    ```

Während die `50-pipewire.conf` von einem der Pipewire-Pakete bei der Installation automatisch platziert wurde, habe ich das `99-pipewire-default.conf` aus `/usr/share/doc/pipewire/examples/alsa.conf.d/` kopiert, das man durch die Installation von `pipewire-audio-client-libraries` bekommt.

Damit konnte ich jetzt reproduzierbar auf Debian, Ubuntu (20.04, 22.04 und 22.10 Daily) das Audio auf Pipewire umstellen (Die Manjaro Maschine hatte ich mittlerweile nicht mehr).
Der Stellwerksim, Audacity und meine Bibliothek funktionieren alle ohne irgendwelches Knacken, komisches Verhalten oder ähnlichem.  
Aus Interesse habe ich auch mein Bluetooth Headset mal wieder getestet und konnte die Verzerrung nicht mehr reproduzieren.
Da kann aber auch sein ich hatte nur Glück, oder irgend eine andere zwischenzeitliche Änderung in Pipewire hat das Problem behoben.

---

## Links

- [Debian-Pipewire GithubIO Seite][githubio-pipewire-debian]
- UbuntuHandbook
    - [How to Enable PipeWire Audio Service to Replace PulseAudio in Ubuntu
      21.10 & 21.04][ubuntuHandbook-2104]
    - [How to Use PipeWire to replace PulseAudio in Ubuntu
      22.04][ubuntuHandbook-2204]
- Freedesktop Pipewire Issue Tracker
    - [Bluetooth Headset Problem][freedesktopGitlab-mSBC]
    - [Alsa Clients kollidierende Konfigurationen nach manuellem
      Wechsel][freedesktopGitlab-alsa-clients]

[freedesktopGitlab-alsa-clients]: https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/2255
[freedesktopGitlab-mSBC]: https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/1833
[githubio-pipewire-debian]: https://pipewire-debian.github.io/pipewire-debian/
[ubuntuHandbook-2104]: https://ubuntuhandbook.org/index.php/2021/05/enable-pipewire-audio-service-ubuntu-21-04/
[ubuntuHandbook-2204]: https://ubuntuhandbook.org/index.php/2022/04/pipewire-replace-pulseaudio-ubuntu-2204/
