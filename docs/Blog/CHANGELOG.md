---
title: Versionshinweise
description: Changelog über neue Einträge und sonstige Änderungen am Text und technischen Unterbau
---

{% include-markdown "../CHANGELOG.text" -%}
