---
title: 9Lukas5 - Über Mich
---

# Über Mich

Ich hinterfrage unheimlich gerne Dinge.
Wenn mir also etwas komisch/unschlüssig erscheint, werd ich höchst wahrscheinlich fragen: "Wo stehts?". :thinking: :eyes:  
Damit geh ich regelmäßig Leuten auf die Nerven, aber hey: nicht mein Problem :shrug:

=== "Allgemeines"

    Ich bin von klein auf Eisenbahn begeistert gewesen und habe mir mit der Lokführer Ausbildung einen Kindheitstraum erfüllt.
    Da ich mich nur mit dieser Ausbildung aber noch nicht ausgelastet gefühlt hatte, habe ich noch das Studium obendrauf gesetzt :nerd_face:

    Meine Interssen liegen primär im Bereich Technik, Eisenbahn, Science-Fiction und Magie.
    Meine Handys haben seit dem ersten Smartphone immer Custom Roms gehabt, auf dem PC habe ich als letztes Windows 7 genutzt bevor seit 2018ish nur noch Linux Distros im Einsatz sind.

    Ich bin ein Git-Guru.
    Wer in meinen Projekten irgendwo mitmachen will, muss sich je nach Projekt darauf einstellen, dass ich versuchen werde Workflow's und Git-Handhabung betreffend explizite Regeln zu etablieren und auf diesen rumreiten werde :grimacing:

    Aufgewachsen bin ich mit _Harry Potter_ und habe mich dementsprechend über _Phantastische Tierwesen_ gefreut, bin ein Fan von _Stargate_ (SG-1, Atlantis, Universe) und _Vampire Diaries_ mit seinen Ablegen _The Originals_ und _Legacies_ (wer es nicht so mit dem übertrieben schnulzigen hat, aber den übernatürlichen Kram haben will: _The Originals_ ist eure Serie :ok_hand_tone2:).

    Aber ich habe auch ein paar Bücher in dem Bereich gelesen: _The Darkest Minds_ (Reihe aus 3+2 Büchern), _Fate The Winx Saga_ (Das Buch zur Netflix Serie) und _Magic Academy (engl: The Black Mage Series)_ (Reihe aus 4 Büchern).


=== "Lebenslauf"

    - Realschulabschluss
    - Abschluss als Lokführer (EiB-L/T)
        - 1 Jahr Vollzeit gefahren, seitdem in Teilzeit zum Führerscheinerhalt
    - Fachhochschulreife
        - im Abendunterricht parallel zur Berufsausbildung
    - [Bachelor of Science in Informatik :link:](https://gitlab.com/9Lukas5/hft-if7-ba/-/releases){target="_blank"}
    - [Master of Science in Software Technology :link:](https://gitlab.com/9Lukas5/master-thesis/-/releases){target="_blank"}
