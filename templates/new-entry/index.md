---
tags:
    - TBD

title: TBD
description:
    "TBD"

date: yyyy-MM-dd HH:mm
update: yyyy-MM-dd HH:mm
#comments:
#    host: mastodontech.de
#    username: 9Lukas5
#    id: TBD
#image: TBD
#list_entry: true
---

# {{ title }}

![TBD](./header.jpg){ data-description=".header-desc" loading=lazy }
<div class="glightbox-desc header-desc">
    TBD
</div>

<p class="introduction" markdown>
<span class="date">{{ date }}</span>  
{{ description }}
</p>

---
