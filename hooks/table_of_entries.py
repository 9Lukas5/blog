from mkdocs.config.defaults import MkDocsConfig
from mkdocs.structure.files import Files
from mkdocs.structure.pages import Page

enabled = True
preview_file_uri = 'Blog/index.md'
entries_table_file_uri = 'Verzeichnis/index.md'

pages_to_list: list|None = None


def on_files(files: Files, config: MkDocsConfig, **kwargs):
    global enabled
    if not enabled:
        return

    global preview_file_uri
    global entries_table_file_uri
    global pages_to_list

    pages_to_list = list()
    for f in files.documentation_pages():
        if f.src_uri == preview_file_uri:
            files.remove(f)
            files.append(f)
        elif f.src_uri == entries_table_file_uri:
            files.remove(f)
            files.append(f)


def on_page_markdown(markdown: str, page: Page, config: MkDocsConfig, files: Files, **kwargs):
    global enabled
    if not enabled:
        return

    global preview_file_uri
    global entries_table_file_uri
    global pages_to_list

    if page.meta.get('list_entry'):
        pages_to_list.append(page)

    if page.file.src_uri == preview_file_uri:

        preview_markdown = ''
        pages_to_list.sort(key=lambda page: page.meta.get("date"), reverse=True)

        for p in pages_to_list:
            preview_markdown += create_entry_preview(p, config)

        return markdown.replace('::table_of_entries::', preview_markdown)

    elif page.file.src_uri == entries_table_file_uri:
        pages_to_list.sort(key=lambda page: page.meta.get("date"), reverse=True)

        tables = dict()

        p: Page
        for p in pages_to_list:
            year = p.meta.get('date')[:4]
            table_line = create_entry_table_line(p)

            if not tables.get(year):
                tables[year] = table_line
            else:
                tables[year] += table_line

        markdown_tables = ''
        years = sorted(tables.keys(), reverse=True)
        for year in years:
            markdown_tables += (f'=== "{year}"\n'
                                f'\n'
                                f'    | Datum | Titel |\n'
                                f'    | - |:- |\n'
                                f'{tables[year]}\n\n')

        return markdown.replace('::table_of_entries::', markdown_tables)


def create_entry_preview(page: Page, config: MkDocsConfig) -> str:
    return (f'## [{page.meta.get("title")}](/{page.file.src_uri})\n'
            f'\n'
            f'<p class="introduction" markdown>\n'
            f'<span class="date">{page.meta.get("date")}</span>\n'
            f'{page.meta.get("description")}\n'
            f'</p>\n'
            f'\n'
            f'---\n')


def create_entry_table_line(page: Page) -> str:
    return f'    | {page.meta.get("date")} | [{page.meta.get("title")}](../{page.file.src_uri}) |\n'
