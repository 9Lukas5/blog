# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [15.0.1](../../compare/15.0.0...15.0.1) (2024-12-31)


### Technische Änderungen

* enable Mastodon comments on last entry ([f241a29](f241a29752feac159afdcab2ce008ccdbbba24a7))

## [15.0.0](../../compare/14.1.1...15.0.0) (2024-12-31)


### Erscheinungsbild

* switch to classic admonitions ([8a4c2b2](8a4c2b226a29b4f9c58abd455b3ba5b37b63f266))


### Technische Änderungen

* **config:** use pymdownx.highlight with linenums ([5a89202](5a8920269e8fc827e11131c1c5ce33e44751db10))
* **Dockerfile:** don't install git-lfs ([f3e552f](f3e552f322294f5b31465520275a8639eae2aa53))
* **Dockerfile:** remove more elements from the build stage ([b435736](b435736479564b3ba161a01d9b3619e91f1122e9))
* **RSS:** narrow down match path again to just the year folders ([fc10ae7](fc10ae7a786fb656a0bca1d1599eb7fc6792dcae))


### Fehler Behebungen

* **Dockerfile:** don't run poetry for config, copy it from build-stage ([775b8ec](775b8ec15b41ddf0bcba3e659adf31220c0a15e1))


### Neue Einträge

* Mutli-Stage custom Docker Image for CI-Pipeline ([5569061](5569061774c8af6475e1d6f20fe8f28c884c6da9))

## [14.1.1](../../compare/14.1.0...14.1.1) (2024-12-29)


### Fehler Behebungen

* **ci:** docker-retag source tag was wrong ([0545463](05454631436e8fe2f0e4c66a0809ead71897ddce))

## [14.1.0](../../compare/14.0.1...14.1.0) (2024-12-29)


### Technische Änderungen

* update dependencies ([e1c5fd5](e1c5fd5ca66f157bd8b1861389694938cb5e67b8))
* use project specific built docker image ([2746888](2746888fe0fd422fa2076ed61a12d3807151e50e))


### Fehler Behebungen

* **ci:** docker-build rules were faulty ([5b5012e](5b5012ee99b95748222bfe0c59bd6755d49c54c7))

## [14.0.1](../../compare/14.0.0...14.0.1) (2024-05-05)


### Technische Änderungen

* enable Mastodon comments on last entry ([3e10eda](3e10eda7c817de93f6ab521871544c6d4e3e6570))

## [14.0.0](../../compare/13.0.1...14.0.0) (2024-05-05)


### Neue Einträge

* Neue Glasplatte fuers Kochfeld ([13ac367](13ac367b2b58bda5af14da2fcf4cff5c32be6f6f))

## [13.0.1](../../compare/13.0.0...13.0.1) (2023-12-05)


### Redaktionelle Änderungen

* **Tagestourismus am Ritten:** Mastodon-Post-Id angepasst ([5d779b1](5d779b1abf586374681e5e40a79cf6cb6a600a13))

## [13.0.0](../../compare/12.0.1...13.0.0) (2023-12-05)


### Neue Einträge

* Tagestourismus am Ritten - Fluch oder Segen? ([5b7cdf7](5b7cdf7ee14bf7c1d55b7ff7f4cb79199966d082))

## [12.0.1](../../compare/12.0.0...12.0.1) (2023-11-06)


### Technische Änderungen

* add mkdocs hook script to generate preview and entries tables ([719f37b](719f37b41964e0e4920341d7cd5358712310997d))
* migrate all entries to the new py-hook generation style ([3ba349c](3ba349c34851c977a665e08527eac8c4f9a324db))

## [12.0.0](../../compare/11.1.2...12.0.0) (2023-10-31)


### Neue Einträge

* Inselbahn Borkum ([a398005](a398005b7824e2c18a16e5e362270c342dd95927))


### Redaktionelle Änderungen

* **Inselbahn:** Kommentare aktiviert ([09c2480](09c2480f3ac0a5eb62ac0ff0812c14e3715eeeb0))

## [11.1.2](../../compare/11.1.1...11.1.2) (2023-10-12)


### Technische Änderungen

* bundle iframe-worker ([960d0c5](960d0c54f4d49115ab2ed026f4f04130f3eb4a95))

## [11.1.1](../../compare/11.1.0...11.1.1) (2023-10-11)


### Technische Änderungen

* instant-loading wieder deaktiviert ([d25dcef](d25dcefad669842a4d7bd3a504b9d32445a40dd6))

## [11.1.0](../../compare/11.0.1...11.1.0) (2023-10-11)


### Technische Änderungen

* einige mkdocs-material Features aktiviert ([599a3a0](599a3a08a95df18d1ed7dedff0dd4f25c1692a79))

## [11.0.1](../../compare/11.0.0...11.0.1) (2023-10-11)


### Fehler Behebungen

* Brucker Lache: Social-Cards Bild konfiguriert ([ef5044a](ef5044a867a4a85f0f9d2dd5921ae9cc70841748))

## [11.0.0](../../compare/10.3.0...11.0.0) (2023-10-11)


### Technische Änderungen

* **ci:** set default PUML-Server to localhost and use public one in pipeline ([2773238](27732382c295ed644829e6536b66ff38fb43020d))


### Neue Einträge

* Brucker Lache mit Langenaufeld ([c6ad6f2](c6ad6f2b58fbe692bb7c1ab37aeba72040a9288e))

## [10.3.0](../../compare/10.2.1...10.3.0) (2023-09-30)


### Redaktionelle Änderungen

* remove bird social link ([f16f89c](f16f89c1899f24677c3c4b4490c800b3463a620b))


### Technische Änderungen

* **config:** locale Keyword is deprecated ([d01debd](d01debddb2c7fc2bab3c95719951fe903ac21fc6))
* **robots:** Schließe weitere Bots aus ([1f89b55](1f89b5527f4762f03e22ea9e435724a717565025))
* update mkdocs-include-markdown-plugin to 6.x ([d1216ff](d1216ff06f7d664572a2e02395bbc196444cd4f9))

### [10.2.1](../../compare/10.2.0...10.2.1) (2023-08-08)


### Technische Änderungen

* **robots:** Schließe GPT aus ([a847116](a847116b100d9b4088f7041923ad5a09c25edec7))

## [10.2.0](../../compare/10.1.0...10.2.0) (2023-08-04)


### Inhaltliche Änderungen

* **systemd-boot:** change steps for Fedora, not using uefi combined image ([6ae240c](6ae240c49a00e808ea339585daaba1627f4d7757))

## [10.1.0](../../compare/10.0.1...10.1.0) (2023-07-30)


### Redaktionelle Änderungen

* 'sort of' aus Blog-Titel entfernt ([f4261e2](f4261e25b085c714901b1278286419237a534d45))


### Technische Änderungen

* einige pymdownx Funktionen aktiviert ([ade2708](ade2708b264589c3238fb8d7e0322703926ce12e))
* RSS feed mit mkdocs-rss-plugin hinzugefuegt ([f0567ee](f0567ee454317532463a393e7febb28333fc4156))

### [10.0.1](../../compare/10.0.0...10.0.1) (2023-07-29)


### Redaktionelle Änderungen

* enable comments on newest entry ([2e0e6a2](2e0e6a2d26478777a48e707b55eaf57850c82e59))

## [10.0.0](../../compare/9.1.0...10.0.0) (2023-07-29)


### Erscheinungsbild

* **2023/01:** android update: anpassung an glitchbox einfuehrung ([63dc86c](63dc86c64f212c73d6b4dd14e086f0709f351e09))


### Technische Änderungen

* link tabs mit selbem Namen ([7f91040](7f91040c984114fb3df5950ccfa7ad8a1390e700))
* **overrides:** remove social override ([c445213](c445213b9388f5d8f5ef134861f7363cdab78a67))


### Fehler Behebungen

* restore social-cards ([2b4cdef](2b4cdef5ee6b0354fa087f8e17e57b467365cf5f))


### Neue Einträge

* Replace Grub with Systemd-Boot ([a8c0cfd](a8c0cfd6684aa2c6c1fba3ac7b7eeea92669422b))

## [9.1.0](../../compare/9.0.1...9.1.0) (2023-07-24)


### Erscheinungsbild

* add glightbox for better image experience ([060b6e4](060b6e49e4a44a0d724ef872973c1e4695c68e79))


### Technische Änderungen

* change filename to index instead of text ([ed61ad6](ed61ad61e02991289bc6849c5acb80a9b7b22c7e))
* make search working in offline version ([156a174](156a17443025ed1c9a9b9c01bfe8f31475dbd776))
* move pictures to git-lfs ([e27546d](e27546df72c91384fb844ef6fcae87f3d359426e))

### [9.0.1](../../compare/9.0.0...9.0.1) (2023-07-16)


### Redaktionelle Änderungen

* enable comments on newest entry ([7bf1be9](7bf1be9df21272d38cc3a0c4ec4f4f3513661027))

## [9.0.0](../../compare/8.0.1...9.0.0) (2023-07-16)


### Technische Änderungen

* upgrade MkDocs to 9.x ([c8518a6](c8518a6f027304812f9a01b2ec52809f99e4d091))


### Redaktionelle Änderungen

* update copyright to 2023 ([943b46f](943b46fe83514a07e3d4c0b19ccca6de769d2d80))


### Neue Einträge

* The USB-PD lockdown frustration ([3dc7522](3dc7522649c34ed0d05f3656ffcb27728f0fe875))

## [8.0.1](../../compare/8.0.0...8.0.1) (2023-07-06)


### Redaktionelle Änderungen

* enable comments on newest entry ([cb93bf6](cb93bf6d02afa8e88e47b4a28cbffdcc4940124d))

## [8.0.0](../../compare/7.0.0...8.0.0) (2023-07-06)


### Fehler Behebungen

* **2023-04-19_glasfaser:** Schreibfehler korrigiert ([d5e9b60](d5e9b60a2482c4a9350598b5b5da1cd3dec16ba4))


### Neue Einträge

* Kommentare in statischem Blog via Fediverse ([dbf8761](dbf8761ac7d07871c258a53fc8a0bcb297559fa2))

## [7.0.0](../../compare/6.0.1...7.0.0) (2023-04-19)


### Neue Einträge

* Glasfaser endlich in Betrieb ([6bf4b6b](6bf4b6ba276d15509eaa8c535e7fe59a491d34a3))

## [6.0.1](../../compare/6.0.0...6.0.1) (2023-03-10)


### Fehler Behebungen

* add date to telekom entry ([78506b8](78506b889e5830ad068c4b506e91b05bbd041de0))

## [6.0.0](../../compare/5.1.0...6.0.0) (2023-03-10)


### Neue Einträge

* Telekom Service-Hoelle ([01c565d](01c565d43a12663d8f5095960b1cf0c0b5a9f9dd))

## [5.1.0](../../compare/5.0.0...5.1.0) (2023-01-22)


### Erscheinungsbild

* **Android-Major Upgrade:** Füge ein paar Screenshots hinzu ([e00b5a1](e00b5a1a5f307801b89a4231bc6ea14775416555))

## [5.0.0](../../compare/4.0.0...5.0.0) (2023-01-21)


### Neue Einträge

* Android 13 Major Upgrade ([8d96626](8d9662664eb898562fafff310785a41d5f1cb182))

## [4.0.0](../../compare/3.2.0...4.0.0) (2022-12-10)


### Neue Einträge

* Docker zieht um ([7684607](76846078d170cdb41643944ce8cdcf8571b44e79))

## [3.2.0](../../compare/3.1.0...3.2.0) (2022-11-10)


### Technische Änderungen

* **2022-06_Pipewire-Alsa:** Linkvorschau: Füge Titel/Beschreibung hinzu ([bf1afba](bf1afba9358fc116e8e743180b7e3f45bea19498))
* **2022-10_Docker-Ipv6:** Linkvorschau: Füge Titel/Beschreibung hinzu ([396afd3](396afd3e92f1036a198ce421b4348a0e0a3bdecc))
* **2022-10_Ubuntu-bringt-Pipewire:** Linkvorschau: Füge Titel/Beschreibung hinzu ([f6e0680](f6e068013ff5968233363df3bfc5e0629118c1c2))
* **2022-11_zahnbürste-akkus:** Linkvorschau: Bild konfiguriert ([4a50f8f](4a50f8f039a23daa0d276efbd0223d0400abd9e8))
* **Changelog:** Linkvorschau: Füge Titel/Beschreibung hinzu ([38eb257](38eb2573a830b1ec601f74446a915a38225439d3))
* **Startseite:** Linkvorschau: Füge Titel/Beschreibung hinzu ([31b4bc9](31b4bc9a1c9168227373583856a033206353a591))

## [3.1.0](../../compare/3.0.0...3.1.0) (2022-11-10)


### Erscheinungsbild

* Doppelte Unendlichkeit als Logo/Favicon ([ae357a6](ae357a65c890141f1ad27e973fec8570b88737a5))
* Primär-Farbe dunkleres grün ([cb4954f](cb4954fc81ace73a012ab1147968445445ee9d22))


### Redaktionelle Änderungen

* Menü umstrukturiert ([9ec9c7e](9ec9c7ed7fe708b9b8f8319fc22b2146e1f980bf))


### Inhaltliche Änderungen

* Füge Changelog hinzu ([9477099](947709982bedf81e31d7e20c209511b2e1db837e))
* **Über:** Allgemeinen Infotext ergänzt ([5d32539](5d32539d99398cd0175e28d741f7d4c6a492d4cd))
* **Verzeichnis:** Themenauflistung für 2022 ([768e783](768e783b1526c7f352a67f9e2826bc3d2cde30ad))


### Technische Änderungen

* **CI:** Pipeline läuft nur wenn bestimmte Dateien verändert ([c70ac51](c70ac513d473411494699e1f7933c1c5c2ecd56e))
* Linkvorschau eingebaut ([273b58f](273b58fa93928d6ee64a3db628357c3f5a71e63d))
* **Linkvorschau:** Setze Standard-Bild ([8598793](85987933a4ab7c2dbe7ee2abc241bc68335a31da))

## [3.0.0](../../compare/2.0.0...3.0.0) (2022-11-08)

### Neue Einträge

* Tiefbauarbeiten am Linux Server

## [2.0.0](../../compare/1.0.0...2.0.0) (2022-11-01)

### Neue Einträge

* Akkus bei Schallzahnbürste tauschen

## 1.0.0 (2022-10-28)

### Neue Einträge

* Ubuntu 22.10 bringt Pipewire-Audio, aber Alsa ist kaputt
* Docker Ipv6 NAT und KVM VMs geht nicht
* Alsa mit Pipewire funktioniert endlich
